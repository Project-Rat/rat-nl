# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(SuperLUMT_PKGCONF SuperLUMT)

# Include dir
find_path(SuperLUMT_INCLUDE_DIR
  NAMES slu_mt_ddefs.h
  PATHS ${SuperLUMT_PKGCONF_INCLUDE_DIR}
  HINTS /usr/include/superlu_mt
)

# Finally the library itself
find_library(SuperLUMT_LIBRARY
  NAMES libsuperlu_mt_PTHREAD.a
  PATHS ${SuperLUMT_PKGCONF_LIBRARY_DIR}
  HINTS /usr/lib/libsuperlu_mt_PTHREAD.a
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(SuperLUMT_PROCESS_INCLUDES SuperLUMT_INCLUDE_DIR)
set(SuperLUMT_PROCESS_LIBS SuperLUMT_LIBRARY)
libfind_process(SuperLUMT)

if(NOT TARGET SuperLU::MT)
    add_library(SuperLU::MT INTERFACE IMPORTED)
    set_target_properties(SuperLU::MT PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${SuperLUMT_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "${SuperLUMT_LIBRARY}")
endif()
