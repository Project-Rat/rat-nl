// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>

// model headers
#include "rat/models/modelcube.hh"
#include "rat/models/modelsolenoid.hh"
#include "rat/models/vtkunstr.hh"

// fmm headers
#include "rat/mlfmm/mlfmm.hh"

// distmesh headers
#include "rat/dmsh/distmesh2d.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfcircle.hh"

// solver
#include "nonlinsolver.hh"
#include "preccholmod.hh"

// main function
int main(){
	// settings
	const rat::fltp Rin = 0.02; 
	const rat::fltp Rout = 0.03;
	const rat::fltp height = 0.04;
	const arma::uword num_rad = 4;
	const arma::uword num_height = 16;
	const arma::uword num_azym = 50;
	const rat::fltp J = 400e6;
	const arma::uword num_exp = 8;
	const arma::uword num_refine = 50;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// // create cube
	// const rat::mdl::ShModelSolenoidPr cube = rat::mdl::ModelSolenoid::create(0.1,0.02,0.02,0.005);
	// cube->add_rotation({1,0,0},arma::Datum<rat::fltp>::pi/2);
	// const std::list<rat::mdl::ShMeshDataPr> meshes = cube->create_meshes({},rat::mdl::MeshSettings());

	// // extract mesh
	// const arma::Mat<rat::fltp>& Rn = meshes.front()->get_nodes();
	// const arma::Mat<arma::uword>& n = meshes.front()->get_elements();

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(0.015);

	// the distance function
	mymesh->set_distfun(rat::dm::DFDiff::create(
		rat::dm::DFCircle::create(0.12,0,0),
		rat::dm::DFCircle::create(0.05,0,0)));

	// setup mesh
	mymesh->setup(); mymesh->extrude(0.1,2*0.1/0.015);

	// get mesh
	arma::Mat<rat::fltp> Rn = mymesh->get_nodes().t();
	const arma::Mat<arma::uword> n = mymesh->get_elements().t();
	Rn.swap_rows(1,2);

	// create HB curve
	rat::nl::ShHBCurvePr hb_curve = rat::nl::HBCurve::create();
	hb_curve->set_comsol();
	arma::Row<rat::fltp> Bi1 = arma::linspace<arma::Row<rat::fltp> >(0,20,100);
	arma::Row<rat::fltp> nu1 = hb_curve->calc_reluctivity(Bi1);
	arma::Row<rat::fltp> dnudB1 = hb_curve->calc_reluctivity_diff(Bi1);
	std::cout<<arma::join_vert(Bi1,nu1,dnudB1).t()<<std::endl;

	// create solver
	const rat::nl::ShNonLinSolverPr nl = rat::nl::NonLinSolver::create();
	// nl->set_preconditioner(rat::nl::PrecCholMod::create());

	// set logger
	nl->set_lg(lg);
	
	// set mesh to solver
	nl->set_mesh(Rn,n);
	nl->set_hb_curve(hb_curve);

	// setup the mesh
	nl->setup_mesh();

	// calculate external field
	const rat::fmm::ShMgnTargetsPr tar = nl->get_ext_targets();

	// create mlfmm calculation
	rat::fmm::ShCurrentSourcesPr src = rat::fmm::CurrentSources::create();
	src->setup_solenoid(Rin,Rout,height,num_rad,num_height,num_azym,J);

	// create mlfmm
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create(src,tar);

	// create settings
	rat::fmm::ShSettingsPr settings = myfmm->settings();
	settings->set_enable_gpu(false);
	settings->set_num_exp(num_exp);
	settings->set_num_refine(num_refine);
	settings->set_large_ilist();

	// setup mlfmm
	myfmm->setup(lg);

	// run multipole method
	myfmm->calculate(lg);

	// set external field
	nl->set_ext_field(tar);

	// solve
	const arma::Row<rat::fltp> A = nl->solve().t();

	// get field at gauss points
	const arma::Mat<rat::fltp> Bg = nl->calc_elem_flux_density(A);

	// get field
	const arma::Mat<rat::fltp> B = nl->element_average(Bg);

	// calculate reluctance
	const arma::Row<rat::fltp> nu = nl->element_average(nl->calc_reluctivity(rat::cmn::Extra::vec_norm(nl->calc_elem_flux_density(A))));

	// external field at gauss nodes
	// const arma::Mat<rat::fltp> Bgext = nl->calc_elem_flux_density(nl->get_Aeext());

	// external field
	// const arma::Mat<rat::fltp> Bext = nl->element_average(Bgext);

	// calculate field in facets
	// const arma::Row<rat::fltp> Bnf = nl->calc_facet_field(nl->get_Aeext());

	// calculate field in facets
	const arma::Row<rat::fltp> Bf = nl->calc_facet_field(A);

	// get coordinates of the gauss points
	const arma::Mat<rat::fltp> Rg = nl->get_gauss_coords();

	// get external field
	const arma::Mat<rat::fltp> Hext = nl->get_ext_field();


	// create export
	const rat::mdl::ShVTKUnstrPr vtk1 = rat::mdl::VTKUnstr::create();
	vtk1->set_mesh(nl->get_nodes(),nl->get_elements(),12);
	vtk1->set_elementdata(B,"flux density");
	// vtk1->set_elementdata(Bext,"flux density ext");
	vtk1->set_elementdata(B.each_row()%nu,"magnetic field");
	vtk1->set_elementdata(nu,"reluctance");
	vtk1->write("data.vtu");

	// create export
	const rat::mdl::ShVTKUnstrPr vtk2 = rat::mdl::VTKUnstr::create();
	vtk2->set_mesh(nl->get_nodes(),nl->get_facets(),9);
	// vtk2->set_elementdata(Bnf,"flux density ext");
	vtk2->set_elementdata(Bf,"flux density");
	vtk2->write("facet_data.vtu");

	// export gauss points
	const rat::mdl::ShVTKUnstrPr vtk3 = rat::mdl::VTKUnstr::create();
	vtk3->set_mesh(Rg,arma::regspace<arma::Row<arma::uword> >(0,Rg.n_cols-1),1);
	vtk3->set_elementdata(Bg,"flux density");
	// vtk3->set_elementdata(Bgext,"flux density ext");
	vtk3->set_elementdata(Hext,"magnetic field ext");
	vtk3->write("gauss_data.vtu");

	// done
	return 0;
}
