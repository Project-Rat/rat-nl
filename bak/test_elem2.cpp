// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>

// common headers
#include "rat/common/elements.hh"

// fmm headers
#include "rat/mlfmm/mlfmm.hh"

// solver
#include "nonlinsolver.hh"

// main function
int main(){
	// settings
	const arma::uword num_gauss_face = 4;
	const rat::fltp D = RAT_CONST(0.01); // box side length
	const rat::fltp tol = RAT_CONST(1e-4);
	const rat::fltp dxtrap = RAT_CONST(0.01);
	const arma::uword N = 100;

	// define box corners
	const arma::Col<rat::fltp>::fixed<3> R0 = {-D/2-dxtrap,-D/2-dxtrap,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R1 = {+D/2,-D/2,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R2 = {+D/2,+D/2,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R3 = {-D/2,+D/2,-D/2};

	// assemble matrix with nodes
	arma::Mat<rat::fltp> Rn(3,4);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;

	// get mesh (1 element)
	const arma::Col<arma::uword> n = arma::regspace<arma::Col<arma::uword> >(0,3);

	// gauss points
	const arma::Mat<fltp> gp = cmn::Quadrilateral::create_gauss_points(num_gauss_face);
	const arma::Mat<fltp> Rq = gp.rows(0,1);
	const arma::Row<fltp> wq = gp.row(2);

	// get shape derivative of shape function
	const arma::Mat<fltp> Rg = cmn::Quadrilateral::quad2cart(Rn,Rq.rows(0,1));
	const arma::Mat<fltp> dN = cmn::Quadrilateral::shape_function_derivative(Rq.rows(0,1));
	const arma::Mat<fltp> J = cmn::Quadrilateral::shape_function_jacobian(Rn,dN);
	const arma::Row<fltp> Jdet = cmn::Quadrilateral::jacobian2determinant(J);

	std::cout<<arma::join_vert(Rg,wq,Jdet).t()<<std::endl;

	std::cout<<1e6*arma::accu(Jdet%wq)<<std::endl;
	std::cout<<1e6*cmn::Quadrilateral::calc_area(Rn,n)<<std::endl;

	// std::cout<<Rg.t()<<std::endl;
	// std::cout<<dRg.t()<<std::endl;
	// std::cout<<cmn::Extra::vec_norm(dRg).t()<<std::endl;

	// done
	return 0;
}




	
