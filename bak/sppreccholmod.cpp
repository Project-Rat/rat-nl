/* Foxie - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "sppreccholmod.hh"

// constructor
SpPrecCholMod::SpPrecCholMod(){
	// set solver data object
	cholmod_l_start(&c_);
	c_.supernodal = CHOLMOD_SIMPLICIAL;
	// c_.useGPU = true;

	// allocate matrix
	A_ = new cholmod_sparse;
}

// destructor
SpPrecCholMod::~SpPrecCholMod(){
	// cholmod_l_gpu_stats(&c_);
	delete A_;
	if(L_!=NULL)cholmod_l_free_factor(&L_, &c_);
	cholmod_l_finish(&c_); 
}

// factory
ShSpPrecCholModPr SpPrecCholMod::create(){
	return std::make_shared<SpPrecCholMod>();
}

// analysis
void SpPrecCholMod::analyse(){
	// convert arrays
	li_idx_col_c_ = arma::conv_to<arma::Row<long int> >::from(idx_col_c_);
	li_idx_row_ = arma::conv_to<arma::Row<long int> >::from(idx_row_);	

	// allocate sparse matrix
	//A_ = cholmod_allocate_sparse(num_size_,num_size_,num_nnz_,true,true,0,CHOLMOD_REAL,&c_);
	// transfer matrix pointers
	A_->nrow = num_size_;
	A_->ncol = num_size_;
	A_->nzmax = num_nnz_;
	A_->nz = NULL;
	A_->p = li_idx_col_c_.memptr();
	A_->i = li_idx_row_.memptr();
	A_->x = val_.memptr();
	A_->z = NULL;
	A_->stype = 1; // symmetric (only upper triangle used)
	A_->itype = CHOLMOD_LONG; // problem with LONG
	A_->xtype = CHOLMOD_REAL;
	A_->dtype = CHOLMOD_DOUBLE;
	A_->sorted = true;
	A_->packed = true;

	// check matrix 
	cholmod_l_check_sparse(A_, &c_);

	// free matrix if it was already used
	if(L_!=NULL)cholmod_l_free_factor(&L_, &c_);

	// run analysis
	L_ = cholmod_l_analyze(A_, &c_);
}

// factorisation
void SpPrecCholMod::factorise(){
	// run factorisation
	cholmod_l_factorize(A_, L_, &c_);
}

// solving
arma::Col<double> SpPrecCholMod::solve(
	const arma::Col<double> &b){
	// copy B
	arma::Col<double> rhs = b;

	// convert right hand side
	cholmod_dense *chol_rhs = new cholmod_dense; 
	chol_rhs->nrow = num_size_;
	chol_rhs->ncol = 1;
	chol_rhs->nzmax = num_size_;
	chol_rhs->x = rhs.memptr();
	chol_rhs->z = NULL;
	chol_rhs->xtype = CHOLMOD_REAL;
	chol_rhs->dtype = CHOLMOD_DOUBLE;
	chol_rhs->d = num_size_;

	// solve
	cholmod_dense *chol_x = cholmod_l_solve(CHOLMOD_A, L_, chol_rhs, &c_); 

	// get result by copying memory
	arma::Col<double> x((double*)chol_x->x, num_size_);

	// remove right hand side
	delete chol_rhs;
	cholmod_l_free_dense(&chol_x, &c_);

	// return result
	return x;
}


