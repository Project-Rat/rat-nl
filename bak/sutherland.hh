/* Raccoon - An Electro-Magnetic and Thermal network 
solver accelerated with the Multi-Level Fast Multipole Method
Copyright (C) 2020  Jeroen van Nugteren*/

#ifndef NL_SUTHERLAND_HH

#include <iostream>
#include <armadillo>

#include "rat/common/typedefs.hh"
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// source: https://www.geeksforgeeks.org/polygon-clipping-sutherland-hodgman-algorithm-please-change-bmp-images-jpeg-png/

// code specific to Raccoon
namespace rat{namespace nl{

	// class for namespace
	class Sutherland{
		public:
			// Returns y-value of point of intersection of
			// two lines
			static arma::Col<rat::fltp>::fixed<2> intersect(
				const arma::Col<rat::fltp>::fixed<2> &p1, const arma::Col<rat::fltp>::fixed<2> &p2,
				const arma::Col<rat::fltp>::fixed<2> &p3, const arma::Col<rat::fltp>::fixed<2> &p4){

				// x intersect
				const rat::fltp num1 = (p1(0)*p2(1) - p1(1)*p2(0)) * (p3(0)-p4(0)) - (p1(0)-p2(0)) * (p3(0)*p4(1) - p3(1)*p4(0));
				const rat::fltp den1 = (p1(0)-p2(0)) * (p3(1)-p4(1)) - (p1(1)-p2(1)) * (p3(0)-p4(0));
				
				// y intersect
				const rat::fltp num2 = (p1(0)*p2(1) - p1(1)*p2(0)) * (p3(1)-p4(1)) - (p1(1)-p2(1)) * (p3(0)*p4(1) - p3(1)*p4(0));
				const rat::fltp den2 = (p1(0)-p2(0)) * (p3(1)-p4(1)) - (p1(1)-p2(1)) * (p3(0)-p4(0));

				return {num1/den1, num2/den2};
			}
			  
			// This functions clips all the edges w.r.t one clip
			// edge of clipping area
			static arma::Mat<rat::fltp> clip(
				const arma::Mat<rat::fltp> &poly_points, 
				const arma::Col<rat::fltp>::fixed<2> &p1, 
				const arma::Col<rat::fltp>::fixed<2> &p2){
				// store initial size
				arma::Mat<rat::fltp> new_points(2,poly_points.n_cols*2);
				arma::uword num_points = 0;

				// pi and ik are the coordinate values of the points
				for (arma::uword i=0;i<poly_points.n_cols;i++){
					// check size
					if(num_points>=new_points.n_cols)
						rat_throw_line("exceeding pre-allocated size");

					// i and k form a line in polygon
					const arma::uword k = (i+1)%poly_points.n_cols;
					const arma::Col<rat::fltp>::fixed<2> pi = poly_points.col(i);
					const arma::Col<rat::fltp>::fixed<2> pk = poly_points.col(k);

					// Calculating position of first point w.r.t. clipper line
					const rat::fltp i_pos = (p2(0)-p1(0)) * (pi(1)-p1(1)) - (p2(1)-p1(1)) * (pi(0)-p1(0));

					// Calculating position of second point w.r.t. clipper line
					const rat::fltp k_pos = (p2(0)-p1(0)) * (pk(1)-p1(1)) - (p2(1)-p1(1)) * (pk(0)-p1(0));

					// Case 1 : When both points are inside
					if (i_pos < 0  && k_pos < 0){
						// add only second point
						new_points.col(num_points++) = pk; 
					}

					// Case 2: When only first point is outside
					else if (i_pos >= 0  && k_pos < 0){
						// Point of intersection with edge
						// and the second point is added
						new_points.col(num_points++) = intersect(p1, p2, pi, pk);
						new_points.col(num_points++) = pk;
					}

					// Case 3: When only second point is outside
					else if (i_pos < 0  && k_pos >= 0){
						//Only point of intersection with edge is added
						new_points.col(num_points++) = intersect(p1, p2, pi, pk);
					}

					// Case 4: When both points are outside
					else
					{
						//No points are added
					}
				}

				// return new point list
				if(num_points>0)return new_points.cols(0,num_points-1); else return arma::Mat<rat::fltp>(2,0);
			}

			// Implements Sutherland–Hodgman algorithm
			// points must be clockwise
			static arma::Mat<rat::fltp> calculate_clip(
				const arma::Mat<rat::fltp> &poly_points, 
				const arma::Mat<rat::fltp> &clipper_points){

				// allocate ouput
				arma::Mat<rat::fltp> output_points = poly_points;

				//i and k are two consecutive indexes
				for (arma::uword i=0; i<clipper_points.n_cols; i++){
					arma::uword k = (i+1)%clipper_points.n_cols;

					// We pass the current array of vertices, it's size
					// and the end points of the selected clipper line
					output_points = clip(output_points, clipper_points.col(i), clipper_points.col(k));
				}

				// reduce array size
				return output_points;
			}
	};

}}

#endif