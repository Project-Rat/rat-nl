# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(KLU_PKGCONF KLU)

# Include dir
find_path(KLU_INCLUDE_DIR
  NAMES klu.h
  PATHS ${KLU_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(KLU_LIBRARY
  NAMES klu
  PATHS ${KLU_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(KLU_PROCESS_INCLUDES KLU_INCLUDE_DIR)
set(KLU_PROCESS_LIBS KLU_LIBRARY)
libfind_process(KLU)

if(NOT TARGET KLU::KLU)
	add_library(KLU::KLU INTERFACE IMPORTED)
	set_target_properties(KLU::KLU PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${KLU_INCLUDE_DIR}"
		INTERFACE_LINK_LIBRARIES "${KLU_LIBRARY}")
endif()