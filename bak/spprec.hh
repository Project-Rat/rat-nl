// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef SP_PREC_HH
#define SP_PREC_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "extra.hh"
#include "umfpack.h"

// shared pointer definition
typedef std::shared_ptr<class SpPrec> ShSpPrecPr;

// sparse preconditioner template class
class SpPrec{
	// properties
	protected:
		// matrix size
		arma::uword num_size_;
		arma::uword num_nnz_;

		// matrix entries in compressed column storage format (CCS)
		arma::Row<arma::sword> idx_col_c_;
		arma::Row<arma::sword> idx_row_;
		arma::Row<double> val_;

	public:
		// destructor
		virtual ~SpPrec(){};

		// set matrix
		void set(const arma::Row<arma::uword> &idx_col_c, const arma::Row<arma::uword> &idx_row, const arma::Row<double> &val, const arma::uword num_size); 

		// solver
		virtual void analyse() = 0;
		virtual void factorise() = 0;
		virtual arma::Col<double> solve(const arma::Col<double> &b) = 0;

		// matrix vector product
		arma::Col<double> mvp(const arma::Col<double> &x) const;
};

#endif