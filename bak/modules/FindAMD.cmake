# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(AMD_PKGCONF AMD)

# Include dir
find_path(AMD_INCLUDE_DIR
  NAMES amd.h
  PATHS ${AMD_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(AMD_LIBRARY
  NAMES amd
  PATHS ${AMD_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(AMD_PROCESS_INCLUDES AMD_INCLUDE_DIR)
set(AMD_PROCESS_LIBS AMD_LIBRARY)
libfind_process(AMD)
