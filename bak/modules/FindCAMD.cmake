# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(CAMD_PKGCONF CAMD)

# Include dir
find_path(CAMD_INCLUDE_DIR
  NAMES camd.h
  PATHS ${CAMD_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(CAMD_LIBRARY
  NAMES camd
  PATHS ${CAMD_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(CAMD_PROCESS_INCLUDES CAMD_INCLUDE_DIR)
set(CAMD_PROCESS_LIBS CAMD_LIBRARY)
libfind_process(CAMD)
