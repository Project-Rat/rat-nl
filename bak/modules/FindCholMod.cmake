# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(CHOLMOD_PKGCONF CHOLMOD)

# Include dir
find_path(CHOLMOD_INCLUDE_DIR
  NAMES cholmod.h
  PATHS ${CHOLMOD_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(CHOLMOD_LIBRARY
  NAMES cholmod
  PATHS ${CHOLMOD_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(CHOLMOD_PROCESS_INCLUDES CHOLMOD_INCLUDE_DIR)
set(CHOLMOD_PROCESS_LIBS CHOLMOD_LIBRARY)
libfind_process(CHOLMOD)
