# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(KINSOL_PKGCONF KINSOL)

# Include dir
find_path(KINSOL_INCLUDE_DIR
    nvector/nvector_serial.h
    sundials/sundials_dense.h
    sundials/sundials_types.h
    kinsol/kinsol.h
    kinsol/kinsol_dense.h
    kinsol/kinsol_band.h
    kinsol/kinsol_spgmr.h
    kinsol/kinsol_spbcgs.h
    kinsol/kinsol_sptfqmr.h
    kinsol/kinsol_impl.h
  PATHS ${KINSOL_PKGCONF_INCLUDE_DIR}
)


# Finally the library itself
find_library(KINSOL_LIBRARY
  sundials_kinsol
  sundials_fnvecserial
  sundials_nvecserial
  PATHS ${KINSOL_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(KINSOL_PROCESS_INCLUDES KINSOL_INCLUDE_DIR)
set(KINSOL_PROCESS_LIBS KINSOL_LIBRARY)
libfind_process(KINSOL)
