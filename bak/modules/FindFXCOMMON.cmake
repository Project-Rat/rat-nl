# include libfind
include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(FXCOMMON_PKGCONF FXCOMMON)

# Include dir
find_path(FXCOMMON_INCLUDE_DIR
  NAMES elements.hh extra.hh parfor.hh
  PATHS ${FXCOMMON_PKGCONF_INCLUDE_DIR}
  HINTS /usr/local/include/fx-common
)

# Finally the library itself
find_library(FXCOMMON_LIBRARY
  NAMES fx-common-st
  PATHS ${FXCOMMON_PKGCONF_LIBRARY_DIR}
  HINTS /usr/local/lib/fx-common
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(FXCOMMON_PROCESS_INCLUDES FXCOMMON_INCLUDE_DIR)
set(FXCOMMON_PROCESS_LIBS FXCOMMON_LIBRARY)
libfind_process(FXCOMMON)
