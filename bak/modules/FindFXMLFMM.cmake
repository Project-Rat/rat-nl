# include libfind
include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(FXMLFMM_PKGCONF FXMLFMM)

# Include dir
find_path(FXMLFMM_INCLUDE_DIR
  NAMES mlfmm.hh
  PATHS ${FXMLFMM_PKGCONF_INCLUDE_DIR}
  HINTS /usr/local/include/fx-mlfmm
)

# Finally the library itself
find_library(FXMLFMM_LIBRARY
  NAMES fx-mlfmm-st
  PATHS ${FXMLFMM_PKGCONF_LIBRARY_DIR}
  HINTS /usr/local/lib/fx-mlfmm
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(FXMLFMM_PROCESS_INCLUDES FXMLFMM_INCLUDE_DIR)
set(FXMLFMM_PROCESS_LIBS FXMLFMM_LIBRARY)
libfind_process(FXMLFMM)

