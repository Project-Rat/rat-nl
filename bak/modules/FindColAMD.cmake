# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(COLAMD_PKGCONF COLAMD)

# Include dir
find_path(COLAMD_INCLUDE_DIR
  NAMES colamd.h
  PATHS ${COLAMD_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(CCOLAMD_LIBRARY
  NAMES colamd
  PATHS ${COLAMD_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(COLAMD_PROCESS_INCLUDES COLAMD_INCLUDE_DIR)
set(COLAMD_PROCESS_LIBS COLAMD_LIBRARY)
libfind_process(COLAMD)
