// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "precsuperlumt.hh"

#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecSuperLUMT::PrecSuperLUMT(){
		SetDefaultOptions(&options_);
	}

	PrecSuperLUMT::~PrecSuperLUMT(){
		if(factorised_){
			Destroy_SuperMatrix_Store(&A_);
			Destroy_SuperNode_Matrix(&L_);
			Destroy_CompCol_Matrix(&U_);
		}
	}

	// factory
	ShPrecSuperLUMTPr PrecSuperLUMT::create(){
		return std::make_shared<PrecSuperLUMT>();
	}

	// set matrix
	void PrecSuperLUMT::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		// // number of nonzeros
		// arma::uword nnz = val.n_elem;

		// // get pointers

		// // Create compressed column matrix
		// dCreate_CompCol_Matrix(&A_, num_size, num_size, nnz, val.memptr(), idx_col.memptr(), idx_row.memptr(), SLU_NC, SLU_D, SLU_GE);

		// // allocate permutation vectors
		// perm_r_.set_size(num_size);
		// perm_c_.set_size(num_size);
	}

	// analysis
	void PrecSuperLUMT::analyse(){
		// // report
		// lg_->msg("%s  <<< superlu analyse >>>%s\n",KMAG,KNRM);

		// // analysis is done as part of factorisation

		// // set analyzed to true
		// analysed_ = true;
	}

	// factorisation
	void PrecSuperLUMT::factorise(){
		// // factorise t
		// lg_->msg("%s  <<< superlu factorise >>>%s\n",KMAG,KNRM);
		
		// // Perform factorization
		// dgstrf(&options_, &A_, 0, 0, perm_c_.memptr(), perm_r_.memptr(), &L, &U, &stat, &info);

		// // report
		// lg_->msg("%s  <<< superlu-MT solving >>>%s\n",KMAG,KNRM);

		// // set factorised to true
		// factorised_ = true;
	}

	// solving
	arma::Col<rat::fltp> PrecSuperLUMT::solve(
		const arma::Col<rat::fltp> &b, 
		const rat::fltp /*delta*/){
				
		// // solve for x
		// arma::Col<fltp> x;
		// const bool solution_ok = SF_.solve(x,b);

		// // check output
		// if(solution_ok==false)rat_throw_line("superlu solution not ok");

		// // return output
		// return x;
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecSuperLUMT::mvp(
		const arma::Col<rat::fltp> &x) const{

		// // output
		// return A_*x;
	}

	// display preconditioner info
	void PrecSuperLUMT::display()const{
		// lg_->msg("preconditioner: %sSUPERLU%s (SuiteSparse)\n",KYEL,KNRM);
		// lg_->msg("factorisation type: %sLU%s\n",KYEL,KNRM);
	}

}}