// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef NL_MAGNETIC_CHARGE_SURFACE_HH
#define NL_MAGNETIC_CHARGE_SURFACE_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/error.hh"

// mlfmm headers
#include "rat/mlfmm/sources.hh"
#include "rat/mlfmm/targetpoints.hh"
#include "rat/mlfmm/stmat.hh"

// code specific to Rat
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class MgnChargeSurface> ShMgnChargeSurfacePr;

	// collection of charges for electric field calculation
	// is derived from the sources class
	class MgnChargeSurface: virtual public fmm::Sources, public rat::fmm::TargetPoints{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 1;

			// coordinates in [m]
			arma::Mat<fltp> Rn_;

			// faces
			arma::Mat<arma::uword> f_;

			// magnetic charge
			arma::Mat<fltp> sigma_s_;

			// number of gauss points target side
			arma::uword num_gauss_ = 4;

			// number of sources
			arma::uword num_sources_ = 0;

			// source to multipole matrices
			fmm::StMat_So2Mp_J M_J_;
			arma::Mat<fltp> dR_;

			// multipole operation matrices
			fmm::StMat_Lp2Ta M_A_; // for vector potential
			arma::Mat<fltp> dRl2p_;

		// methods
		public:
			// constructor
			MgnChargeSurface();
			MgnChargeSurface(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &f, 
				const arma::Row<fltp> &sigma_s);

			// factory
			static ShMgnChargeSurfacePr create();
			static ShMgnChargeSurfacePr create(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &f, 
				const arma::Row<fltp> &sigma_s);

			// set coordinates and currents of elements
			void set_mesh(const arma::Mat<fltp> &Rs, const arma::Mat<arma::uword> &f);

			// set direction vector
			void set_magnetic_charge(const arma::Row<fltp> &sigma_s);

			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// get charge
			arma::Mat<fltp> get_charge() const;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// get element size
			fltp element_size() const override;

			// source to multipole
			void setup_source_to_multipole(
				const arma::Mat<fltp> &dR, 
				const fmm::ShSettingsPr &stngs) override;
			void source_to_multipole(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const fmm::ShSettingsPr &stngs) const override;
				
			// localpole to target virtual functions
			void setup_localpole_to_target(
				const arma::Mat<fltp> &dR, 
				const rat::fmm::ShSettingsPr &stngs) override;
			void localpole_to_target(
				const arma::Mat<std::complex<fltp> > &Lp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::uword num_dim, 
				const rat::fmm::ShSettingsPr &stngs) override;

			// direct field calculation for both A and B
			void calc_direct(
				const fmm::ShTargetsPr &tar, 
				const fmm::ShSettingsPr &stngs) const override;
			void source_to_target(
				const fmm::ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const fmm::ShSettingsPr &stngs) const override;
	};

}}

#endif
