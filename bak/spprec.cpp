/* Foxie - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "spprec.hh"

// set matrix
void SpPrec::set(const arma::Row<arma::uword> &idx_col_c, const arma::Row<arma::uword> &idx_row, const arma::Row<double> &val, const arma::uword num_size){
	// check matrix
	assert(idx_col_c.is_sorted());
	assert(idx_col_c.n_elem==num_size+1);
	assert(idx_row.n_elem==val.n_elem);

	// set matrix
	idx_col_c_ = arma::conv_to<arma::Row<arma::sword> >::from(idx_col_c);
	idx_row_ = arma::conv_to<arma::Row<arma::sword> >::from(idx_row);
	val_ = val; 

	// set counters
	num_size_ = num_size;
	num_nnz_ = val.n_elem;
}

// matrix vector multiplication
arma::Col<double> SpPrec::mvp(
	const arma::Col<double> &x) const{

	// make sure matrix is still compressed
	assert(!idx_col_c_.is_empty());
	assert(x.n_elem==num_size_);

	// allocate output
	arma::Col<double> b(num_size_,arma::fill::zeros);

	// walk over columns
	for(arma::uword i=0;i<num_size_;i++){
		// get the indices of the rows present in this column
		arma::Col<arma::uword> idx_row = 
			arma::conv_to<arma::Col<arma::uword>>::from(
			idx_row_.cols(idx_col_c_(i),idx_col_c_(i+1)-1));

		// add this column's contribution to b
		b.rows(idx_row) += (val_.cols(idx_col_c_(i),idx_col_c_(i+1)-1)*x(i)).t();
	}

	// output
	return b;
}


