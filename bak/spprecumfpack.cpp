/* Foxie - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "spprecumfpack.hh"

// constructor
SpPrecUMFPack::SpPrecUMFPack(){
	// set default settings
	umfpack_dl_defaults(control_);
	control_[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	control_[UMFPACK_STRATEGY] = UMFPACK_STRATEGY_AUTO;
}

// destructor
SpPrecUMFPack::~SpPrecUMFPack(){
	if(Symbolic_!=NULL)umfpack_dl_free_symbolic(&Symbolic_);
	if(Numeric_!=NULL)umfpack_dl_free_numeric(&Numeric_);
}

// factory
ShSpPrecUMFPackPr SpPrecUMFPack::create(){
	return std::make_shared<SpPrecUMFPack>();
}

// analysis
void SpPrecUMFPack::analyse(){
	// make sure matrix is compressed
	assert(!idx_col_c_.is_empty());
	assert(arma::as_scalar(idx_col_c_.tail(1))==(arma::sword)num_nnz_);

	// std::cout<<idx_row_.rows(0,10)<<std::endl;
	// std::cout<<idx_col_c_.rows(0,10)<<std::endl;

	// get and cast memory pointers
	const long int* col_ptr = (long int*)idx_col_c_.memptr();
	const long int* row_ptr = (long int*)idx_row_.memptr();
	const double* val_ptr = (double*)val_.memptr();
	int n = (int)num_size_;

	//  Carry out the symbolic factorization.
	status_ = umfpack_dl_symbolic(n, n, col_ptr, row_ptr, 
		val_ptr, &Symbolic_, control_, info_ );

	// report and check
	if(status_!=UMFPACK_OK)umfpack_di_report_status(control_, status_);
	assert(status_==UMFPACK_OK);

	// set analyzed to true
	analysed_ = true;
}

// factorisation
void SpPrecUMFPack::factorise(){
	// make sure values are set
	assert(!idx_row_.is_empty());
	assert(!idx_col_c_.is_empty());
	assert(!val_.is_empty());

	// check
	assert(analysed_==true);
	assert(!idx_col_c_.is_empty());

	// get and cast memory pointers
	const long int* col_ptr = (long int*)idx_col_c_.memptr();
	const long int* row_ptr = (long int*)idx_row_.memptr();
	const double* val_ptr = (double*)val_.memptr();

	//  Use the symbolic factorization to carry out the numeric factorization.
	status_ = umfpack_dl_numeric(col_ptr, row_ptr, 
		val_ptr, Symbolic_, &Numeric_, control_, info_ );

	// report and check
	if(status_!=UMFPACK_OK)umfpack_di_report_status(control_, status_);
	assert(status_==UMFPACK_OK);

	// check strategy
	if(info_[UMFPACK_STRATEGY_USED]==UMFPACK_STRATEGY_SYMMETRIC)std::printf("using symmetric strategy\n");

	// set factorised to true
	factorised_ = true;
}

// solving
arma::Col<double> SpPrecUMFPack::solve(
	const arma::Col<double> &b){
	
	// make sure matrix is still compressed
	assert(!idx_col_c_.is_empty());
	
	// make sure factorisation was performed
	assert(factorised_==true);

	// make sure R has the correct size
	assert(b.n_elem==num_size_);

	// allocate output
	arma::Col<double> x(num_size_,arma::fill::zeros);

	// get pointer to r
	double* B = (double*)b.memptr();
	double* X = (double*)x.memptr();

	// get and cast memory pointers
	const long int* col_ptr = (long int*)idx_col_c_.memptr();
	const long int* row_ptr = (long int*)idx_row_.memptr();
	const double* val_ptr = (double*)val_.memptr();

	//  Solve the linear system.
	status_ = umfpack_dl_solve(UMFPACK_A, col_ptr, row_ptr, 
		val_ptr, X, B, Numeric_, control_, info_);
	
	// report and check
	if(status_!=UMFPACK_OK)umfpack_di_report_status(control_, status_);
	assert(status_==UMFPACK_OK);

	// check result
	assert(arma::norm(mvp(x)-b)/arma::norm(b)<1e-3);

	// return output
	return x;
}


