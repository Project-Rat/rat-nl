# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(BTF_PKGCONF BTF)

# Include dir
find_path(BTF_INCLUDE_DIR
  NAMES btf.h
  PATHS ${BTF_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(BTF_LIBRARY
  NAMES btf
  PATHS ${BTF_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(BTF_PROCESS_INCLUDES BTF_INCLUDE_DIR)
set(BTF_PROCESS_LIBS BTF_LIBRARY)
libfind_process(BTF)
