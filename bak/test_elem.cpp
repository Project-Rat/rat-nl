// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>

// common headers
#include "rat/common/elements.hh"

// fmm headers
#include "rat/mlfmm/mlfmm.hh"

// solver
#include "nonlinsolver.hh"

// main function
int main(){
	// settings
	const arma::uword num_gauss_hex = 2;
	const rat::fltp D = RAT_CONST(1); // box side length
	const rat::fltp tol = RAT_CONST(1e-4);
	const rat::fltp dxtrap = RAT_CONST(0.0);

	// define box corners
	const arma::Col<rat::fltp>::fixed<3> R0 = {-D/2-dxtrap,-D/2-dxtrap,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R1 = {+D/2,-D/2,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R2 = {-D/2,+D/2,-D/2}; 
	// const arma::Col<rat::fltp>::fixed<3> R3 = {-D/2,+D/2,-D/2};
	const arma::Col<rat::fltp>::fixed<3> R4 = {-D/2-dxtrap,-D/2-dxtrap,+D/2}; 
	// const arma::Col<rat::fltp>::fixed<3> R5 = {+D/2,-D/2,+D/2}; 
	// const arma::Col<rat::fltp>::fixed<3> R6 = {+D/2,+D/2,+D/2}; 
	// const arma::Col<rat::fltp>::fixed<3> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix with nodes
	arma::Mat<rat::fltp> Rn(3,4);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R4;
	// Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// get mesh (1 element)
	const arma::Col<arma::uword> n = arma::regspace<arma::Col<arma::uword> >(0,3);

	// create jacobian
	const arma::Mat<fltp> gp = rat::cmn::Tetrahedron::create_gauss_points(num_gauss_hex);
	const arma::Mat<fltp> Rq = gp.rows(0,2);
	const arma::Row<fltp> wq = gp.row(3);

	// const arma::Mat<fltp> Rg = rat::cmn::Tetrahedron::shape_function(Rq.rows(0,2));
	// const arma::Mat<fltp> J = rat::cmn::Tetrahedron::trilinear_jacobian(Rn,Rq.rows(0,2));
	const arma::Mat<fltp> Rc = rat::cmn::Tetrahedron::quad2cart(Rn,Rq);

	const arma::Mat<fltp> dN = rat::cmn::Tetrahedron::shape_function_derivative(Rq);
	const arma::Mat<fltp> Vq = rat::cmn::Tetrahedron::nedelec_hdiv_shape_function(Rq); // check
	// const arma::Mat<fltp> Vq = rat::cmn::Tetrahedron::nedelec_hcurl_shape_function(Rq); // check

	const arma::Mat<fltp> J = rat::cmn::Tetrahedron::shape_function_jacobian(Rn.cols(n),dN);

	const arma::Mat<fltp> Jdet = rat::cmn::Tetrahedron::jacobian2determinant(J);

	const arma::Mat<fltp> Jinv = rat::cmn::Tetrahedron::invert_jacobian(J,Jdet);
	// // const arma::Col<fltp> dNcart = arma::reshape((arma::reshape(Jinv,3,3)*arma::reshape(dN,8,3).t()).t(),24,1);
	const arma::Mat<fltp> Vc1 = rat::cmn::Tetrahedron::quad2cart_covariant_piola(Vq,Jinv);
	const arma::Mat<fltp> Vc2 = rat::cmn::Tetrahedron::quad2cart_contravariant_piola(Vq,J,Jdet);
	const arma::Row<fltp> Af = rat::cmn::Triangle::calc_area(Rn,cmn::Tetrahedron::get_faces().t());

	// const arma::Mat<fltp> Nf = cmn::Tetrahedron::calc_face_normals(Rn,n);

	// // check
	// arma::Col<fltp>::fixed<3> Bbg = {0.1,0.2,-0.3};
	// const arma::Mat<fltp> Nf = -rat::cmn::Quadrilateral::calc_face_normals(Rn,cmn::Tetrahedron::get_faces().t());
	// const arma::Row<fltp> Bn = arma::sum(Nf.each_col()%Bbg);
	// arma::Mat<fltp> Bq(3,num_gauss_hex*num_gauss_hex*num_gauss_hex,arma::fill::zeros);
	// for(arma::uword i=0;i<6;i++){
	// 	// Bq += (Bn(i)*std::sqrt(Af(i))/2)*Vc2.rows(i*3,(i+1)*3-1);
	// 	Bq += (Bn(i)*Af(i)/4)*(Vc2.rows(i*3,(i+1)*3-1));
	// }

	std::cout<<Rn.t()<<std::endl;

	// std::cout<<Jdet<<std::endl;
	// std::cout<<wq<<std::endl;


	std::cout<<Rc.t()<<std::endl;
	// std::cout<<Vq<<std::endl;
	
	// std::cout<<Rc<<std::endl;
	// std::cout<<Vc1<<std::endl;
	std::cout<<Vc2.t()<<std::endl;

	// std::cout<<Nf<<std::endl;

	// std::cout<<Bq<<std::endl;

	// figure; for i=1:12; subplot(3,4,i); title(i); hold on; xlabel('xi'); ylabel('nu'); zlabel('mu'); axis equal; quiver3(Rn(:,1),Rn(:,2),Rn(:,3),Vc(3*(i-1)+1,:)',Vc(3*(i-1)+2,:)',Vc(3*(i-1)+3,:)'); plot3(Rn(:,1),Rn(:,2),Rn(:,3),'rs'); end;
	// figure; plot3(Rn(:,1),Rn(:,2),Rn(:,3),'rs',Rq(:,1),Rq(:,2),Rq(:,3),'go'); hold on; for i=1:4; quiver3(Rq(:,1),Rq(:,2),Rq(:,3),Vc(:,3*(i-1)+1),Vc(:,3*(i-1)+2),Vc(:,3*(i-1)+3)); end; axis equal;
	return 0;
}