// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef SP_PREC_CHOLMOD_HH
#define SP_PREC_CHOLMOD_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "extra.hh"
#include "spprec.hh"
#include "cholmod.h"

// shared pointer definition
typedef std::shared_ptr<class SpPrecCholMod> ShSpPrecCholModPr;

// export CHOLMOD_USE_GPU=1

// UMFpack preconditioner
class SpPrecCholMod: public SpPrec{
	// properties
	private:
		cholmod_sparse *A_ = NULL;
		cholmod_factor *L_ = NULL;
		cholmod_common c_;

		// long int versions of indexing arrays
		arma::Row<long int> li_idx_col_c_;
		arma::Row<long int> li_idx_row_;

	public:
		// constructors
		SpPrecCholMod();
		
		// destructor
		~SpPrecCholMod();

		// factory
		static ShSpPrecCholModPr create();

		// solver
		void analyse();
		void factorise();
		arma::Col<double> solve(const arma::Col<double> &b);
};

#endif