# include libfind
include(LibFindMacros)

# Dependencies

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(CCOLAMD_PKGCONF CCOLAMD)

# Include dir
find_path(CCOLAMD_INCLUDE_DIR
  NAMES ccolamd.h
  PATHS ${CCOLAMD_PKGCONF_INCLUDE_DIR}
)

# Finally the library itself
find_library(CCOLAMD_LIBRARY
  NAMES ccolamd
  PATHS ${CCOLAMD_PKGCONF_LIBRARY_DIR}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(CCOLAMD_PROCESS_INCLUDES CCOLAMD_INCLUDE_DIR)
set(CCOLAMD_PROCESS_LIBS CCOLAMD_LIBRARY)
libfind_process(CCOLAMD)
