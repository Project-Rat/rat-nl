// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>

// main function
int main(){
	// settings
	const arma::uword num_rows = 800;
	const arma::uword num_cols = 1000;
	const double filling_fraction = 0.1;

	// create sparse matrix
	const arma::SpMat<double> M1 = arma::sprandu(num_rows,num_cols,filling_fraction);
	const arma::SpMat<double> M2 = arma::sprandu(num_cols,num_rows,filling_fraction);

	// create vector
	const arma::Col<double> b(num_rows, arma::fill::value(1.0));

	// create timer
	arma::wall_clock timer; timer.tic();

	// matrix matrix vector product
	const arma::Col<double> c = M1*M2*b;

	// show time
	std::cout<<"regular mmvp time = "<<timer.toc()<<std::endl;

	// reset timer
	timer.tic();

	// matrix matrix vector product with brackets
	const arma::Col<double> d = M1*(M2*b);

	// show time
	std::cout<<"bracketed mmvp time = "<<timer.toc()<<std::endl;

	// done
	return 0;
}
