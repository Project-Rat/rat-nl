// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "mgnchargesurface.hh"

#include "rat/common/elements.hh"
#include "rat/common/gauss.hh"

// code specific to Rat
namespace rat{namespace nl{

	// constructor
	MgnChargeSurface::MgnChargeSurface(){

	}

	// constructor with input
	MgnChargeSurface::MgnChargeSurface(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &f, 
		const arma::Row<fltp> &sigma_s){
		
		// set sources 
		set_mesh(Rn,f); 
		set_magnetic_charge(sigma_s);
	}

	// factory
	ShMgnChargeSurfacePr MgnChargeSurface::create(){
		//return ShIListPr(new IList);
		return std::make_shared<MgnChargeSurface>();
	}

	// factory with input
	ShMgnChargeSurfacePr MgnChargeSurface::create(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &f, 
		const arma::Row<fltp> &sigma_s){

		//return ShIListPr(new IList);
		return std::make_shared<MgnChargeSurface>(Rn,f,sigma_s);
	}

	// get number of dimensions
	arma::uword MgnChargeSurface::get_num_dim() const{
		return num_dim_;
	}

	// set coordinate vectors
	void MgnChargeSurface::set_mesh(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &f){
		
		// set coordinate vectors
		Rn_ = Rn; f_ = f;

		// set number of sources
		num_sources_ = f_.n_cols;

		// create source and target pointss
		arma::Mat<fltp> Rt(3,num_sources_);
		for(arma::uword i=0;i<num_sources_;i++)
			Rt.col(i) = arma::mean(Rn_.cols(f_.col(i)),1);

		// set targets
		set_target_coords(Rt);
	}

	// set number of solar masses
	void MgnChargeSurface::set_magnetic_charge(const arma::Row<fltp> &sigma_s){
		sigma_s_ = sigma_s;
	}

	// sorting function
	void MgnChargeSurface::sort_sources(const arma::Row<arma::uword> &sort_idx){
		// sort sources
		f_ = f_.cols(sort_idx);
		sigma_s_ = sigma_s_.cols(sort_idx);
	}

	// unsorting function
	void MgnChargeSurface::unsort_sources(const arma::Row<arma::uword> &sort_idx){
		// sort sources
		f_.cols(sort_idx) = f_;
		sigma_s_.cols(sort_idx) = sigma_s_;
	}

	// count number of sources stored
	arma::uword MgnChargeSurface::num_sources() const{
		return num_sources_;
	}

	// method for getting all coordinates
	arma::Mat<fltp> MgnChargeSurface::get_source_coords() const{
		return Rt_;
	}

	// method for getting all coordinates
	arma::Mat<fltp> MgnChargeSurface::get_charge() const{
		return sigma_s_;
	}

	// get elmenet size
	fltp MgnChargeSurface::element_size() const{
		return arma::max(cmn::Extra::vec_norm(Rn_.cols(f_.row(3)) - Rn_.cols(f_.row(0))));
	}

	// setup source to multipole matrices
	void MgnChargeSurface::setup_source_to_multipole(
		const arma::Mat<fltp> &dR, 
		const fmm::ShSettingsPr &stngs){

		// get number of expansions
		const int num_exp = stngs->get_num_exp();

		// memory efficient implementation (default)
		if(stngs->get_memory_efficient_s2m()){
			dR_ = dR;
		}

		// maximize speed over memory efficiency
		else{
			// set number of expansions and setup matrix
			M_J_.set_num_exp(num_exp);
			M_J_.calc_matrix(-dR);
		}
	}

	// get multipole contribution of the sources with indices
	// the contributions of the sources are already summed
	void MgnChargeSurface::source_to_multipole(
		arma::Mat<std::complex<fltp> > &Mp,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source,
		const fmm::ShSettingsPr &stngs) const{

		// check input
		assert(first_source.n_elem==last_source.n_elem);
		assert(!Mp.is_empty());

		// get number of expansions
		const int num_exp = stngs->get_num_exp();

		// memory efficient implementation (default)
		if(stngs->get_memory_efficient_s2m()){		
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,stngs->get_parallel_s2m(),[&](arma::uword i, int){
				// calculate contribution of currents and return calculated multipole
				fmm::StMat_So2Mp_J M_J;
				M_J.set_num_exp(num_exp);
				M_J.calc_matrix(-dR_.cols(first_source(i),last_source(i)));

				// apply matrix
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_J.apply(
					sigma_s_.cols(first_source(i),last_source(i)));
			});
		}

		// faster less memory efficient implementation
		else{
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,stngs->get_parallel_s2m(),[&](arma::uword i, int){
				// add child source contribution to this multipole
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_J_.apply(
					sigma_s_.cols(first_source(i),last_source(i)),
					first_source(i),last_source(i));
			});
		}
	}

	// localpole to target setup function
	void MgnChargeSurface::setup_localpole_to_target(
		const arma::Mat<fltp> &dR, 
		const rat::fmm::ShSettingsPr &stngs){
		
		const int num_exp = stngs->get_num_exp();

		// check input
		assert(dR.is_finite());

		// memory efficient implementation (default)
		if(stngs->get_memory_efficient_l2t()){
			dRl2p_ = dR;
		}

		// maximize computation speed
		else{
			// gravitational potential
			if(has('S')){
				// calculate matrix for all target points
				M_A_.set_num_exp(num_exp);
				M_A_.calc_matrix(-dR);
			}
		}
	}

	// localpole to target function
	void MgnChargeSurface::localpole_to_target(
		const arma::Mat<std::complex<fltp> > &Lp, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target, 
		const arma::uword num_dim, const rat::fmm::ShSettingsPr &stngs){

		// set number of expansions
		const int num_exp = stngs->get_num_exp();

		// memory efficient implementation (default)
		// note that this method is called many times in parallel
		// do not re-use the class property of M_A and M_H
		if(stngs->get_memory_efficient_l2t()){
			// check if dR was set
			assert(!dRl2p_.is_empty());

			// gravitational potential
			if(has('S')){
				// create temporary storage for vector potential
				arma::Row<fltp> S(num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,stngs->get_parallel_l2t(),[&](arma::uword i, int){
					// calculate matrix for these target points
					fmm::StMat_Lp2Ta M_A;
					M_A.set_num_exp(num_exp);
					M_A.calc_matrix(-dRl2p_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					S.cols(first_target(i),last_target(i)) += M_A.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1));
				});

				// add to self
				add_field('S',S,true);
			}
		}

		// maximize computation speed using pre-calculated matrix
		else{
			// electric field
			if(has('S')){
				// check if localpole to target matrix was set
				assert(!M_A_.is_empty());

				// create temporary storage for vector potential
				arma::Row<fltp> S(num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,stngs->get_parallel_l2t(),[&](arma::uword i, int){
					S.cols(first_target(i),last_target(i)) += M_A_.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1), first_target(i), last_target(i));
				});

				// add to self
				add_field('S',S,true);
			}
		}

	}

	// direct calculation of vector potential or magnetic 
	// field for all sources at all target points
	void MgnChargeSurface::calc_direct(
		const fmm::ShTargetsPr &tar, 
		const fmm::ShSettingsPr &stngs) const{
		rat_throw_line("not yet implemented");
		// // get target coordinates
		// const arma::Mat<fltp> Rt = tar->get_target_coords();

		// // allocate output
		// arma::Row<fltp> S(Rt.n_cols);

		// // forward calculation of vector potential to extra
		// if(tar->has('S')){
		// 	// walk over targets
		// 	cmn::parfor(0,Rt.n_cols,stngs->get_parallel_s2t(),[&](arma::uword i, int) { // second int is CPU number
		// 		// relative position
		// 		const arma::Mat<fltp> dR = Rt.col(i) - Rs_.each_col();

		// 		// distance
		// 		const arma::Row<fltp> rho = arma::sqrt(arma::sum(dR%dR,0));

		// 		// check distance
		// 		const arma::Col<arma::uword> id = arma::find(rho>0);

		// 		// calculate contribution of each source to V
		// 		S(i) = arma::accu(sigma_s_(id)/rho(id));
		// 	});

		// 	// set
		// 	tar->add_field('S',S,false);
		// }
	}

	// source to target kernel
	void MgnChargeSurface::source_to_target(
		const fmm::ShTargetsPr &tar, 
		const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target, 
		const fmm::ShSettingsPr &stngs) const{

		// calculation of gravitational potential
		if(tar->has('S')){
			// allocate
			arma::Row<fltp> S(tar->num_targets(),arma::fill::zeros);

			// create gauss points
			const cmn::Gauss gs(num_gauss_);
			const arma::Row<fltp>& wg = gs.get_weights();
			const arma::Row<fltp>& xg = gs.get_abscissae();
			arma::Mat<fltp> Rqf(2,num_gauss_*num_gauss_);
			arma::Row<fltp> wqf(num_gauss_*num_gauss_);
			for(arma::uword i=0;i<num_gauss_;i++){
				for(arma::uword j=0;j<num_gauss_;j++){
					const arma::uword idx = i*num_gauss_ + j;
					wqf(idx) = wg(i)*wg(j)/4;
					Rqf(0,idx) = xg(i);
					Rqf(1,idx) = xg(j);
				}
			}

			// create gauss points for targets
			arma::Mat<fltp> Rg(3,Rt_.n_cols*Rqf.n_cols);
			for(arma::uword j=0;j<Rt_.n_cols;j++)
				Rg.cols(j*num_gauss_,(j+1)*num_gauss_-1) = cmn::Quadrilateral::quad2cart(Rn_.cols(f_.col(j)),Rqf);

			// walk over source nodes
			cmn::parfor(0,target_list.n_elem,stngs->get_parallel_s2t(),[&](arma::uword i, int){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());

				// walk over target nodes
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get source node
					const arma::uword source_idx = source_list(i)(j);

					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// sources 
					for(arma::uword m=fs;m<=ls;m++){
						// calculate face normal unit vector
						const arma::Col<fltp>::fixed<3> zeta = cmn::Quadrilateral::calc_face_normals(Rn_,f_.col(m)); // unit vector normal to face

						// walk over source edges
						for(arma::uword j=0;j<f2e_.n_rows;j++){
							// get edge index
							const arma::uword eid = f2e_(j,m);

							// get face nodes
							arma::Mat<fltp>::fixed<3,2> Re = Rn_.cols(e_.col(eid));

							// make edge clockwise with respect to face
							if(ecw_(j,tfid)==0)Re = arma::fliplr(Re);

							// edge unit vector
							const arma::Col<fltp>::fixed<3> Vg = arma::diff(Re,1,1);
							const arma::Col<fltp>::fixed<3> xi = Vg.each_row()/cmn::Extra::vec_norm(Vg); // unit vector along edge

							// plane unit vector
							const arma::Col<fltp>::fixed<3> nu = cmn::Extra::cross(xi,zeta); // unit vector across the edge

							// walk over target points
							for(arma::uword k=ft;k<=lt;k++){
								// walk over gauss points
								for(arma::uword m=0;m<num_gauss_;m++){
									// relative position of start and end point
									const arma::Col<fltp>::fixed<3> R0 = Rg.col(k*num_gauss_ + m) - Re.col(0);
									const arma::Col<fltp>::fixed<3> R1 = Rg.col(k*num_gauss_ + m) - Re.col(1);

									// distance between points
									const fltp r0 = arma::as_scalar(cmn::Extra::vec_norm(R0));
									const fltp r1 = arma::as_scalar(cmn::Extra::vec_norm(R1));

									// in local coordinates
									const fltp rxi0 = arma::as_scalar(cmn::Extra::dot(xi,R0));
									const fltp rxi1 = arma::as_scalar(cmn::Extra::dot(xi,R1));
									const fltp rnu = arma::as_scalar(cmn::Extra::dot(nu,R0)); 
									const fltp rzeta = arma::as_scalar(cmn::Extra::dot(zeta,R0)); 

									// add edge contribution to phi
									S(k) += wg(m)*rnu*(rxi1/(r1 + std::abs(rzeta)) - rxi0/(r0 + std::abs(rzeta)));
								}
							}

						}
					}
				}
			});

			// set field to targets
			tar->add_field('S',S,true);
		}

	}

}}