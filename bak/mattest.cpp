// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>


arma::Mat<double> mvp(const arma::Mat<double>&A, const arma::Mat<double>&B){
	return A*B;
}


// main function
int main(){
	// settings
	const arma::uword num_rows = 1000;
	const arma::uword num_cols = 1000;
	const arma::uword idx1 = 200;
	const arma::uword idx2 = 300;
	const arma::uword idx3 = 500;
	const arma::uword idx4 = 600;

	// create sparse matrix
	const arma::Mat<double> M1 = arma::randu(num_rows,num_cols);
	const arma::Mat<double> M2 = arma::randu(num_cols,num_rows);
	
	// create timer
	arma::wall_clock timer; timer.tic();
	const arma::Mat<double> M3 = M1.cols(idx1,idx2)*M2.rows(idx3,idx4);
	std::cout<<"direct mm: "<<timer.toc()<<std::endl;

	timer.tic();
	const arma::Mat<double> M4 = mvp(M1.cols(idx1,idx2), M2.rows(idx3,idx4));
	std::cout<<"fun mm: "<<timer.toc()<<std::endl;


	// done
	return 0;
}
