// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_CURRENT_MESH_HH
#define FMM_CURRENT_MESH_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// common headers
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// mlfmm headers
#include "settings.hh"
#include "sources.hh"
#include "savart.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class CurrentMesh> ShCurrentMeshPr;
	typedef arma::field<ShCurrentMeshPr> ShCurrentMeshPrList;

	// hexahedron mesh with volume elements
	// is derived from the sources class (still partially virtual)
	class MgnSurface: virtual public Sources{
		// properties
		protected:
			// node locations
			arma::Mat<fltp> Rn_;

			// all faces
			arma::Mat<arma::uword> f_;

			// face charge
			arma::Row<fltp> sigma_;


		// methods
		public:
			// constructors
			CurrentMesh();
			CurrentMesh(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &f);

			// factory
			static ShCurrentMeshPr create();
			static ShCurrentMeshPr create(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &f);

			// setting a hexahedronal mesh with volume elements
			void set_mesh(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &f);



			// source to multipole
			void setup_source_to_multipole(
				const arma::Mat<fltp> &dR, 
				const ShSettingsPr &stngs) override;
			void source_to_multipole(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const ShSettingsPr &stngs) const override;
			
			// direct field calculation for both A and B
			void calc_direct(
				const ShTargetsPr &tar, 
				const ShSettingsPr &stngs) const override;
			void source_to_target(
				const ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const ShSettingsPr &stngs) const override;
	};

}}

#endif
