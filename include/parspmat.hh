// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef NL_PAR_SPMAT_HH
#define NL_PAR_SPMAT_HH

#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

#include <rat/common/typedefs.hh>

// code specific to Raccoon
namespace rat{namespace nl{

	// hb curve class template
	class ParSpMat{
		// properties
		protected:
			// data
			arma::field<arma::SpMat<fltp> > M_;

			// indexing
			arma::Col<arma::uword> block_indices_;

			// counters
			arma::uword nnz_;
			arma::uword n_rows_;
			arma::uword n_cols_;

		// methods
		public:
			// default constructor
			ParSpMat(){};

			// same constructor as armadillo SpMat
			ParSpMat(
				const bool add_values, 
				const arma::Mat<arma::uword> &locations, 
				const arma::Col<fltp> &val, 
				const arma::uword num_rows, 
				const arma::uword num_cols, 
				const bool sort_locations = true, 
				const bool check_for_zeros = true);
			explicit ParSpMat(const arma::SpMat<fltp> &M);

			// access data
			const arma::field<arma::SpMat<fltp> >& get_matrix()const;
			const arma::Col<arma::uword>& get_block_indices()const;

			// some useful functions
			bool empty()const;
			arma::uword n_nonzero()const;
			arma::uword n_rows()const;
			arma::uword n_cols()const;

			// setup function
			void setup(const arma::SpMat<fltp>&M);

			// matrix vector product
			arma::Col<fltp> operator*(const arma::Col<fltp> &x)const;
	};

}}

#endif