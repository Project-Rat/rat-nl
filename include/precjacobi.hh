// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef NL_PREC_JACOBI_HH
#define NL_PREC_JACOBI_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "preconditioner.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class PrecJacobi> ShPrecJacobiPr;

	// UMFpack preconditioner
	class PrecJacobi: public Preconditioner{
		// properties
		protected:
			arma::Col<rat::fltp> diag_;

		public:
			// constructors
			PrecJacobi();

			// destructor
			~PrecJacobi();

			// factory
			static ShPrecJacobiPr create();

			// set matrix
			void set_matrix(
				const arma::Col<arma::uword> &idx_col, 
				const arma::Col<arma::uword> &idx_row, 
				const arma::Col<rat::fltp> &val, 
				const arma::uword num_size) override;

			// matrix vector product
			arma::Col<rat::fltp> mvp(
				const arma::Col<rat::fltp> &x) const override;

			// solver
			void analyse() override;
			void factorise() override;
			arma::Col<rat::fltp> solve(
				const arma::Col<rat::fltp> &b, 
				const rat::fltp delta) override;

			// display stats
			void display()const override;
	};

}}

#endif
