// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef NL_PREC_UMFPACK_HH
#define NL_PREC_UMFPACK_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "preconditioner.hh"
#include "umfpack.h"

// code specific to Raccoon
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class PrecUMFPack> ShPrecUMFPackPr;

	// UMFpack preconditioner
	class PrecUMFPack: public Preconditioner{
		// properties
		protected:
			// matrix size
			arma::uword num_size_;
			arma::uword num_nnz_;

			// matrix entries in compressed column storage format (CCS)
			arma::Col<SuiteSparse_long> idx_col_c_;
			arma::Col<SuiteSparse_long> idx_row_;
			arma::Col<rat::fltp> val_;

			// umfpack data
			SuiteSparse_long status_ = 0;
			double control_ [UMFPACK_CONTROL];
			double info_ [UMFPACK_INFO];
			void *Symbolic_ = NULL;
			void *Numeric_ = NULL;

		public:
			// constructors
			PrecUMFPack();

			// destructor
			~PrecUMFPack();

			// factory
			static ShPrecUMFPackPr create();

			// drop tolerance
			void set_droptol(const rat::fltp droptol);
			
			// set matrix
			void set_matrix(
				const arma::Col<arma::uword> &idx_col, 
				const arma::Col<arma::uword> &idx_row, 
				const arma::Col<rat::fltp> &val, 
				const arma::uword num_size) override;

			// matrix vector product
			arma::Col<rat::fltp> mvp(
				const arma::Col<rat::fltp> &x) const override;

			// solver
			void analyse() override;
			void factorise() override;
			arma::Col<rat::fltp> solve(
				const arma::Col<rat::fltp> &b, 
				const rat::fltp delta) override;

			// display stats
			void display()const override;
	};

}}

#endif
