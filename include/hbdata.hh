// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef NL_HB_DATA_HH
#define NL_HB_DATA_HH

#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

#include "rat/common/typedefs.hh"
#include "rat/common/node.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class HBData> ShHBDataPr;
	typedef arma::field<ShHBDataPr> ShHBDataPrList;

	// hb curve class template
	class HBData{
		// properties
		protected:
			// HB-data
			arma::Col<fltp> Hinterp_; // [A/m]
			arma::Col<fltp> Binterp_; // [T]

			// iron filling fraction
			fltp ff_ = RAT_CONST(1.0);

			// mono spacing (determined when H and B are set)
			bool mono_spaced_h_ = false;
			bool mono_spaced_b_ = false;

			// finite difference settings
			const fltp dH_ = RAT_CONST(0.1);
			const fltp dB_ = RAT_CONST(1e-6);

		// methods
		public:
			// constructor
			HBData();
			HBData(
				const arma::Col<fltp> &Hinterp, 
				const arma::Col<fltp> &Binterp, 
				const fltp ff = 1.0);

			// factory
			static ShHBDataPr create();
			static ShHBDataPr create(
				const arma::Col<fltp> &Hinterp, 
				const arma::Col<fltp> &Binterp, 
				const fltp ff = 1.0);

			// access data
			const arma::Col<fltp>& get_magnetic_field_table()const;
			const arma::Col<fltp>& get_flux_density_table()const;

			// default datasets
			void set_team13();
			void set_armco_hot_rolled();
			void set_armco_cold_rolled();

			// setters
			void set_filling_fraction(const fltp ff);

			// adjusted interpolation flux density for filling fraction
			arma::Col<fltp> calc_Bff()const;

			// interpolation functions
			arma::Row<fltp> calc_magnetic_flux_density(const arma::Row<fltp> &Hmag) const;
			arma::Row<fltp> calc_magnetic_field(const arma::Row<fltp> &Bmag) const;
			arma::Row<fltp> calc_magnetisation(const arma::Row<fltp> &Hmag) const;

			// linearization
			arma::Row<fltp> calc_reluctivity(const arma::Row<fltp> &Bmag) const;
			arma::Row<fltp> calc_reluctivity_diff(const arma::Row<fltp> &Bmag) const;
			arma::Row<fltp> calc_susceptibility(const arma::Row<fltp> &Hmag) const;
			arma::Row<fltp> calc_susceptibility_diff(const arma::Row<fltp> &Hmag) const;

			// permeability
			arma::Row<fltp> calc_permeability(const arma::Row<fltp> &Hmag)const;
			arma::Row<fltp> calc_relative_permeability(const arma::Row<fltp> &Hmag)const;
	};

}}

#endif