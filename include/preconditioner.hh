// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef NL_PRECONDITIONER_HH
#define NL_PRECONDITIONER_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "rat/common/extra.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class Preconditioner> ShPreconditionerPr;

	// sparse preconditioner template class
	class Preconditioner{
		// settings
		protected:
			// log for preconditioner
			cmn::ShLogPr lg_ = cmn::NullLog::create();

			// status
			bool factorised_ = false;
			bool analysed_ = false;

		// methods
		public:
			// destructor
			virtual ~Preconditioner(){};

			// set logger
			void set_logger(const rat::cmn::ShLogPr& lg);

			// request that the matrix is row sorted
			virtual bool is_row_sorted() const;

			// set matrix from triplets
			virtual void set_matrix(
				const arma::Col<arma::uword> &idx_col, 
				const arma::Col<arma::uword> &idx_row, 
				const arma::Col<rat::fltp> &val, 
				const arma::uword num_size) = 0;

			// matrix vector product
			virtual arma::Col<rat::fltp> mvp(
				const arma::Col<rat::fltp> &x) const = 0;

			// solver
			virtual void analyse() = 0;
			virtual void factorise() = 0;
			virtual arma::Col<rat::fltp> solve(
				const arma::Col<rat::fltp> &b, 
				const rat::fltp delta) = 0;

			// display stats
			virtual void display()const = 0;

			// get state
			bool get_analyzed()const;
			bool get_factorised()const;
	};

}}

#endif