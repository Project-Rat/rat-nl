// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef NL_MAGNETIC_POTENTIALS_HH
#define NL_MAGNETIC_POTENTIALS_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/error.hh"

// mlfmm headers
#include "rat/mlfmm/targetpoints.hh"
#include "rat/mlfmm/stmat.hh"

// code specific to Rat
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class MgnPotentials> ShMgnPotentialsPr;

	// collection of charges for electric field calculation
	// is derived from the sources class
	class MgnPotentials: public fmm::TargetPoints{
		// properties
		protected:
			// multipole operation matrices
			rat::fmm::StMat_Lp2Ta M_A_; // for vector potential
			rat::fmm::StMat_Lp2Ta_Grad M_H_; // for vector potential
			arma::Mat<fltp> dRl2p_;

		// methods
		public:
			// constructor
			MgnPotentials();
			explicit MgnPotentials(const arma::Mat<fltp> &Rt);

			// factory
			static ShMgnPotentialsPr create();
			static ShMgnPotentialsPr create(const arma::Mat<fltp> &Rt);

			// localpole to target virtual functions
			void setup_localpole_to_target(
				const arma::Mat<fltp> &dR, 
				const arma::uword num_dim,
				const rat::fmm::ShSettingsPr &stngs) override;
			void localpole_to_target(
				const arma::Mat<std::complex<fltp> > &Lp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::uword num_dim, 
				const rat::fmm::ShSettingsPr &stngs) override;
	};

}}

#endif
