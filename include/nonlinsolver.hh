// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef NL_SOLVER_HH
#define NL_SOLVER_HH

// general headers
#include <armadillo> 
#include <memory>
#include <thread> 

// common headerss
#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/log.hh"

// fmm headers
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentsources.hh"

// sundials general headers
#include <sundials/sundials_version.h>
#include <sundials/sundials_context.h>

// kinsol solver
#include <kinsol/kinsol.h>
// #include <kinsol/kinsol_spils.h> 

// sundials vector
#ifdef NVECTOR_PTHREADS
#include <nvector/nvector_pthreads.h>
#else
#include <nvector/nvector_serial.h>
#endif

// sundials linear solvers
#include <sunlinsol/sunlinsol_spbcgs.h>
#include <sunlinsol/sunlinsol_spgmr.h>
#include <sunlinsol/sunlinsol_spfgmr.h>
#include <sunlinsol/sunlinsol_sptfqmr.h>

// multipole methods
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/momentsources.hh"
#include "rat/mlfmm/mgncharges.hh"

// my headers
#include "hbdata.hh"
#include "preconditioner.hh"
#include "mgnpotentials.hh"
#include "mgncharges.hh"
#include "parspmat.hh"
#include "solvercache.hh"

// code specific to Nonlinear Solver
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class NonLinSolver> ShNonLinSolverPr;

	// The Non-Linear Magnetic Materials Solver class
	class NonLinSolver{
		// Enums
		public:
			// linear solver types
			enum class LinsolType{FGMRES,GMRES,BCGS,TFQMR};

			// preconditioner types
			enum class PreconditionerType{SUPERLU,CHOLMOD,UMFPACK,JACOBI,QDLDL,PARU};

		// DATA STRUCTURES
		protected:
			// list of timings
			struct Timings{
				// setup time
				fltp ms2t_setup_ = RAT_CONST(0.0);
				fltp mf2n_setup_ = RAT_CONST(0.0);
				fltp me2f_setup_ = RAT_CONST(0.0);
				fltp mpre_setup_ = RAT_CONST(0.0);
				fltp mlfmm_setup_ = RAT_CONST(0.0);

				// specific parts of code
				fltp mlfmm_ = RAT_CONST(0.0);
				fltp stiffness_mvp_ = RAT_CONST(0.0);
				fltp source2target_ = RAT_CONST(0.0);
				fltp flux_leakage_ = RAT_CONST(0.0);
				fltp factorization_ = RAT_CONST(0.0);
				fltp flux_density_ = RAT_CONST(0.0);
				fltp reluctance_ = RAT_CONST(0.0);
				fltp dreluctance_ = RAT_CONST(0.0);

				// functions
				fltp system_fn_ = RAT_CONST(0.0);
				fltp jtv_fn_ = RAT_CONST(0.0);
				fltp prec_setup_fn_ = RAT_CONST(0.0);
				fltp prec_solve_fn_ = RAT_CONST(0.0);

				// total time for solving
				fltp solve_ = RAT_CONST(0.0);
			};

		// SOLVER SETTINGS
		protected:
			// logger
			rat::cmn::ShLogPr lg_ = rat::cmn::NullLog::create();

			// global strategy
			// linesearch can help with highly non-linear systems
			// the other two options are not implemented in Rat-NL
			int globalstrategy_ = KIN_LINESEARCH; // KIN_NONE, KIN_LINESEARCH, (KIN_PICARD, KIN_FP)
			
			// maximum number of non-linear iterations
			int max_iter_ = 500;

			// the maximum number of nonlinear iterations that can be 
			// performed between calls to the preconditioner or Jacobian setup function
			int msbset_ = 10; // 0 for default (10)

			// krylov subspace size for linear solver
			int maxl_ = 50; // 5 is the sundials default

			// krylov subspace size for linear solver during dsolve
			int dmaxl_ = 1000;

			// max number of restarts for GMRES linear solver
			int maxlrst_ = 0; 

			// max number of restarts for GMRES linear solver during dsolve
			int dmaxlrst_ = 0;

			// linear solver
			// FGMRES is more tolerant to small changes for example
			// due to limited numerical precision and race-conditions
			// on the GPU
			LinsolType linear_solver_type_ = LinsolType::FGMRES;

			// preconditioner type (is set by constructor but can be changed after creation)
			PreconditionerType preconditioner_type_;

			// sundials preconditioner setting
			const int preconditioner_side_ = SUN_PREC_RIGHT; // do not change!

			// regularization parameter
			// the preconditioner matrix is singular by design 
			// due to the used edge method this parameter is 
			// added to the diagonal to make the system solveable
			fltp regularization_parameter_ = RAT_CONST(1e-2);

			// eta choice
			int eta_choice_ = KIN_ETACHOICE1; // KIN_ETACHOICE1, KIN_ETACHOICE2, KIN_ETACONSTANT

			// eta value when using KIN_ETACONSTANT
			sunrealtype eta_const_ = 1e-4;

			// tolerances
			// bool use_gpu_ = false;
			// sunrealtype fnormtol_ = std::pow(arma::Datum<fltp>::eps,1.0/3); // 0 for default
			// sunrealtype scsteptol_ = std::pow(arma::Datum<fltp>::eps,2.0/3); // 0 for default

			// tolerances
			bool use_gpu_ = true;
			std::set<int> gpu_devices_;
			// sunrealtype fnormtol_ = std::pow(arma::Datum<cufltp>::eps,1.0/3); // 0 for default
			// sunrealtype scsteptol_ = 0.1*std::pow(arma::Datum<cufltp>::eps,2.0/3); // 0 for default
			sunrealtype fnormtol_double_ = 1e-7; // double precision (CPU)
			sunrealtype fnormtol_single_ = 1e-6; // single precision (GPU)
			sunrealtype scsteptol_double_ = 1e-12; // double precision (CPU)
			sunrealtype scsteptol_single_ = 1e-11; // single precision (GPU)

			// tolerance for linear solver during dsolve
			sunrealtype dlinsoltol_ = 1e-6;

			// number of gauss points for sources and targets 
			// on surface when using hexahedral mesh
			arma::uword num_gauss_quadrilateral_ = 5llu;
			arma::uword num_gauss_quadrilateral_self_ = 5llu;
			arma::uword num_gauss_hexahedron_ = 2llu;

			// number of gauss points for sources and 
			// targets on surface when using tetrahedral mesh
			// not that data tables exist only for certain values
			arma::uword num_gauss_triangle_ = 5llu; 
			arma::uword num_gauss_triangle_self_ = 5llu;
			arma::uword num_gauss_tetrahedron_ = 2llu;

			// tolerance for merging nodes during mesh setup
			// nodes spaced below this distance are combined into
			// a single node during solving
			fltp node_merge_tol_ = RAT_CONST(1e-8); // [m]

			// calculate source to target using (semi) analytical integrals
			// this is usually slower than numerical (depending on 
			// number of gauss points used) but more accurate
			// note that the singular self-field integrals are
			// always calculated analytically. When enabled
			// the system is more likely to converge even
			// when elements have large aspect ratios
			bool use_analytical_integrals_ = true;

			// limit the refinement of the MLFMM to the smallest edge size
			// when enabled the number of source to target interactions increases
			// significantly, especially when elements have strange aspect ratios
			// when the s2t interactions are evaluated using analytical integrals
			// the system becomes more correct and is more likely to converge
			bool limit_refinement_ = false;

			// calculate the matrix vector product for the source to 
			// target matrix in parallel. This is currently not useful 
			// because it is not the bottleneck.
			bool use_parallel_s2t_matrix_ = false;

		// MESH DATA
		protected:
			// hb curves
			arma::field<ShHBDataPr> hb_curve_;

			// node coordinates
			arma::Mat<fltp> Rn_;

			// node merge indices
			arma::Row<arma::uword> original2merged_;

			// elements hexahedrons
			arma::Mat<arma::uword> n_;
			arma::Row<arma::uword> n2part_;

			// faces
			arma::Mat<arma::uword> f_;

			// edges
			arma::Mat<arma::uword> e_;

			// connection matrices
			arma::Mat<arma::uword> n2f_;
			arma::Mat<arma::uword> f2e_;
			arma::Mat<arma::uword> f2n_; 
			arma::Mat<arma::uword> e2f_;

			// clockwise
			arma::Mat<arma::uword> fcw_;
			arma::Mat<arma::uword> ecw_;

			// reference counters
			arma::Col<arma::uword> fcnt_;
			arma::Col<arma::uword> ecnt_;

			// indices of faces and elements at surface of mesh
			arma::Col<arma::uword> fs_;

			// indices of edges at the surface of the mesh
			arma::Col<arma::uword> es_;

			// face and edge dimensions
			arma::Row<fltp> elem_volume_;
			arma::Row<fltp> face_area_;
			arma::Row<fltp> edge_length_;

			// face normal vectors
			arma::Mat<fltp> Nf_;

			// self integrals of surface faces
			arma::Row<fltp> self_integrals_;

			// external field
			arma::Mat<fltp> Hext_;
			arma::Mat<fltp> dHext_;

			// gauss points for quadrilateral and their weights
			arma::Mat<fltp> Rqf_;
			arma::Row<fltp> wqf_;

			// gauss points for quadrilateral self field calculation
			arma::Mat<fltp> Rqs_;
			arma::Row<fltp> wqs_;

			// gauss points for hexahedron
			arma::Mat<fltp> Rqh_;
			arma::Row<fltp> wqh_;


		// SOLVER DATA
		protected:
			// sundials context
			SUNContext sunctx_;

			// KINSOL context
			void* kmem_ = NULL;

			// linear solver
			SUNLinearSolver LS_;

			// preconditioner
			ShPreconditionerPr preconditioner_;

			// source to target matrix
			arma::SpMat<fltp> Ms2t_;
			ParSpMat Ms2t_par_;

			// matrix for converting flux through faces to field in elements
			arma::SpMat<fltp> Mf2n_;
			// arma::SpMat<fltp> Mn2f_;

			// matrix for calculating flux through faces from vector potential at edges
			arma::SpMat<fltp> Me2f_;
			// arma::SpMat<fltp> Mf2e_;

			// volume associated with gauss points
			arma::Row<fltp> gauss_volume_;

			// right hand side
			arma::Col<fltp> rhs_;

			// reluctivity
			arma::Row<fltp> nu_;
			arma::Row<fltp> dnudB_;

			// reference to state
			// currently only used for dsolve
			arma::Row<fltp> Ae_;


		// MLFMM DATA
		protected:
			// settings for the mlfmm
			rat::fmm::ShSettingsPr stngs_;

			// the mlfmm itself
			rat::fmm::ShMlfmmPr mlfmm_ = NULL;

			// sources and targets
			ShMgnChargesPr src_;
			ShMgnPotentialsPr tar_;

		// mutable stuff
		protected:
			// timings
			mutable Timings time_;

			// counter for the last iteration shown in the display function
			mutable long int iter_displayed_ = -1;

		// methods
		public:

			// CONSTRUCTOR
			// constructor
			NonLinSolver();

			// factory method
			static ShNonLinSolverPr create();

			// SETTINGS
			// gpu use
			void set_use_gpu(const bool use_gpu = true);
			void set_gpu_devices(const std::set<int>& gpu_devices);

			// INPUT MESH
			// set the mesh from node coordinates and elements
			void set_mesh(
				const arma::Mat<fltp>& Rn, 
				const arma::Mat<arma::uword> &n);

			// set the part the mesh elements belong to
			// this is used to associate the HB curves to
			// a specific set of elements
			void set_n2part(const arma::Row<arma::uword> &n2part);

			// set a (list of) HB curve(s) to be associated 
			// to different parts of the mesh (see set_n2part)
			void set_hb_curve(const ShHBDataPr& hb_curve);
			void set_hb_curve(const arma::field<ShHBDataPr>& hb_curves);

			// set the preconditioner type
			void set_preconditioner_type(
				const PreconditionerType preconditioner_type);

			// set the linear solver type
			void set_linear_solver_type(
				const LinsolType linear_solver_type);

			// mesh setup
			void setup_mesh();
			
			// display the statistics for this mesh
			void display_mesh_analysis()const;

			// get gauss points
			arma::uword get_num_gauss_hexahedron()const;
			arma::uword get_num_gauss_tetrahedron()const;
			arma::uword get_num_gauss_element()const;
			const arma::Mat<fltp>& get_elem_gauss_coords()const;
			const arma::Mat<fltp>& get_elem_gauss_weights()const;

			// getters
			arma::uword get_num_elements()const;
			arma::uword get_num_facets()const;
			arma::uword get_num_edges()const;

			// get the preconditioner type
			PreconditionerType get_preconditioner_type()const;

			// get the linear solver type
			LinsolType get_linear_solver_type()const;

			// get elements
			const arma::Mat<arma::uword>& get_facets()const;
			const arma::Mat<arma::uword>& get_edges()const;
			const arma::Mat<arma::uword>& get_elements()const;
			const arma::Mat<fltp>& get_nodes()const;
			const arma::Row<arma::uword>& get_original2merged()const;

			// check if mesh is planar
			bool mesh_is_planar()const;


			// EXTERNAL FIELD CALCULATION
			// get gauss point coordinates
			arma::Mat<fltp> get_gauss_coords()const;

			// get target coordinates for the field calculation
			arma::Mat<fltp> get_ext_target_coords()const;

			// set the external magnetic field at the target coords
			void set_ext_field(const arma::Mat<fltp>& Hext);

			// get MLFMM magnetic target coordinate object
			// that can be used directly for the external
			// field calculation
			fmm::ShMgnTargetsPr get_ext_targets()const;

			// return the magnetic target coordinate object after 
			// teh magnetic field is calculated by the MLFMM
			void set_ext_field(const fmm::ShMgnTargetsPr& tar);

			// get the calculated external field
			const arma::Mat<fltp>& get_ext_field()const;

			// external delta field
			void set_ext_dfield(const fmm::ShMgnTargetsPr& tar);

			// delta field 
			void set_ext_dfield(const arma::Mat<fltp>& dHext);


			// SYSTEM SETUP
			// calculate the volume associated to each gauss point
			void setup_gauss_volume();

			// create a sparse source to target matrix 
			// that represents the S2T step of the MLFMM
			void create_source_to_target_matrix();

			// create a sparse matrix that calculates the flux 
			// in the gauss points from the flux at the faces
			void create_face_to_element_matrix();

			// create a sparse matrix that calculates the flux
			// through the faces from the vector potential at the edges
			void create_edge_to_face_matrix();

			// pre-calculate the singular surface integrals
			void setup_surface_integrals();

			// calculate the singular surface integrals and output them
			arma::Row<fltp> calc_surface_integrals()const;

			// setup right hand side based on the external
			// magnetic field at the gauss points
			arma::Col<fltp> setup_rhs(const arma::Mat<fltp>&Hext);

			// setup fast multipole method
			void setup_mlfmm();


			// create the preconditioner matrix based on the
			// given reluctance and vector potential at edges
			void setup_preconditioner_matrix(
				const arma::Row<fltp> &nu,
				const arma::Row<fltp> &dnudB,
				const arma::Row<fltp>&Ae);

			// setup the preconditioner factorization
			void setup_preconditioner();

			// clockwise to sign conversion function
			// multiplies by 2 and then subtracts 1 
			static arma::Col<fltp> clockwise2sign(const arma::Col<arma::uword>&cw);
			static fltp clockwise2sign(const arma::uword cw);


			// GAUSS POINTS, FACES AND EDGES
			// calculate the differential reluctance over the faces from the
			// reluctance inside the elements. By default the external reluctance
			// of air is subtracted from the surface elements, but this can
			// be disabled by enabling the jacobian setting
			arma::Row<fltp> calc_delta_reluctance(
				const arma::Row<fltp> &nu, 
				const bool is_jacobian = false)const;

			// integrata values given at gauss points over the element
			arma::Mat<fltp> element_volume_integral(
				const arma::Mat<fltp>&v)const;

			// calculate the element wise averaged value from the 
			// values at the gauss points
			arma::Mat<fltp> element_average(
				const arma::Mat<fltp>&v)const;

			// calculate the flux density at the gauss nodes
			arma::Mat<fltp> calc_elem_flux_density(const arma::Row<fltp> &Ae)const;
			
			// calculate total flux through each facet
			arma::Row<fltp> calc_flux(const arma::Row<fltp> &Ae)const;

			// calculate flux density in facets
			arma::Row<fltp> calc_facet_field(const arma::Row<fltp> &Ae)const;

				
			// KINSOL NEWTON METHOD
			// set the logger to be used to output messages
			void set_lg(const cmn::ShLogPr& lg);

			// find starting point from cache memory
			arma::Col<fltp> get_Ae0(
				const ShSolverCachePr& cache, 
				const arma::Col<fltp>& fscale);

			// linear solver
			void create_linear_solver(
				N_Vector& nvec_Ae, 
				const bool dsolve = false);

			// create the preconditioner
			void create_preconditioner();

			// get vector potential at edges for a constant background field
			arma::Col<fltp> get_Ae_bg(const arma::Col<fltp>::fixed<3>& Hbg);

			// function for determining scaling
			rat::fltp get_scaling();

			// solve function outputs vector potential at edges
			arma::Col<fltp> solve(const ShSolverCachePr& cache = NULL);

			// display the status of the solver
			void display_status()const;

			// convert KINSOL output flag to a message and display in logger
			void flag2msg(const int flag)const;

			// reset the timings for the profile
			void reset_timings()const;

			// display the timings for the profiler
			void display_timings()const;

			// calculate the matrix vector product with the stiffness matrix
			arma::Col<fltp> calc_stiffness_matrix_mvp(
				const arma::Row<fltp> &nu,
				const arma::Row<fltp> &Ae);

			// calculate the matrix vector product with the flux leakage matrix
			arma::Col<fltp> calc_flux_leakage(
				const arma::Row<fltp> &nu,
				const arma::Row<fltp> &Ae,
				const bool is_jacobian = false);

			// update the reluctivity in the main solver object
			// based on the vector potential at the edges
			void update_reluctivity(
				const arma::Row<fltp> &Ae);

			// calculate reluctance from the flux density
			arma::Row<fltp> calc_reluctivity(const arma::Row<fltp> &Bmag)const;

			// calculate derivative of reluctance with respect to flux density 
			arma::Row<fltp> calc_reluctivity_diff(const arma::Row<fltp> &Bmag)const;


			// SYSTEM FUNCTIONS FOR NEWTON METHOD
			// system function for newton
			static int nt_systemfn(
				N_Vector nvec_u, 
				N_Vector nvec_fval, 
				void *user_data);

			// Jacobian times vector function for newton
			static int nt_jtvfn(
				N_Vector nvec_v, 
				N_Vector nvec_Jv, 
				N_Vector nvec_u, 
				int* new_u, 
				void *user_data);

			// Preconditioner setup function for newton
			static int nt_precsetupfn(
				N_Vector nvec_u, 
				N_Vector nvec_uscale, 
				N_Vector nvec_fval, 
				N_Vector nvec_fscale, 
				void *user_data);

			// Preconditioner solve function for newton
			static int nt_precsolvefn(
				N_Vector nvec_u, 
				N_Vector nvec_uscale, 
				N_Vector nvec_fval, 
				N_Vector nvec_fscale, 
				N_Vector nvec_v, 
				void *user_data);

			// // info function
			// static void nt_infofn(
			// 	const char*, 
			// 	const char*, 
			// 	char*, 
			// 	void *user_data);

			// // error handler fun
			// static void nt_errfn(
			// 	int error_code, 
			// 	const char *module, 
			// 	const char *function, 
			// 	char *msg, 
			// 	void *user_data);

			// error handler
			static void err_fn(
				int line, 
				const char *func, 
				const char *file, 
				const char *msg, 
				SUNErrCode err_code, 
				void *err_user_data, 
				SUNContext sunctx);


			// CONVERSION BETWEEN ARMADILLO AND SUNDIALS NVECTOR
			// check if N-vector is identical to armadillo vector
			static int compare_vectors(
				const N_Vector &nvec, 
				const arma::Col<rat::fltp> &v);

			// convert N-vector to armadillo vector
			static N_Vector conv2nvec(
				arma::Col<rat::fltp> &M, 
				SUNContext &sunctx);

			// convert armadillo vector to N-vector
			static arma::Col<rat::fltp> conv2arma(
				N_Vector &V);

			// destroy n-vector
			static void destroy_nvector(
				N_Vector &nvec_v);


			// SOLVE FOR DERIVATIVES
			// solve for derivatives using linear solver
			arma::Col<fltp> dsolve(const arma::Col<fltp>&Ae);

			// display status
			void ddisplay_status()const;

			// preconditioner for linear solver
			static int dprec_solve_fn(
				void *user_data, 
				N_Vector nvec_r, 
				N_Vector nvec_z, 
				sunrealtype tol, 
				int lr);

			// system function for linear solver
			static int datimes_fn(
				void *user_data, 
				N_Vector nvec_v, 
				N_Vector nvec_z);


			// POST PROCESSING
			// magnetic moment sources
			fmm::ShMomentSourcesPr create_magnetic_moments(const arma::Row<fltp> &Ae)const;

			// // get the magnetic field extrapolated and averaged at the nodes
			// arma::Mat<fltp> calc_nodal_magnetic_field(const arma::Row<fltp>&Ae)const;

			// // get the vector potential averaged at the nodes
			// arma::Mat<fltp> calc_nodal_vector_potential(const arma::Row<fltp>&Ae)const;

			// // get the magnetization averaged at the nodes
			arma::Mat<fltp> calc_nodal_magnetization(const arma::Row<fltp>&Ae)const;

			// calculate flux density averaged at the nodes
			arma::Mat<fltp> calc_node_flux_density(const arma::Row<fltp> &Ae)const;

			// // calculate gradient of magnetization
			// arma::Mat<fltp> calc_elem_magnetization_grad(const arma::Row<fltp>&Ae)const;

			// // calculate vector potential at the gauss points
			// arma::Mat<fltp> calc_elem_vector_potential(const arma::Row<fltp> &Ae)const;

			// calculate magnetization at the gauss points
			arma::Mat<fltp> calc_elem_magnetization(const arma::Row<fltp> &Ae)const;

			// calculation of co-energy
			arma::Row<fltp> calc_coenergy(const arma::Row<fltp>&Ae)const;
	};

}}

#endif
