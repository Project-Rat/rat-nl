// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef NL_CACHE_HH
#define NL_CACHE_HH

#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>
#include <list>
#include <mutex>

// common headers
#include "rat/common/typedefs.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// shared pointer definition
	typedef std::shared_ptr<class SolverCache> ShSolverCachePr;
	typedef arma::field<ShSolverCachePr> ShSolverCachePrList;

	// hb curve class template
	class SolverCache{
		// properties
		protected:
			// max cache size in bytes
			fltp max_num_bytes_ = 256.0*1024*1024;

			// maximum number of solutions
			arma::uword max_num_solutions_ = 20llu;

			// maximum number of solutions per number of edges
			arma::uword max_num_solutions_per_num_edges_ = 5llu;

			// vector potential at edges for different solutions
			std::map<arma::uword, std::list<arma::Col<fltp> > > Ae_;

			// create a lock
			std::mutex lock_;

		// methods
		public:
			// constructor
			SolverCache();
			
			// factory
			static ShSolverCachePr create();
			
			// getting
			fltp get_max_num_bytes()const;

			// add solution to cache
			void add_solution(const arma::Col<fltp>&Ae);

			// find solution
			const std::list<arma::Col<fltp> >& get_solutions(const arma::uword num_edges)const;

			// get storage size in bytes
			arma::uword get_storage_size()const;

			// get total number of solutions and number 
			// of solutions with specified number of edges
			arma::uword get_num_solutions()const;
			arma::uword get_num_solutions(const arma::uword num_edges)const;

	};

}}

#endif