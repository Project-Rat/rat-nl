



// https://en.wikipedia.org/wiki/Incomplete_Cholesky_factorization


// COLAMD?

// use iterators 

// incomplete cholesky
function Sp_ichol(const arma::SpMat<fltp>& A){
	// get size of the input matrix
	const arma::uword n = A.n_rows;

	// indices
	arma::uword c_start = 0llu;
	arma::uword c_end = 0llu;

	// walk over columns
	for(arma::uword col=0llu;col<ncols;col++){
		// is next column
		bool is_next_col=false;

		// update start and end indices
		c_start = c_end;
		for(arma::uword i=0;i<n;i++){
			// check if we are in the same column
			if(A(i).col==col){
				if(A(i).col==A(i).row){
					A(i).val=sqrt(A(i).val);
					div=A(i).val;
				}else{
					A(i).val=A(i).val/div;
				}
			}

			// are we in the upper triangular part
			if(A(i).col>col){
				// check if we are not yet in the next column
				if(is_next_col==false){
					c_end = i-1; // check indexing
					is_next_col=true;
				}

				fltp v1 = 0;
				fltp v2 = 0;

				for(arma::uword j=c_start;j<=c_end;j++){// check indexing here
					if(A(j).col==col){
						if(A(j).row==A(i).row){
							v1=A(j).val;
						}
						if(A(j).row==A(i).col){
							v2=A(j).val;
						}
						if(v1~=0 && v2~=0){
							A(i).val=A(i).val-v1*v2;
							break;
						}
					}
				}

			}
		}
	}
}