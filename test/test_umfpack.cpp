// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>

#include "rat/common/error.hh"
#include "precumfpack.hh"

//    This program uses UMFPACK to solve the 5x5 linear system A*X=B:
//
//        2  3  0  0  0        1.0         8.0
//        3  0  4  0  6        2.0        45.0
//    A = 0 -1 -3  2  0    X = 3.0    B = -3.0
//        0  0  1  0  0        4.0         3.0
//        0  4  2  0  1        5.0        19.0

// main function
int main(){
	// matrix setup (purposely added a few duplicates for testing)
	// const arma::Col<arma::uword> col_ptr = {0,2,5,9,10,12};
	const arma::Col<arma::uword> col_idx = {0,0,1,1,1,2,2,2,2,3,4,4};
	const arma::Col<arma::uword> row_idx = {0,1,0,2,4,1,2,3,4,2,1,4};
	arma::Col<double> val = {2,3,3,-1,4,4,-3,1,2,2,6,1};
	// std::cout<<col.n_elem<<" "<<row.n_elem<<" "<<val.n_elem<<std::endl;
	arma::Col<double> B = {8,45,-3,3,19};
	arma::uword N = 5;

	// make sparse matrix
	rat::nl::ShPreconditionerPr M = rat::nl::PrecUMFPack::create();
	
	// set indexes
	M->set_matrix(col_idx,row_idx,val,N);

	// analyze matrix
	M->analyse();

	// factorise matrix
	M->factorise();

	// solve factorisation
	arma::Col<double> X = M->solve(B,0);

	// check output
	if(arma::any(arma::abs(X-arma::regspace<arma::Col<double> >(1,5))>1e-5))
		rat_throw_line("tolerance was not achieved");
	
	return 0;
}
