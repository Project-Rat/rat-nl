// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>

// sundials suite
#include <kinsol/kinsol.h>
#include <nvector/nvector_serial.h>  
#include <sundials/sundials_context.h>

// solver
#include "nonlinsolver.hh"

// main
int main(){
	// vector length
	arma::uword N = 10;

	// create armadillo vector
	arma::Col<double> M(N,arma::fill::randu);

	// sundials context
	SUNContext sunctx;

	// create context
	SUNContext_Create(SUN_COMM_NULL, &sunctx);

	// turn into nvector
	N_Vector V = rat::nl::NonLinSolver::conv2nvec(M,sunctx);
	
	// turn back into armadillo vector
	arma::Mat<double> W = rat::nl::NonLinSolver::conv2arma(V);
	W(5) = 5;

	// 
	N_VPrint_Serial(V);

	// free context
	SUNContext_Free(&sunctx);

	// assert that both have the same values stored
	if(arma::any(W!=M))rat_throw_line("conversion failed");

	// return
	return 0;
}
