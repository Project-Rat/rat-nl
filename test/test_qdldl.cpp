// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include <cassert>
#include <armadillo>

#include "rat/common/error.hh"
#include "precqdldl.hh"

// main function
int main(){
	// matrix setup (purposely added a few duplicates for testing)
	arma::uword N = 10;
	const arma::Col<arma::uword> col_idx = {0, 1, 2, 2, 3, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 9};
	const arma::Col<arma::uword> row_idx = {0, 1, 1, 2, 3, 4, 1, 5, 0, 6, 3, 7, 6,  8,  1, 2, 9};
	arma::Col<rat::fltp> val = {1.0, 0.460641, -0.121189, 0.417928, 0.177828, 0.1, -0.0290058, -1.0, 0.350321, -0.441092, -0.0845395, -0.316228, 0.178663, -0.299077, 0.182452, -1.56506, -0.1};
	arma::Col<rat::fltp> B1 = {1,2,3,4,5,6,7,8,9,10};
	const rat::fltp tol = 1e-9;

	// A\b = [10.2,3.94,-5.69,9.29,50,-6.11,-26.3,-27.8,-45.8,-3.74,]

	// create full matrix
	const arma::Col<arma::uword> idx_non_diag = arma::find(row_idx<col_idx);
	arma::Col<arma::uword> col_idx_full = arma::join_vert(col_idx, row_idx.rows(idx_non_diag));
	arma::Col<arma::uword> row_idx_full = arma::join_vert(row_idx, col_idx.rows(idx_non_diag));
	arma::Col<rat::fltp> val_full = arma::join_vert(val, val.rows(idx_non_diag));

	// resort
	arma::Col<arma::uword> id1 = arma::sort_index(row_idx_full);
	row_idx_full = row_idx_full(id1); col_idx_full = col_idx_full(id1); val_full = val_full(id1);
	
	// sort matrix entries by col without disturbing rows
	arma::Col<arma::uword> id2 = arma::stable_sort_index(col_idx_full);
	row_idx_full = row_idx_full(id2); col_idx_full = col_idx_full(id2); val_full = val_full(id2);


	// make sparse matrix
	rat::nl::ShPreconditionerPr M = rat::nl::PrecQDLDL::create();
	
	// set indexes
	M->set_matrix(col_idx_full,row_idx_full,val_full,N);

	// analyze matrix
	M->analyse();

	// factorise matrix
	M->factorise();

	// solve factorisation
	arma::Col<rat::fltp> X = M->solve(B1,0);

	// check matrix vector product
	const arma::Col<rat::fltp> B2 = M->mvp(X);

	// check output
	if(arma::any(B1-B2>tol))rat_throw_line("solution is out of tolerance");

	return 0;
}
