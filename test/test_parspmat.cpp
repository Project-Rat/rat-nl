// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>

// solver
#include "parspmat.hh"
#include "rat/common/error.hh"

// main
int main(){
		
	// test various matrix size
	const arma::uword num_cols = 4000;
	const arma::Col<arma::uword> num_rows{1,2,3,4,5,8,10,16,32,64,128,500,1000,2000,10000,40000};
	const rat::fltp filling_fraction = 0.1;

	// create timer
	arma::wall_clock timer;
	arma::Col<rat::fltp> regular_time(num_rows.n_elem), parallel_time(num_rows.n_elem);

	// walk over sizes
	for(arma::uword i=0;i<num_rows.n_elem;i++){
		const arma::SpMat<rat::fltp> spM = arma::sprandu(num_rows(i),num_cols,filling_fraction);
		const arma::Col<rat::fltp> x = arma::randu(num_cols);
		const rat::nl::ParSpMat pspM(spM);

		timer.tic();
		const arma::Col<rat::fltp> y1 = spM*x;
		regular_time(i) = timer.toc();

		timer.tic();
		const arma::Col<rat::fltp> y2 = pspM*x;
		parallel_time(i) = timer.toc();

		if(arma::any(y2 - y1>10*arma::Datum<rat::fltp>::eps))
			rat_throw_line("spmat multiplication not working correctly: " + std::to_string(num_rows(i)));
	}

	std::cout<<arma::join_horiz(regular_time, parallel_time)<<std::endl;

	// return
	return 0;
}
