// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>

#include "rat/common/error.hh"

// solver
#include "solvercache.hh"


// main
int main(){
	// settings
	const arma::uword num_tests = 100llu;
	const rat::fltp num_elements = 1e6;

	// create a cache
	const rat::nl::ShSolverCachePr cache = rat::nl::SolverCache::create();
	
	// add to cache
	for(arma::uword i=0;i<num_tests;i++){
		arma::uword num_eq = 1 + static_cast<arma::uword>(std::round(num_elements*std::rand()/RAND_MAX));

		// generate random result vector
		const arma::Col<rat::fltp> A(num_eq,arma::fill::randu);

		// add the solution to the cache
		cache->add_solution(A);

		// check bytes
		if(cache->get_storage_size()>cache->get_max_num_bytes())
			rat_throw_line("truncation failed");
	}

	// return
	return 0;
}
