// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>

// solver
#include "hbdata.hh"

// main
int main(){
	// settings
	const rat::fltp tol = RAT_CONST(1e-9);

	// hb curve under test
	rat::nl::ShHBDataPr hb = rat::nl::HBData::create();
	hb->set_team13();

	// get table
	const arma::Col<rat::fltp>& Hinterp = hb->get_magnetic_field_table();
	const arma::Col<rat::fltp>& Binterp = hb->get_flux_density_table();
	const arma::uword num_interp = Hinterp.n_elem;

	// check if arrays are equal in size
	if(Hinterp.n_elem!=Binterp.n_elem)rat_throw_line("Hinterp and Binterp must have the same number of elements");

	// test reluctance
	const arma::Col<rat::fltp> nuinterp = Hinterp/Binterp;
	const arma::Col<rat::fltp> nucalc = hb->calc_reluctivity(Binterp.t()).t();
	if(arma::any(arma::abs(nucalc-nuinterp)>tol))rat_throw_line("reluctance calculation is not working correctly");

	// test interpolation
	const arma::Col<rat::fltp> Bcalc = hb->calc_magnetic_flux_density(Hinterp.t()).t();
	const arma::Col<rat::fltp> Hcalc = hb->calc_magnetic_field(Binterp.t()).t();
	if(arma::any(arma::abs(Bcalc-Binterp)>tol))rat_throw_line("interpolation of flux density does not work correctly at nodes");
	if(arma::any(arma::abs(Hcalc-Hinterp)>tol))rat_throw_line("interpolation of magnetic field does not work correctly at nodes");

	// test if interpolation is linear
	const arma::Col<rat::fltp> Hinterpmid = (Hinterp.head_rows(num_interp-1) + Hinterp.tail_rows(num_interp-1))/2;
	const arma::Col<rat::fltp> Binterpmid = (Binterp.head_rows(num_interp-1) + Binterp.tail_rows(num_interp-1))/2;
	const arma::Col<rat::fltp> Bcalcmid = hb->calc_magnetic_flux_density(Hinterpmid.t()).t();
	const arma::Col<rat::fltp> Hcalcmid = hb->calc_magnetic_field(Binterpmid.t()).t();
	if(arma::any(arma::abs(Bcalcmid-Binterpmid)>tol))rat_throw_line("interpolation of flux density does not work correctly at midpoint");
	if(arma::any(arma::abs(Hcalcmid-Hinterpmid)>tol))rat_throw_line("interpolation of magnetic field does not work correctly at midpoint");

	// test reluctance at mid points
	const arma::Col<rat::fltp> nuinterpmid = Hinterpmid/Binterpmid;
	const arma::Col<rat::fltp> nucalcmid = hb->calc_reluctivity(Binterpmid.t()).t();
	if(arma::any(arma::abs(nucalcmid-nuinterpmid)>tol))rat_throw_line("reluctance calculation is not working correctly");

	
	// return
	return 0;
}
