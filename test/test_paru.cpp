/* Foxie - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <cassert>
#include <armadillo>

#include "rat/common/error.hh"
#include "precparu.hh"

//    This program uses UMFPACK to solve the 5x5 linear system A*X=B:
//
//        2  3  0  0  0        1.0         8.0
//        3  0  4  0  6        2.0        45.0
//    A = 0 -1 -3  2  0    X = 3.0    B = -3.0
//        0  0  1  0  0        4.0         3.0
//        0  4  2  0  1        5.0        19.0

// main function
int main(){
	// matrix setup (purposely added a few duplicates for testing)
	// const arma::Col<arma::uword> col_ptr = {0,2,5,9,10,12};
	const arma::Col<arma::uword> col_idx = {0,0,1,1,1,2,2,2,2,3,4,4};
	const arma::Col<arma::uword> row_idx = {0,1,0,2,4,1,2,3,4,2,1,4};
	arma::Col<double> val = {2,3,3,-1,4,4,-3,1,2,2,6,1};
	// std::cout<<col.n_elem<<" "<<row.n_elem<<" "<<val.n_elem<<std::endl;
	arma::Col<double> B = {8,45,-3,3,19};
	arma::uword N = 5;

	// make sparse matrix
	rat::nl::ShPreconditionerPr M = rat::nl::PrecParU::create();
	
	// set indexes
	M->set_matrix(col_idx,row_idx,val,N);

	// analyze matrix
	M->analyse();

	// factorise matrix
	M->factorise();

	// solve factorisation
	arma::Col<double> X = M->solve(B,0);

	// check output
	if(arma::any(arma::abs(X-arma::regspace<arma::Col<double> >(1,5))>1e-5))
		rat_throw_line("tolerance was not achieved");
	
	return 0;
}
