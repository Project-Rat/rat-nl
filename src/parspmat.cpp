// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "parspmat.hh"

// general headers
#include <future>
#include <thread> 

// code specific to Rat
namespace rat{namespace nl{
	// same constructor as armadillo SpMat
	ParSpMat::ParSpMat(
		const bool add_values, 
		const arma::Mat<arma::uword> &locations, 
		const arma::Col<fltp> &val, 
		const arma::uword num_rows, 
		const arma::uword num_cols, 
		const bool sort_locations, 
		const bool check_for_zeros){

		// set counters
		nnz_ = locations.n_cols;
		n_rows_ = num_rows;
		n_cols_ = num_cols;

		// number of threads
		const arma::uword num_threads = std::thread::hardware_concurrency();

		// split matrix row wise
		const arma::uword num_row_block = std::max(1llu,num_rows/(num_threads-1));

		// create a division of work
		block_indices_ = arma::regspace<arma::Col<arma::uword> >(0,num_row_block,num_rows);
		if(block_indices_.back()!=num_rows)
			block_indices_ = arma::join_vert(block_indices_,arma::Col<arma::uword>{num_rows});
		
		assert(block_indices_.is_sorted("strictascend"));

		// allocate
		M_.set_size(block_indices_.n_elem-1);

		// allocate futures
		arma::field<std::future<void> > futures(block_indices_.n_elem-1);

		// assemble matrices one by one
		for(arma::uword i=0;i<block_indices_.n_elem-1;i++){
			futures(i) = std::async(std::launch::async,[&,i]() {
				const arma::uword idx1 = block_indices_(i);
				const arma::uword idx2 = block_indices_(i+1)-1;
				const arma::Col<arma::uword> idx = arma::find(locations.row(0)>=idx1 && locations.row(0)<=idx2);
				arma::Mat<arma::uword> subloc = locations.cols(idx); subloc.row(0)-=arma::sword(idx1);
				M_(i) = arma::SpMat<fltp>(add_values,subloc,val.rows(idx),idx2-arma::sword(idx1)+1,num_cols,sort_locations,check_for_zeros);
			});
		}

		// wait for work to finish
		for(arma::uword i=0;i<M_.n_elem;i++)
			futures(i).get();
	}
	
	// constructor with matrix input	
	ParSpMat::ParSpMat(const arma::SpMat<fltp> &M){
		// set counters
		nnz_ = M.n_nonzero;
		n_rows_ = M.n_rows;
		n_cols_ = M.n_cols;

		// number of threads
		const arma::uword num_threads = std::thread::hardware_concurrency();

		// split matrix row wise
		const arma::uword num_row_block = std::max(1llu,M.n_rows/(num_threads-1));

		// create a division of work
		block_indices_ = arma::regspace<arma::Col<arma::uword> >(0,num_row_block,M.n_rows);
		if(block_indices_.back()!=M.n_rows)
			block_indices_ = arma::join_vert(block_indices_,arma::Col<arma::uword>{M.n_rows});

		// split matrix
		M_.set_size(block_indices_.n_elem-1);
		for(arma::uword i=0;i<block_indices_.n_elem-1;i++){
			M_(i) = M.rows(block_indices_(i),block_indices_(i+1)-1);
		}
	}

	// some useful functions
	bool ParSpMat::empty()const{
		return M_.empty();
	}

	arma::uword ParSpMat::n_nonzero()const{
		return nnz_;
	}
	
	arma::uword ParSpMat::n_rows()const{
		return n_rows_;
	}
	
	arma::uword ParSpMat::n_cols()const{
		return n_cols_;
	}
			

	// access stored matrix
	const arma::field<arma::SpMat<fltp> >& ParSpMat::get_matrix()const{
		return M_;
	}
	
	const arma::Col<arma::uword>& ParSpMat::get_block_indices()const{
		return block_indices_;
	}

	// matrix vector product
	arma::Col<fltp> ParSpMat::operator*(const arma::Col<fltp> &x)const{
		// allocate output
		arma::Col<fltp> b(block_indices_.back());

		// allocate futures
		arma::field<std::future<void> > futures(M_.n_elem);

		// calculate matrix vector product in parallel
		for(arma::uword i=0;i<M_.n_elem;i++){
			futures(i) = std::async(std::launch::async,[this,i,&x,&b]() {
				b.rows(block_indices_(i),block_indices_(i+1)-1) = M_(i)*x;
			});
		}

		// wait for work to finish
		for(arma::uword i=0;i<M_.n_elem;i++)
			futures(i).get();

		return b;
	}

}}