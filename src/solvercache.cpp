// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "solvercache.hh"

// common headers
#include "rat/common/error.hh"


// code specific to Raccoon
namespace rat{namespace nl{

	// empty constructor
	SolverCache::SolverCache(){

	}

	// factory
	ShSolverCachePr SolverCache::create(){
		return std::make_shared<SolverCache>();
	}

	// get maximum number of bytes
	rat::fltp SolverCache::get_max_num_bytes()const{
		return max_num_bytes_;
	}

	// add solution to cache
	void SolverCache::add_solution(const arma::Col<fltp>& Ae){
		// lock the mutex
		lock_.lock();

		// remove old solutions until enough memory available
		while((get_storage_size() + Ae.n_elem*sizeof(fltp)) > max_num_bytes_ || get_num_solutions()>max_num_solutions_){
			// count number of solutions
			arma::Col<arma::uword> num_solutions(Ae_.size()); arma::uword cnt=0;
			for(auto it=Ae_.begin();it!=Ae_.end();it++)
				num_solutions(cnt++) = (*it).second.size();
			const arma::uword idx_max = num_solutions.index_max();

			// find and remove oldest solution
			auto it2 = Ae_.begin(); std::advance(it2,idx_max);
			(*it2).second.pop_back();
		}

		// check if solution very close to this one already exists if so remove it
		auto it = Ae_.find(Ae.n_elem);
		if(it!=Ae_.end()){
			for(auto it2=(*it).second.begin();it2!=(*it).second.end();)
				if(arma::all(arma::abs((*it2) - Ae)<1e-6))it2=(*it).second.erase(it2); else it2++;
		}

		// add new solution
		Ae_[Ae.n_elem].push_front(Ae);

		// check number of edge criterion
		for(auto it=Ae_.begin();it!=Ae_.end();it++)
			while((*it).second.size()>max_num_solutions_per_num_edges_)
				(*it).second.pop_back();

		// unlock the mutex
		lock_.unlock();
	}

	// find solution that meets requirement
	const std::list<arma::Col<fltp> >& SolverCache::get_solutions(const arma::uword num_edges)const{
		auto it = Ae_.find(num_edges);
		if(it==Ae_.end())rat_throw_line("there are no solutions with given number of edges");
		return (*it).second;
	}

	// get storage size in byte
	arma::uword SolverCache::get_storage_size()const{
		// walk over data and count bytes
		arma::uword num_byte = 0;
		for(auto it=Ae_.begin();it!=Ae_.end();it++)
			for(auto it2=(*it).second.begin();it2!=(*it).second.end();it2++)
				num_byte+=(*it2).n_elem*sizeof(fltp);
		return num_byte;
	}

	// get number of solutions
	arma::uword SolverCache::get_num_solutions()const{
		arma::uword num_solutions = 0;
		for(auto it=Ae_.begin();it!=Ae_.end();it++)
			num_solutions+=(*it).second.size();
		return num_solutions;
	}

	// get number of solutions
	arma::uword SolverCache::get_num_solutions(const arma::uword num_edges)const{
		auto it = Ae_.find(num_edges);
		if(it==Ae_.end())return 0;
		return (*it).second.size();
	}

}}