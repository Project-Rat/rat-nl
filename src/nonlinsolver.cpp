// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "nonlinsolver.hh"

// mlfmm headers
#include "rat/mlfmm/chargedpolyhedron.hh"

// common headers
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// nl headers
#ifdef ENABLE_UMFPACK
#include "precumfpack.hh"
#endif
#ifdef ENABLE_CHOLMOD
#include "preccholmod.hh"
#endif
#ifdef ENABLE_QDLDL
#include "precqdldl.hh"
#endif
#ifdef ENABLE_PARU
#include "precparu.hh"
#endif
#ifdef ENABLE_SUPERLU
#include "precsuperlu.hh"
#endif
#include "precjacobi.hh"

// openmp
#include <omp.h>

// code specific to Rat-NL
namespace rat{namespace nl{

	// CONSTRUCTOR/DESTRUCTOR
	// ======================
	// constructor
	NonLinSolver::NonLinSolver(){
		// default preconditioner
		#ifdef ENABLE_CHOLMOD
		preconditioner_type_ = PreconditionerType::CHOLMOD;
		#else
		#ifdef ENABLE_UMFPACK
		preconditioner_type_ = PreconditionerType::UMFPACK;
		#else
		#ifdef ENABLE_PARU
		preconditioner_type_ = PreconditionerType::PARU;
		#else
		#ifdef ARMA_USE_SUPERLU
		preconditioner_type_ = PreconditionerType::SUPERLU;
		#else
		preconditioner_type_ = PreconditionerType::JACOBI; // this is a last resort may result in non-convergence
		#endif
		#endif
		#endif
		#endif
	}

	// factory methods
	ShNonLinSolverPr NonLinSolver::create(){
		return std::make_shared<NonLinSolver>();
	}


	// SETTERS
	// gpu enabled switch
	void NonLinSolver::set_use_gpu(const bool use_gpu){
		use_gpu_ = use_gpu;
	}

	// device
	void NonLinSolver::set_gpu_devices(const std::set<int>& gpu_devices){
		gpu_devices_ = gpu_devices;
	}

	// mesh
	void NonLinSolver::set_mesh(
		const arma::Mat<fltp>& Rn, 
		const arma::Mat<arma::uword> &n){
		Rn_ = Rn; n_ = n;
	}

	void NonLinSolver::set_n2part(
		const arma::Row<arma::uword> &n2part){
		n2part_ = n2part;
	}

	// set logger
	void NonLinSolver::set_lg(const cmn::ShLogPr& lg){
		lg_ = lg;
	}

	void NonLinSolver::set_hb_curve(const ShHBDataPr& hb_curve){
		hb_curve_ = {hb_curve};
	}

	void NonLinSolver::set_hb_curve(const arma::field<ShHBDataPr>& hb_curve){
		hb_curve_ = hb_curve;
	}


	void NonLinSolver::set_preconditioner_type(
		const PreconditionerType preconditioner_type){
		preconditioner_type_ = preconditioner_type;
	}

	// set the linear solver type
	void NonLinSolver::set_linear_solver_type(
		const LinsolType linear_solver_type){
		linear_solver_type_ = linear_solver_type;
	}


	// GETTERS
	// count number of hex elements
	arma::uword NonLinSolver::get_num_elements()const{
		return n_.n_cols;
	}

	// count number of facets
	arma::uword NonLinSolver::get_num_facets()const{
		return f_.n_cols;
	}

	// count number of faces
	arma::uword NonLinSolver::get_num_edges()const{
		return e_.n_cols;
	}

	// get facets
	const arma::Mat<arma::uword>& NonLinSolver::get_facets()const{
		return f_;
	}

	// get edges
	const arma::Mat<arma::uword>& NonLinSolver::get_edges()const{
		return e_;
	}

	// get edges
	const arma::Mat<arma::uword>& NonLinSolver::get_elements()const{
		return n_;
	}

	// get nodes
	const arma::Mat<fltp>& NonLinSolver::get_nodes()const{
		return Rn_;
	}

	// get node merging indices
	const arma::Row<arma::uword>& NonLinSolver::get_original2merged()const{
		return original2merged_;
	}

	// get linear solver preconditioner
	NonLinSolver::PreconditionerType NonLinSolver::get_preconditioner_type()const{
		return preconditioner_type_;
	}

	// get the linear solver type
	NonLinSolver::LinsolType NonLinSolver::get_linear_solver_type()const{
		return linear_solver_type_;
	}

	// convert clockwise to sign
	arma::Col<fltp> NonLinSolver::clockwise2sign(const arma::Col<arma::uword>&cw){
		return arma::conv_to<arma::Col<fltp> >::from(2*cw)-RAT_CONST(1.0);
	}

	// convert clockwise to sign
	fltp NonLinSolver::clockwise2sign(const arma::uword cw){
		return 2*fltp(cw)-RAT_CONST(1.0);
	}

	// check if faces are planar
	bool NonLinSolver::mesh_is_planar()const{
		if(n_.n_rows==8){
			for(arma::uword i=0;i<f_.n_cols;i++)
				if(!cmn::Quadrilateral::check_nodes(Rn_.cols(f_.col(i))))return false;
		}

		// all checks passed
		return true;
	}

	// setup function for mesh
	void NonLinSolver::setup_mesh(){
		// check dimensions
		if(n_.n_rows!=4 && n_.n_rows!=8)rat_throw_line("element matrix must either have 4 rows (tetrahedron) or 8 rows (hexahedron)");
		if(n_.max()>=Rn_.n_cols)rat_throw_line("max element index exceeds number of nodes");
		if(!Rn_.is_finite())rat_throw_line("input coordinates are not finite");

		// get connection matrices
		const arma::Mat<arma::uword> F = n_.n_rows==8 ? cmn::Hexahedron::get_faces().eval() : cmn::Tetrahedron::get_faces().eval();
		const arma::Mat<arma::uword> E = n_.n_rows==8 ? cmn::Quadrilateral::get_edges().eval() : cmn::Triangle::get_edges().eval();

		// merge nodes
		original2merged_ = cmn::Extra::combine_nodes(Rn_,node_merge_tol_);
		n_ = arma::reshape(original2merged_(arma::vectorise(n_)),n_.n_rows,n_.n_cols);

		// get counters
		const arma::uword num_hexahedrons = n_.n_cols;
		const arma::uword num_faces = F.n_rows;
		const arma::uword num_edges = E.n_rows;
		const arma::uword num_nodes_face = F.n_cols;
		const arma::uword num_nodes_edge = E.n_cols;

		// face element matrices
		f_.set_size(num_nodes_face, num_hexahedrons*num_faces);

		// check for inverted hexahedrons
		{
			// check clockwise
			const arma::Col<arma::uword> cw = arma::find((n_.n_rows==8 ? cmn::Hexahedron::is_clockwise(Rn_,n_) : cmn::Tetrahedron::is_clockwise(Rn_,n_))==0);

			// swap faces, and flip faces
			if(!cw.is_empty())n_.cols(cw) = n_.n_rows==8 ? cmn::Hexahedron::invert_elements(n_.cols(cw)) : cmn::Tetrahedron::invert_elements(n_.cols(cw));

			// check clockwise
			if(!arma::all((n_.n_rows==8 ? cmn::Hexahedron::is_clockwise(Rn_,n_) : cmn::Tetrahedron::is_clockwise(Rn_,n_))==true))
				rat_throw_line("inversion failed");
		}

		// walk over tetrahedron/hexahedron
		for(arma::uword i=0;i<n_.n_cols;i++){
			// get this tetrahedron/hexahedron
			const arma::Col<arma::uword> h = n_.col(i);

			// walk over faces
			for(arma::uword j=0;j<num_faces;j++){
				// get faces from this hexahedron
				f_.col(i*num_faces + j) = h.rows(arma::vectorise(F.row(j)));
			}
		}

		// get unique faces
		{
			// origin
			arma::Col<arma::uword> origin = arma::regspace<arma::Col<arma::uword> >(0,f_.n_cols-1);

			// create new matrix with sorted faces
			arma::Mat<arma::uword> fs(f_.n_rows,f_.n_cols);
			for(arma::uword i=0;i<f_.n_cols;i++)
				fs.col(i) = arma::sort(f_.col(i));

			// sort faces
			for(arma::uword i=0;i<fs.n_rows;i++){
				const arma::Col<arma::uword> sort_index = i==0 ? arma::sort_index(fs.row(i)).eval() : arma::stable_sort_index(fs.row(i)).eval();
				origin = origin(sort_index); fs = fs.cols(sort_index);
			}

			// find duplicate faces
			arma::Col<arma::uword> is_duplicate(fs.n_cols); is_duplicate(0) = 0;
			is_duplicate.rows(1,fs.n_cols-1) = arma::all(fs.head_cols(fs.n_cols-1)==fs.tail_cols(fs.n_cols-1)).t();

			// face re-indexing array
			arma::Col<arma::uword> fidx = arma::cumsum(is_duplicate==0)-1;

			// sort back to original indexing
			fidx(origin) = fidx;

			// remove duplicate faces
			f_ = f_.cols(origin(arma::find(is_duplicate==0)));

			// nodes for each face must be unique
			#ifndef NDEBUG
			for(arma::uword i=0;i<f_.n_cols;i++)assert(arma::unique(f_.col(i)).eval().n_elem==f_.n_rows);
			#endif

			// indexing into faces
			n2f_ = arma::reshape(fidx, num_faces, num_hexahedrons);

			// std::cout<<f_.n_cols<<std::endl;
			// std::cout<<n2f_.t()<<std::endl;
			// assert(n2f_.max()==f_.n_cols-1);

			// connect faces to the elements
			f2n_.set_size(2,f_.n_cols); f2n_.fill(-1); // largest integer possible in arma::uword
			fcnt_.zeros(f_.n_cols);
			for(arma::uword i=0;i<n2f_.n_cols;i++){
				for(arma::uword j=0;j<n2f_.n_rows;j++){
					const arma::uword idx = n2f_(j,i);
					f2n_(fcnt_(idx)++, idx)=i;
				}
			}

			// face can only be referenced twice max
			assert(fcnt_.max()<=2);

			// find faces on surface
			fs_ = arma::find(fcnt_==1);

			// faces that were duplicates are clockwise 
			// with respect to the other element
			// but not to this element
			fcw_.zeros(num_faces,num_hexahedrons);
			fcw_(origin) = is_duplicate==0;
		}

		// edge element matrices
		e_.set_size(num_nodes_edge, f_.n_cols*num_edges);

		// walk over faces
		for(arma::uword i=0;i<f_.n_cols;i++){
			// get this face
			const arma::Col<arma::uword> f = f_.col(i);

			// walk over edges
			for(arma::uword j=0;j<num_edges;j++){
				// get faces from this hexahedron
				e_.col(i*num_edges + j) = f.rows(arma::vectorise(E.row(j)));
			}
		}

		// get unique edges
		{
			// origin
			arma::Col<arma::uword> origin = arma::regspace<arma::Col<arma::uword> >(0,e_.n_cols-1);

			// create new matrix with sorted edges
			arma::Mat<arma::uword> es(e_.n_rows,e_.n_cols);
			for(arma::uword i=0;i<e_.n_cols;i++)
				es.col(i) = arma::sort(e_.col(i));

			// sort edges
			for(arma::uword i=0;i<es.n_rows;i++){
				const arma::Col<arma::uword> sort_index = i==0 ? arma::sort_index(es.row(i)).eval() : arma::stable_sort_index(es.row(i)).eval();
				origin = origin(sort_index); es = es.cols(sort_index);
			}

			// find duplicate edges
			arma::Col<arma::uword> is_duplicate(es.n_cols); is_duplicate(0) = 0;
			is_duplicate.rows(1,es.n_cols-1) = arma::all(es.head_cols(es.n_cols-1)==es.tail_cols(es.n_cols-1)).t();

			// face re-indexing array
			arma::Col<arma::uword> eidx = arma::cumsum(is_duplicate==0)-1;

			// sort back to original indexing
			eidx(origin) = eidx;

			// remove duplicate edges
			e_ = e_.cols(origin(arma::find(is_duplicate==0)));

			// indexing from face into edges
			f2e_ = arma::reshape(eidx, num_edges, f_.n_cols);

			// edge element matrices
			ecw_.zeros(num_edges, f_.n_cols);

			// walk over faces
			for(arma::uword i=0;i<f_.n_cols;i++){
				// get this face
				const arma::Col<arma::uword> f = f_.col(i);

				// walk over edges
				for(arma::uword j=0;j<num_edges;j++)
					ecw_(j,i) = arma::all(e_.col(f2e_(j,i)) == f.rows(arma::vectorise(E.row(j))));
			}

			// count number of references to this edge
			ecnt_.zeros(e_.n_cols);
			for(arma::uword i=0;i<f2e_.n_elem;i++)ecnt_(f2e_(i))++;

			// connect edges to the faces
			e2f_.set_size(ecnt_.max(),e_.n_cols); e2f_.fill(-1); // largest integer possible in arma::uword
			ecnt_.zeros(e_.n_cols);
			for(arma::uword i=0;i<f2e_.n_cols;i++){
				for(arma::uword j=0;j<f2e_.n_rows;j++){
					const arma::uword idx = f2e_(j,i);
					e2f_(ecnt_(idx)++, idx)=i;
				}
			}

			// surface edges
			es_.set_size(f2e_.n_rows*fs_.n_elem);
			for(arma::uword i=0;i<fs_.n_elem;i++)
				es_.rows(i*f2e_.n_rows,(i+1)*f2e_.n_rows-1) = f2e_.col(fs_(i));
			es_ = arma::unique(arma::vectorise(es_));
		}

		// calculate element, face and edge dimensions
		elem_volume_ = n_.n_rows==8 ? cmn::Hexahedron::calc_volume(Rn_,n_) : cmn::Tetrahedron::calc_volume(Rn_,n_);
		face_area_ = n_.n_rows==8 ? cmn::Quadrilateral::calc_area(Rn_,f_) : cmn::Triangle::calc_area(Rn_,f_);
		edge_length_ = cmn::Line::calc_length(Rn_,e_);

		// check
		assert(arma::all(face_area_>0));
		assert(arma::all(elem_volume_>0));
		assert(arma::all(edge_length_>0));

		// calculate face normals of all facets
		Nf_ = n_.n_rows==8 ? cmn::Quadrilateral::calc_face_normals(Rn_,f_) : cmn::Triangle::calc_face_normals(Rn_,f_);

		// create 2D gauss grid for quadrilaterals
		const arma::Mat<fltp> gpf = n_.n_rows==8 ? cmn::Quadrilateral::create_gauss_points(num_gauss_quadrilateral_) : cmn::Triangle::create_gauss_points(num_gauss_triangle_);
		Rqf_ = gpf.rows(0,1); wqf_ = gpf.row(2);

		// Create 2D gauss grid for singularities
		const arma::Mat<fltp> gps = n_.n_rows==8 ? cmn::Quadrilateral::create_gauss_points(num_gauss_quadrilateral_self_) : cmn::Triangle::create_gauss_points(num_gauss_triangle_self_);
		Rqs_ = gps.rows(0,1); wqs_ = gps.row(2);

		// Create 3D gauss grid
		const arma::Mat<fltp> gph = n_.n_rows==8 ? cmn::Hexahedron::create_gauss_points(num_gauss_hexahedron_) : cmn::Tetrahedron::create_gauss_points(num_gauss_tetrahedron_);
		Rqh_ = gph.rows(0,2); wqh_ = gph.row(3);

		// check that all surface faces are clockwise
		#ifndef NDEBUG
		for(arma::uword i=0;i<n_.n_cols;i++){
			for(arma::uword j=0;j<n2f_.n_rows;j++){
				const arma::uword fid = n2f_(j,i);
				if(fcnt_(fid)==1)if(fcw_(j,i)==0)rat_throw_line("surface face must be clockwise");
			}
		}
		#endif
	}

	// get gauss points
	arma::uword NonLinSolver::get_num_gauss_hexahedron()const{
		return num_gauss_hexahedron_;
	}

	arma::uword NonLinSolver::get_num_gauss_tetrahedron()const{
		return num_gauss_tetrahedron_;
	}

	arma::uword NonLinSolver::get_num_gauss_element()const{
		assert(!Rqh_.empty());
		return Rqh_.n_cols;
	}

	const arma::Mat<fltp>& NonLinSolver::get_elem_gauss_coords()const{
		return Rqh_;
	}

	const arma::Mat<fltp>& NonLinSolver::get_elem_gauss_weights()const{
		return wqh_;
	}


	// get gauss point coordinates
	arma::Mat<fltp> NonLinSolver::get_gauss_coords()const{
		arma::Mat<fltp> Rg(3,n_.n_cols*Rqh_.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			Rg.cols(i*Rqh_.n_cols,(i+1)*Rqh_.n_cols-1) = n_.n_rows==8 ? 
				cmn::Hexahedron::quad2cart(Rn_.cols(n_.col(i)),Rqh_) : 
				cmn::Tetrahedron::quad2cart(Rn_.cols(n_.col(i)),Rqh_);
		return Rg;
	}

	// create source to target matrix
	void NonLinSolver::create_source_to_target_matrix(){
		// sanity check
		assert(src_->num_sources()==f_.n_cols);
		assert(tar_->num_targets()==fs_.n_elem);
		assert(!Rqf_.empty()); assert(!wqf_.empty());

		// set timer
		arma::wall_clock timer; timer.tic();
		lg_->msg(2,"%sSetting up Source to Target interactions%s\n",KBLU,KNRM);
		lg_->msg("using %s%s%s integrals ...\n",KYEL,use_analytical_integrals_ ? "analytical" : "numerical",KNRM);

		// get source to target interactions
		const fmm::ShNodeLevelPr& leaf = mlfmm_->get_root()->get_leaf();
		const arma::field<arma::Col<arma::uword> >& source_list = leaf->get_s2t_lists_src();
		const arma::Col<arma::uword>& target_list = leaf->get_s2t_list_tar();

		// get list of source and target positions
		const fmm::ShGridPr& grid = mlfmm_->get_grid();
		const arma::Row<arma::uword>& first_source =grid->get_first_source();
		const arma::Row<arma::uword>& last_source = grid->get_last_source();
		const arma::Row<arma::uword>& first_target = grid->get_first_target();
		const arma::Row<arma::uword>& last_target = grid->get_last_target();

		// translate the indices through grid
		const arma::Row<arma::uword>& source_sort_index = grid->get_source_sort_index();
		const arma::Row<arma::uword>& target_sort_index = grid->get_target_sort_index();

		// num S2T matrix
		arma::Row<arma::uword> spmat_num_s2t(target_list.n_elem,arma::fill::zeros);
		for(arma::uword i=0;i<target_list.n_elem;i++)
			for(arma::uword j=0;j<source_list(i).n_elem;j++)
				spmat_num_s2t(i) += (last_target(target_list(i)) - first_target(target_list(i)) + 1)*
					(last_source(source_list(i)(j)) - first_source(source_list(i)(j)) + 1);

		// cumalative sum to get sparse index of each target node
		const arma::Row<arma::uword> spmat_id = arma::join_horiz(arma::Row<arma::uword>{0},arma::cumsum(spmat_num_s2t));
		const arma::uword num_s2t = spmat_id.back(); //num_s2t -= fs_.n_elem;

		// report
		lg_->msg("number of interactions: %s%8.2e%s\n",KYEL,static_cast<fltp>(num_s2t),KNRM);

		// allocate sparse matrix
		arma::Mat<arma::uword> locations(2,num_s2t);
		arma::Col<fltp> val(num_s2t,arma::fill::zeros); 

		// create conversion matrices
		arma::Row<arma::uword> source_destination = arma::regspace<arma::Row<arma::uword> >(0,grid->get_num_sources()-1);
		arma::Row<arma::uword> target_destination = arma::regspace<arma::Row<arma::uword> >(0,grid->get_num_targets()-1);
		source_destination = source_destination.cols(source_sort_index);
		target_destination = target_destination.cols(target_sort_index);

		// calculate derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Quadrilateral::shape_function_derivative(Rqf_) : cmn::Triangle::shape_function_derivative(Rqf_);

		// calculate jacobian determinants for each face
		arma::Row<fltp> Jdet(f_.n_cols*Rqf_.n_cols);
		arma::Mat<fltp> Rg(3,f_.n_cols*Rqf_.n_cols);
		cmn::parfor(0,f_.n_cols,true,[&](arma::uword i, arma::uword /*cpu*/){
			// calculate jacobian
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Quadrilateral::shape_function_jacobian(Rn_.cols(f_.col(i)),dN) : cmn::Triangle::shape_function_jacobian(Rn_.cols(f_.col(i)),dN);

			// calculate determinant and store
			Jdet.cols(i*Rqf_.n_cols, (i+1)*Rqf_.n_cols-1) = n_.n_rows==8 ? cmn::Quadrilateral::jacobian2determinant(J) : cmn::Triangle::jacobian2determinant(J);

			// create gauss points
			Rg.cols(i*Rqf_.n_cols, (i+1)*Rqf_.n_cols-1) = n_.n_rows==8 ? cmn::Quadrilateral::quad2cart(Rn_.cols(f_.col(i)), Rqf_) : cmn::Triangle::quad2cart(Rn_.cols(f_.col(i)), Rqf_);

			// check
			assert(arma::all(Jdet.cols(i*Rqf_.n_cols, (i+1)*Rqf_.n_cols-1)>RAT_CONST(0.0)));
			
			// this check fails if mesh is not perfectly planar
			// assert(std::abs(arma::accu(Jdet.cols(i*Rqf_.n_cols, (i+1)*Rqf_.n_cols-1)%wqf_) - face_area_(i))/std::abs(face_area_(i))<1e-6);
		});

		// create locations list
		for(arma::uword i=0;i<target_list.n_elem;i++){
			// get target node
			const arma::uword tni = target_list(i);

			// get location of the targets
			const arma::uword ft = first_target(tni);
			const arma::uword lt = last_target(tni);
			assert(ft<=lt); assert(lt<fs_.n_elem);

			// get target id's
			const arma::Row<arma::uword>& tsfids = target_destination.cols(ft,lt);
			assert(!tsfids.empty());

			// get starting point
			arma::uword cnt = spmat_id(i);

			// walk over target FMM nodes
			for(arma::uword j=0;j<source_list(i).n_elem;j++){
				// get source node
				const arma::uword sni = source_list(i)(j);

				// get my source
				const arma::uword fs = first_source(sni);
				const arma::uword ls = last_source(sni);
				assert(fs<=ls); assert(ls<f_.n_elem);

				// get source id's
				const arma::Row<arma::uword>& sfids = source_destination.cols(fs,ls);
				assert(!sfids.empty());
				
				// walk over target faces
				for(arma::uword k=0;k<tsfids.n_elem;k++){
					// get target face id
					const arma::uword tsfi = tsfids(k); // surface face index
					// const arma::uword tfi = fs_(tsfi);
					assert(tsfi<fs_.n_elem); assert(fs_(tsfi)<f_.n_cols);

					// walk over source faces
					for(arma::uword l=0;l<sfids.n_elem;l++){
						// get source face id
						const arma::uword sfi = sfids(l);

						// add entry to sparse matrix
						locations(0,cnt) = tsfi; locations(1,cnt) = sfi;

						// increment counter
						cnt++;
					}
				}
			}
		}

		// table header
		lg_->msg(2,"Processing:\n");
		lg_->msg("%s%8s %11s%s\n",KCYN,"time [s]","pctdone [%]",KNRM);
		lg_->msg("%s====================%s\n",KCYN,KNRM);

		// walk over source FMM nodes
		fltp last_report_time = -5.0;
		cmn::parfor(0,locations.n_cols,true,[&](arma::uword i, arma::uword cpu){
			// stop loop on cancel
			if(lg_->is_cancelled())return;

			// add entry to sparse matrix
			const arma::uword tsfi = locations(0,i); 
			const arma::uword sfi = locations(1,i);
			const arma::uword tfi = fs_(tsfi);

			// do not include self interaction 
			if(tfi==sfi)return;

			// gauss points
			const arma::Mat<fltp> Rt = Rg.cols(tfi*Rqf_.n_cols, (tfi+1)*Rqf_.n_cols-1);

			// get determinant
			const arma::Row<fltp> tJdet = Jdet.cols(tfi*Rqf_.n_cols, (tfi+1)*Rqf_.n_cols-1);

			// get face areaa
			const fltp target_area = face_area_(tfi);

			// calculate integrals using right triangle code
			if(use_analytical_integrals_){
				// use charged triangle script instead
				val(i) = arma::accu(tJdet%wqf_%rat::fmm::ChargedPolyhedron::calc_scalar_potential(Rn_.cols(f_.col(sfi)), Rt))/target_area;
			}

			// calculate integrals using gaussian integration
			else{
				// get determinant
				const arma::Row<fltp> sJdet = Jdet.cols(sfi*Rqf_.n_cols, (sfi+1)*Rqf_.n_cols-1);

				// gauss points
				const arma::Mat<fltp> Rs = Rg.cols(sfi*Rqf_.n_cols, (sfi+1)*Rqf_.n_cols-1); 

				// walk over source points
				for(arma::uword m=0;m<Rs.n_cols;m++){
					const arma::Row<fltp> r = cmn::Extra::vec_norm(Rs.col(m) - Rt.each_col());
					val(i) += arma::accu(wqf_(m)*sJdet(m)*(tJdet%wqf_/r))/target_area;
				}
			}

			// output
			if(cpu==0){
				if(timer.toc()>=last_report_time + RAT_CONST(5.0)){
					last_report_time = timer.toc();
					lg_->msg("%s%08.2f %011.2f%s\n",KCYN,last_report_time,(i*100.0)/locations.n_cols,KNRM);
				}
			}
			
			// // calculate face normal unit vector
			// const arma::Col<fltp>::fixed<3> zeta = Nf_.col(sfi); // unit vector normal to face
			// assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(zeta)) - 1.0)<1e-9);

			// // walk over source face edges
			// for(arma::uword m=0;m<f2e_.n_rows;m++){
			// 	// get edge index
			// 	const arma::uword eid = f2e_(m,sfi);

			// 	// get edge nodes
			// 	arma::Mat<fltp>::fixed<3,2> Re = Rn_.cols(e_.col(eid));

			// 	// make edge clockwise with respect to face
			// 	if(ecw_(m,sfi)==0)Re = arma::fliplr(Re);

			// 	// edge unit vector
			// 	const arma::Col<fltp>::fixed<3> Vg = arma::diff(Re,1,1);
			// 	const arma::Col<fltp>::fixed<3> xi = Vg.each_row()/cmn::Extra::vec_norm(Vg); // unit vector along edge

			// 	// plane unit vector
			// 	const arma::Col<fltp>::fixed<3> nu = cmn::Extra::cross(xi,zeta); // unit vector across the edge
			// 	assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(nu)) - 1.0)<1e-9);

			// 	// relative position of start and end point
			// 	const arma::Mat<fltp> R0 = Rt.each_col() - Re.col(0);
			// 	const arma::Mat<fltp> R1 = Rt.each_col() - Re.col(1);

			// 	// distance between points
			// 	const arma::Row<fltp> r0 = cmn::Extra::vec_norm(R0);
			// 	const arma::Row<fltp> r1 = cmn::Extra::vec_norm(R1);

			// 	// in local coordinates
			// 	const arma::Row<fltp> rxi0 = arma::sum(R0.each_col()%xi);
			// 	const arma::Row<fltp> rxi1 = arma::sum(R1.each_col()%xi);
			// 	const arma::Row<fltp> rnu = arma::sum(R0.each_col()%nu);
			// 	const arma::Row<fltp> rzeta = arma::sum(R0.each_col()%zeta);

			// 	// add edge contribution to matrix
			// 	val(i) += arma::accu(wqf_%tJdet%rnu%(
			// 		rxi1/(r1 + arma::abs(rzeta)) - 
			// 		rxi0/(r0 + arma::abs(rzeta))))/target_area;
			// }
		});

		// stop loop on cancel
		if(lg_->is_cancelled())return;

		// done
		lg_->msg(-2,"%sdone%s\n",KCYN,KNRM);
		lg_->msg(-2,"\n");

		// show info 
		lg_->msg(2,"%sSource to Target Matrix:%s\n",KBLU,KNRM); 
		lg_->msg("assembling sparse matrix ...\n");

		// create sparse matrix
		if(use_parallel_s2t_matrix_){
			// create parallel matrix
			Ms2t_par_ = ParSpMat(true,locations,val,fs_.n_elem,f_.n_cols,true,true);

			// display info
			lg_->msg("using parallel matrix\n");
			lg_->msg("matrix size: %s%llu%s X %s%llu%s (rows X cols)\n", KYEL, Ms2t_par_.n_rows(), KNRM, KYEL, Ms2t_par_.n_cols(), KNRM);
			lg_->msg("number off nonzeros: %s%llu%s (CSC %s%.2f [MB]%s)\n", KYEL, Ms2t_par_.n_nonzero(), KNRM, KYEL, fltp(2*Ms2t_par_.n_nonzero() + Ms2t_par_.n_cols() + 1)*sizeof(fltp)/1024/1024, KNRM);
			lg_->msg("fill fraction: %s%.2f%s [pct]\n", KYEL, 100*fltp(Ms2t_par_.n_nonzero())/(Ms2t_par_.n_rows()*Ms2t_par_.n_cols()), KNRM);
			lg_->msg(-2,"\n");
		}else{
			// create matrix
			Ms2t_ = arma::SpMat<fltp>(true,locations,val,fs_.n_elem,f_.n_cols,true,true); // using transpose of source to target matrix

			// display info
			lg_->msg("using serial matrix\n");
			lg_->msg("matrix size: %s%llu%s X %s%llu%s (rows X cols)\n", KYEL, Ms2t_.n_rows, KNRM, KYEL, Ms2t_.n_cols, KNRM);
			lg_->msg("number off nonzeros: %s%llu%s (CSC %s%.2f [MB]%s)\n", KYEL, Ms2t_.n_nonzero, KNRM, KYEL, fltp(2*Ms2t_.n_nonzero + Ms2t_.n_cols + 1)*sizeof(fltp)/1024/1024, KNRM);
			lg_->msg("fill fraction: %s%.2f%s [pct]\n", KYEL, 100*fltp(Ms2t_.n_nonzero)/(Ms2t_.n_rows*Ms2t_.n_cols), KNRM);
			lg_->msg(-2,"\n");
		}

		// get time
		time_.ms2t_setup_ += timer.toc();
	}

	// setup FEM stiffness matrix based on current vector potential along edges
	void NonLinSolver::setup_preconditioner_matrix(
		const arma::Row<fltp> &nu,
		const arma::Row<fltp> &/*dnudB*/,
		const arma::Row<fltp> &/*Ae*/){

		// set timer
		arma::wall_clock timer; timer.tic();

		// check orthogonality
		arma::Mat<arma::uword> enable_f2f;

		// for hexahedron
		if(n_.n_rows==8){
			const cmn::Hexahedron::FaceNormalMatrix Fn = cmn::Hexahedron::get_facenormal();
			enable_f2f.set_size(n2f_.n_rows, n2f_.n_rows);
			for(arma::uword j=0;j<n2f_.n_rows;j++)
				for(arma::uword k=0;k<n2f_.n_rows;k++)
					enable_f2f(j,k) = arma::accu(Fn.row(k)%Fn.row(j))!=0;
		}

		// for tetrahedron
		else{
			enable_f2f.ones(4,4);
		}

		// count number of interactions
		const arma::uword num_f2f = arma::accu(enable_f2f);

		// calculate matrix number of non-zero entries
		const arma::uword num_fem = n_.n_cols*num_f2f*f2e_.n_rows*f2e_.n_rows;
		const arma::uword num_s2t = fs_.n_elem*f2e_.n_rows*f2e_.n_rows;
		const arma::uword nnz = num_fem + num_s2t;

		// allocate sparse matrix
		arma::Col<arma::uword> row(nnz),col(nnz);
		arma::Col<fltp> val(nnz); arma::uword cnt = 0;

		// shape function (can be done outside of for loop)
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Hexahedron::shape_function_derivative(Rqh_) : cmn::Tetrahedron::shape_function_derivative(Rqh_);
		const arma::Mat<fltp> Vq = n_.n_rows==8 ? cmn::Hexahedron::nedelec_hdiv_shape_function(Rqh_) : cmn::Tetrahedron::nedelec_hdiv_shape_function(Rqh_);

		// walk over elements
		for(arma::uword i=0;i<n_.n_cols;i++){
			// check which faces are to be reversed for this element
			const arma::Col<fltp> cw = clockwise2sign(fcw_.col(i));

			// get the areas of each face
			const arma::Col<fltp> face_areas = face_area_(n2f_.col(i));

			// get reluctance at gauss points
			const arma::Row<fltp> element_nug = nu.cols(i*Rqh_.n_cols,(i+1)*Rqh_.n_cols-1);

			// calculate shape function
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Hexahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN) : cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Hexahedron::jacobian2determinant(J) : cmn::Tetrahedron::jacobian2determinant(J);
			const arma::Mat<fltp> Vc = n_.n_rows==8 ? cmn::Hexahedron::quad2cart_contravariant_piola(Vq,J,Jdet) : cmn::Tetrahedron::quad2cart_contravariant_piola(Vq,J,Jdet);
			assert(arma::all(Jdet>0));

			// this check fails if mesh is not perfectly planar
			// assert(std::abs(arma::accu(Jdet%wqh_) - elem_volume_(i))/std::abs(elem_volume_(i))<1e-6);

			// field at element faces
			const arma::Col<fltp> fsc = face_areas;

			// allocate integrals and their derivatives
			arma::Mat<fltp> Mf2f(n2f_.n_rows, n2f_.n_rows, arma::fill::zeros);

			// walk over source faces
			for(arma::uword n=0;n<n2f_.n_rows;n++){
				// walk over target faces
				for(arma::uword m=n;m<n2f_.n_rows;m++){
					if(!enable_f2f(n,m))continue;
					Mf2f(n,m) = arma::accu(
						wqh_%Jdet%element_nug%cmn::Extra::dot(
						fsc(n)*Vc.rows(n*3,(n+1)*3-1),
						fsc(m)*Vc.rows(m*3,(m+1)*3-1)));
					Mf2f(m,n) = Mf2f(n,m);
				}
			}

			// walk over source faces
			for(arma::uword j=0;j<n2f_.n_rows;j++){
				// source face index
				const arma::uword sfi = n2f_(j,i);
				assert(sfi<f_.n_cols);

				// get face area for source
				const fltp source_area = face_area_(sfi);
				assert(source_area>RAT_CONST(0.0));

				// direction
				const fltp sfcw = clockwise2sign(fcw_(j,i));

				// walk over target faces
				for(arma::uword k=0;k<n2f_.n_rows;k++){
					// target face index
					const arma::uword tfi = n2f_(k,i);
					assert(tfi<f_.n_cols);

					// check opposing orthogonal faces
					if(!enable_f2f(k,j))continue;

					// get face area for target face
					const fltp target_area = face_area_(tfi);
					assert(target_area>RAT_CONST(0.0));

					// target face clockwise
					const fltp tfcw = clockwise2sign(fcw_(k,i));

					// walk over target edges
					for(arma::uword m=0;m<f2e_.n_rows;m++){
						// target edge index
						const arma::uword tei = f2e_(m,tfi);
						assert(tei<e_.n_cols);

						// get target edge length
						const fltp target_ell = edge_length_(tei);
						assert(target_ell>RAT_CONST(0.0));

						// get direction of the target edge
						const fltp tecw = clockwise2sign(ecw_(m,tfi));

						// create prefactor
						const fltp prefactor = (tecw*target_ell/target_area);

						// walk over source edges
						for(arma::uword l=0;l<f2e_.n_rows;l++){
							// source edge index
							const arma::uword sei = f2e_(l,sfi);
							assert(sei<e_.n_cols);

							// get edge length
							const fltp source_ell = edge_length_(sei);
							assert(source_ell>RAT_CONST(0.0));

							// get direction of source edge
							const fltp secw = clockwise2sign(ecw_(l,sfi));

							// add entry to matrix
							row(cnt) = tei; col(cnt) = sei;
							val(cnt) = prefactor*tfcw*sfcw*Mf2f(j,k)*
								(secw*source_ell/source_area);

							// increment index
							cnt++;
						}

					}

				}
			}
		}

		// sanity check
		assert(cnt==num_fem);

		// calculate delta reluctance
		const arma::Row<fltp> dnu = calc_delta_reluctance(nu);

		// add flux leakage self field term only
		// walk over surface faces
		for(arma::uword i=0;i<fs_.n_elem;i++){
			// face index
			const arma::uword fi = fs_(i);

			// get face area for source
			const fltp face_area = face_area_(fi);

			// get integral
			assert(!self_integrals_.empty());
			const fltp self_integral = self_integrals_(i);

			// get delta reluctance for this face
			const fltp delta_reluctance = dnu(fi);

			// walk over target edges
			for(arma::uword k=0;k<f2e_.n_rows;k++){
				// target edge index
				const arma::uword tei = f2e_(k,fi);
				assert(tei<e_.n_cols);

				// get target edge length
				const fltp target_ell = edge_length_(tei);

				// direction
				const fltp tecw = clockwise2sign(ecw_(k,fi));

				// prefactor
				const fltp prefac = (tecw*target_ell/face_area)*(self_integral*face_area)/(4*arma::Datum<fltp>::pi);

				// walk over source edges
				for(arma::uword j=0;j<f2e_.n_rows;j++){
					// source edge index
					const arma::uword sei = f2e_(j,fi);
					assert(sei<e_.n_cols);

					// get edge length
					const fltp source_ell = edge_length_(sei);

					// direction
					const fltp secw = clockwise2sign(ecw_(j,fi));

					// add entry to sparse matrix
					row(cnt) = tei; col(cnt) = sei;
					val(cnt) = prefac*delta_reluctance*(secw*source_ell/face_area);

					// increment index
					cnt++;
				}
			}
		}

		// sanity check
		assert(cnt==nnz);

		// check matrix
		assert(row.max()<get_num_edges());
		assert(col.max()<get_num_edges());
		assert(val.is_finite());

		// get whether preconditioner needs row sorting or column sorting
		const bool row_sorted = preconditioner_->is_row_sorted();

		// col-wise sorting
		if(row_sorted==false){
			// sort matrix entries by row
			arma::Col<arma::uword> id1 = arma::sort_index(row);
			row = row(id1); col = col(id1); val = val(id1);

			// sort matrix entries by col without disturbing rows
			arma::Col<arma::uword> id2 = arma::stable_sort_index(col);
			row = row(id2); col = col(id2); val = val(id2);
		}

		// row-wise sorting
		else{
			// sort matrix entries by col
			arma::Col<arma::uword> id1 = arma::sort_index(col);
			row = row(id1); col = col(id1); val = val(id1);

			// sort matrix entries by row without disturbing cols
			arma::Col<arma::uword> id2 = arma::stable_sort_index(row);
			row = row(id2); col = col(id2); val = val(id2);
		}

		// find duplicates entries into the sparse matrix
		const arma::Col<arma::uword> duplicates = 
			col.head(nnz-1)==col.tail(nnz-1) &&
			row.head(nnz-1)==row.tail(nnz-1);
			
		// merge duplicates
		if(arma::any(duplicates)){
			// list of values to keep
			arma::Col<arma::uword> sx(nnz, arma::fill::zeros);

			// index of first value
			arma::uword idx_store = 0; sx(0) = 1;

			// walk over values
			for(arma::uword i=1;i<duplicates.n_elem+1;i++){
				if(duplicates(i-1)==0){
					idx_store = i; sx(i) = 1;
				}else{
					val(idx_store) += val(i);
				}
			}

			// remove duplicate values
			const arma::Col<arma::uword> id4 = arma::find(sx);
			row = row(id4); col = col(id4); val = val(id4);
		}

		// add regularization parameter to diagonal of matrix
		val(arma::find(row==col)) += regularization_parameter_;

		// check
		assert(row.max()<get_num_edges());
		assert(col.max()<get_num_edges());

		// store matrix in pre-conditioner object
		preconditioner_->set_matrix(col,row,val,get_num_edges());

		// get time
		time_.mpre_setup_ += timer.toc();
	}

	// setup preconditioner
	void NonLinSolver::setup_preconditioner(){
		// set timer
		arma::wall_clock timer; timer.tic();
		
		// run factorization
		preconditioner_->analyse(); preconditioner_->factorise();

		// add to timer
		time_.factorization_ += timer.toc();
	}


	// setup 
	void NonLinSolver::setup_gauss_volume(){
		// shape function (can be done outside of for loop)
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Hexahedron::shape_function_derivative(Rqh_) : cmn::Tetrahedron::shape_function_derivative(Rqh_);

		// prepare for integrating the magnetization
		gauss_volume_.set_size(n_.n_cols*Rqh_.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++){
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Hexahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN) : cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Hexahedron::jacobian2determinant(J) : cmn::Tetrahedron::jacobian2determinant(J);
			gauss_volume_.cols(i*Rqh_.n_cols,(i+1)*Rqh_.n_cols-1) = Jdet%wqh_;
		}
	}

	// matrix vector product for the stiffness matrix
	arma::Col<fltp> NonLinSolver::calc_stiffness_matrix_mvp(
		const arma::Row<fltp> &nu,
		const arma::Row<fltp> &x){

		// check if the matrices are available
		assert(!Mf2n_.empty()); assert(!Me2f_.empty()); assert(!gauss_volume_.empty());

		// set timer
		arma::wall_clock timer; timer.tic();

		// calculate flux density at gauss nodes
		const arma::Mat<fltp> B = arma::reshape(Mf2n_*(Me2f_*x.t()).eval(),3,n_.n_cols*Rqh_.n_cols);

		// calculate magnetic field
		const arma::Mat<fltp> H = B.each_row()%nu;

		// evaluate integrals and convert to faces
		const arma::Col<fltp> b = Me2f_.t()*(Mf2n_.t()*arma::vectorise(H.each_row()%gauss_volume_)).eval();

		// get time
		time_.stiffness_mvp_ += timer.toc();

		// return 
		return b;
	}

	// calculate flux through facets from vector potential along edges
	arma::Row<fltp> NonLinSolver::calc_flux(const arma::Row<fltp> &Ae)const{
		// check mesh setup
		assert(!Me2f_.empty());

		// calc flux using matrix
		const arma::Row<fltp> phi = (Me2f_*Ae.t()).t();

		// get phi
		return phi;
	}

	// calculate magnetic flux density in facets from vector potential at edges
	arma::Row<fltp> NonLinSolver::calc_facet_field(const arma::Row<fltp> &Ae)const{
		return calc_flux(Ae)/face_area_;
	}

	// precalculate matrix for converting flux 
	// density through faces to field at gauss nodes
	void NonLinSolver::create_face_to_element_matrix(){
		// set timer
		arma::wall_clock timer; timer.tic();

		// show info 
		lg_->msg(2,"%sFace to Element Matrix:%s\n",KBLU,KNRM); 
		lg_->msg("running matrix setup ...\n");

		// derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Hexahedron::shape_function_derivative(Rqh_) : cmn::Tetrahedron::shape_function_derivative(Rqh_);
		const arma::Mat<fltp> Vq = n_.n_rows==8 ? cmn::Hexahedron::nedelec_hdiv_shape_function(Rqh_) : cmn::Tetrahedron::nedelec_hdiv_shape_function(Rqh_);

		// calculate number of nonzeros on the matrix
		const arma::uword nnz = n_.n_cols*3*Rqh_.n_cols*n2f_.n_rows;

		// allocate sparse matrix
		arma::Mat<arma::uword> locations(2,nnz);
		arma::Col<fltp> val(nnz);
		
		// walk over elements
		cmn::parfor(0,n_.n_cols,true,[&](const arma::uword i, const arma::uword /*cpu*/){
		// for(arma::uword i=0;i<n_.n_cols;i++){
			// calculate shape function
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Hexahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN) : cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Hexahedron::jacobian2determinant(J) : cmn::Tetrahedron::jacobian2determinant(J);
			const arma::Mat<fltp> Vc = n_.n_rows==8 ? cmn::Hexahedron::quad2cart_contravariant_piola(Vq,J,Jdet) : cmn::Tetrahedron::quad2cart_contravariant_piola(Vq,J,Jdet);
			assert(arma::all(Jdet>0));

			// this check fails if mesh is not perfectly planar
			// assert(std::abs(arma::accu(Jdet%wqh_) - elem_volume_(i))/std::abs(elem_volume_(i))<1e-6);

			// walk over gauss points
			for(arma::uword j=0;j<Rqh_.n_cols;j++){
				for(arma::uword k=0;k<n2f_.n_rows;k++){
					for(arma::uword m=0;m<3;m++){
						const arma::uword idx = i*Rqh_.n_cols*n2f_.n_rows*3 + j*n2f_.n_rows*3 + k*3 + m;
						locations(0,idx) = 3*(i*Rqh_.n_cols + j) + m;
						locations(1,idx) = n2f_(k,i);
						val(idx) = clockwise2sign(fcw_(k,i))*Vc(3*k+m,j);
					}
				}
			}

		});

		// face to element flux
		Mf2n_ = arma::SpMat<fltp>(true,locations,val,3*n_.n_cols*Rqh_.n_cols,f_.n_cols,true,true);
		// Mn2f_ = Mf2n_.t();

		// show info 
		// lg_->msg("both the matrix %sitself%s and its %stranspose%s are stored\n",KYEL,KNRM,KYEL,KNRM);
		lg_->msg("matrix size: %s%llu%s X %s%llu%s (rows X cols)\n", KYEL, Mf2n_.n_rows, KNRM, KYEL, Mf2n_.n_cols, KNRM);
		lg_->msg("number off nonzeros: %s%llu%s (CSC %s%.2f [MB]%s)\n", KYEL, Mf2n_.n_nonzero, KNRM, KYEL, fltp(2*Mf2n_.n_nonzero + Mf2n_.n_cols + 1)*sizeof(fltp)/1024/1024, KNRM);
		lg_->msg("fill fraction: %s%.2f%s [pct]\n", KYEL, 100*fltp(Mf2n_.n_nonzero)/(Mf2n_.n_rows*Mf2n_.n_cols), KNRM);
		lg_->msg(-2,"\n");

		// count time
		time_.mf2n_setup_ += timer.toc();
	}

	// setup face to edge matrice
	void NonLinSolver::create_edge_to_face_matrix(){
		// set timer
		arma::wall_clock timer; timer.tic();
		
		// show log header
		lg_->msg(2,"%sEdge to Face Matrix:%s\n",KBLU,KNRM); 
		lg_->msg("running matrix setup ...\n");

		// calculate number of nonzeros on the matrix
		const arma::uword nnz = f_.n_cols*f2e_.n_rows;

		// allocate sparse matrix
		arma::Mat<arma::uword> locations(2,nnz);
		arma::Col<fltp> val(nnz);

		// walk over target faces
		for(arma::uword i=0;i<f_.n_cols;i++){
			// walk over edges
			for(arma::uword k=0;k<f2e_.n_rows;k++){
				const arma::uword tei = f2e_(k,i);
				const fltp target_ell = edge_length_(tei);
				const fltp tecw = clockwise2sign(ecw_(k,i));
				
				const arma::uword idx = i*f2e_.n_rows + k;
				locations(0,idx) = i;
				locations(1,idx) = tei;
				val(idx) = (tecw*target_ell);
			}
		}

		// face to element flux
		Me2f_ = arma::SpMat<fltp>(true,locations,val,f_.n_cols,e_.n_cols,true,true);
		// Mf2e_ = Me2f_.t();

		// show info 
		// lg_->msg("both the matrix %sitself%s and its %stranspose%s are stored\n",KYEL,KNRM,KYEL,KNRM);
		lg_->msg("matrix size: %s%llu%s X %s%llu%s (rows X cols)\n", KYEL, Me2f_.n_rows, KNRM, KYEL, Me2f_.n_cols, KNRM);
		lg_->msg("number off nonzeros: %s%llu%s (CSC %s%.2f [MB]%s)\n", KYEL, Me2f_.n_nonzero, KNRM, KYEL, fltp(2*Me2f_.n_nonzero + Me2f_.n_cols + 1)*sizeof(fltp)/1024/1024, KNRM);
		lg_->msg("fill fraction: %s%.2f%s [pct]\n", KYEL, 100*fltp(Me2f_.n_nonzero)/(Me2f_.n_rows*Me2f_.n_cols), KNRM);
		lg_->msg(-2,"\n");

		// count time
		time_.me2f_setup_ += timer.toc();
	}


	// calculate magnetic flux density in element gauss points
	// interpolates from facets using Hdiv shape functions
	arma::Mat<fltp> NonLinSolver::calc_elem_flux_density(const arma::Row<fltp> &Ae)const{
		// set timer
		arma::wall_clock timer; timer.tic();

		// calculate B using matrix
		const arma::Mat<fltp> B = arma::reshape(Mf2n_*calc_flux(Ae).t(),3,n_.n_cols*Rqh_.n_cols);

		// count time
		time_.flux_density_ += timer.toc();

		// convert
		return B;
	}

	// calculate reluctivity
	arma::Row<fltp> NonLinSolver::calc_reluctivity(const arma::Row<fltp> &Bmag)const{
		// create timer
		arma::wall_clock timer; timer.tic();

		// check if HB curve set
		if(hb_curve_.is_empty())rat_throw_line("HB-Curve not set");

		// allocate 
		arma::Row<fltp> nu(n_.n_cols*Rqh_.n_cols);

		// only single hb curve set
		if(hb_curve_.n_elem==1)nu = hb_curve_(0)->calc_reluctivity(Bmag);

		// multiple hb curves
		else{
			// check if indexing set
			if(n2part_.empty())rat_throw_line("parts are not indexed");
		
			// extend indexing to gauss nodes
			const arma::Col<arma::uword> n2part = arma::vectorise(repmat(n2part_,Rqh_.n_cols,1));

			// multiple hb curves
			for(arma::uword i=0;i<hb_curve_.n_elem;i++){
				if(hb_curve_(i)==NULL)rat_throw_line("HB curve is NULL");
				const arma::Col<arma::uword> idx = arma::find(n2part==i);
				nu.cols(idx) = hb_curve_(i)->calc_reluctivity(Bmag.cols(idx));
			}
		}

		// add to time
		time_.reluctance_ += timer.toc();

		// return reluctance
		return nu;
	}

	// calculate reluctivity
	arma::Row<fltp> NonLinSolver::calc_reluctivity_diff(const arma::Row<fltp> &Bmag)const{
		// create timer
		arma::wall_clock timer; timer.tic();

		// check if HB curve set
		if(hb_curve_.is_empty())rat_throw_line("HB-Curve not set");

		// allocate
		arma::Row<fltp> dnudB(n_.n_cols*Rqh_.n_cols);

		// only single hb curve set
		if(hb_curve_.n_elem==1)
			dnudB = hb_curve_(0)->calc_reluctivity_diff(Bmag);

		// multiple hb curves
		else{
			// check if indexing set
			if(n2part_.empty())rat_throw_line("parts are not indexed");
		
			// extend indexing to gauss nodes
			const arma::Col<arma::uword> n2part = arma::vectorise(repmat(n2part_,Rqh_.n_cols,1));

			// multiple hb curves
			for(arma::uword i=0;i<hb_curve_.n_elem;i++){
				if(hb_curve_(i)==NULL)rat_throw_line("HB curve is NULL");
				const arma::Col<arma::uword> idx = arma::find(n2part==i);
				dnudB.cols(idx) = hb_curve_(i)->calc_reluctivity_diff(Bmag.cols(idx));
			}
		}

		// add to time
		time_.dreluctance_ += timer.toc();

		// return slope of reluctance
		return dnudB;
	}

	// update reluctivity
	void NonLinSolver::update_reluctivity(const arma::Row<fltp> &Ae){
		const arma::Row<fltp> Bmag = cmn::Extra::vec_norm(calc_elem_flux_density(Ae));
		nu_ = calc_reluctivity(Bmag); dnudB_ = calc_reluctivity_diff(Bmag);
	}


	// get external target coordinates
	arma::Mat<fltp> NonLinSolver::get_ext_target_coords()const{
		// create coordinates as midpoint between nodes
		return get_gauss_coords();
	}

	// get target points for external field 
	// calculation (i.e. field generated by coils)
	fmm::ShMgnTargetsPr NonLinSolver::get_ext_targets()const{
		// get target coords
		const arma::Mat<fltp> Rt = get_ext_target_coords();

		// create targets
		fmm::ShMgnTargetsPr tar = fmm::MgnTargets::create(Rt);
		tar->set_field_type('H',3);

		// return targets
		return tar;
	}

	// set field from external targets
	void NonLinSolver::set_ext_field(const fmm::ShMgnTargetsPr& tar){
		// check for cancel
		if(lg_->is_cancelled())return;

		// check if field calculated
		assert(tar!=NULL); assert(tar->has_field()); assert(!tar->has_nan());

		// get field from targets
		const arma::Mat<fltp> Hext = tar->get_field('H');

		// set field to self
		set_ext_field(Hext);
	}

	// set field from external targets
	void NonLinSolver::set_ext_dfield(const fmm::ShMgnTargetsPr& tar){
		// check for cancel
		if(lg_->is_cancelled())return;

		// check if field calculated
		assert(tar!=NULL); assert(tar->has_field()); assert(!tar->has_nan());

		// get field from targets
		const arma::Mat<fltp> dHext = tar->get_field('H');

		// set field to self
		set_ext_dfield(dHext);
	}

	// set field from external targets
	void NonLinSolver::set_ext_field(const arma::Mat<fltp>& Hext){
		Hext_ = Hext;
	}

	// set field from external targets
	void NonLinSolver::set_ext_dfield(const arma::Mat<fltp>& dHext){
		dHext_ = dHext;
	}

	// get external field from edges
	const arma::Mat<fltp>& NonLinSolver::get_ext_field()const{
		return Hext_;
	}

	// setup right hand side
	arma::Col<fltp> NonLinSolver::setup_rhs(const arma::Mat<fltp>&Hext){
		// allocate
		arma::Col<fltp> rhs(get_num_edges());

		// shape function (can be done outside of for loop)
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Hexahedron::shape_function_derivative(Rqh_) : cmn::Tetrahedron::shape_function_derivative(Rqh_);
		const arma::Mat<fltp> Vq = n_.n_rows==8 ? cmn::Hexahedron::nedelec_hdiv_shape_function(Rqh_) : cmn::Tetrahedron::nedelec_hdiv_shape_function(Rqh_);

		// walk over hexahedrons
		for(arma::uword i=0;i<n_.n_cols;i++){
			// calculate shape function
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Hexahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN) : cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Hexahedron::jacobian2determinant(J) : cmn::Tetrahedron::jacobian2determinant(J);
			const arma::Mat<fltp> Vc = n_.n_rows==8 ? cmn::Hexahedron::quad2cart_contravariant_piola(Vq,J,Jdet) : cmn::Tetrahedron::quad2cart_contravariant_piola(Vq,J,Jdet);
			assert(arma::all(Jdet>0));

			// this check fails if mesh is not perfectly planar
			// assert(std::abs(arma::accu(Jdet%wqh_) - elem_volume_(i))/std::abs(elem_volume_(i))<RAT_CONST(1e-6));

			// get external field in this hexahedron
			const arma::Mat<fltp> Hexth = Hext.cols(i*Rqh_.n_cols,(i+1)*Rqh_.n_cols-1);

			// get the areas of each face
			const arma::Col<fltp> face_areas = face_area_(n2f_.col(i));

			// field at element faces
			const arma::Col<fltp> fsc = face_areas;

			// walk over target faces
			for(arma::uword k=0;k<n2f_.n_rows;k++){
				// target face index
				const arma::uword tfi = n2f_(k,i);
				assert(tfi<f_.n_cols);

				// target face clockwise
				const fltp tfcw = clockwise2sign(fcw_(k,i));

				// integrate
				const fltp volume_integral = tfcw*arma::accu(wqh_%Jdet%
					cmn::Extra::dot(fsc(k)*Vc.rows(k*3,(k+1)*3-1),Hexth));

				// target face area
				const fltp target_area = face_area_(tfi);

				// target edge index
				const arma::Col<arma::uword> tei = f2e_.col(tfi);

				// get edge lengths
				const arma::Col<fltp> target_ell = edge_length_(tei);

				// direction
				const arma::Col<fltp> tecw = clockwise2sign(ecw_.col(tfi));

				// add entry to sparse matrix
				rhs.rows(tei) += (tecw%target_ell/target_area)*volume_integral;
			}
		}

		// return right hand side vector
		return rhs;
	}

	// setup multipole method for flux leakage calculation
	void NonLinSolver::setup_mlfmm(){
		// set timer
		arma::wall_clock timer; timer.tic();

		// face centers
		arma::Mat<fltp> Rf(3,f_.n_cols);
		for(arma::uword i=0;i<f_.n_cols;i++)
			Rf.col(i) = arma::mean(Rn_.cols(f_.col(i)),1);

		// create settings
		stngs_ = fmm::Settings::create();
		stngs_->set_direct(fmm::DirectMode::NEVER);
		stngs_->set_enable_s2t(false); // using matrix
		stngs_->set_enable_fmm(true);
		#ifdef ENABLE_CUDA_KERNELS
		stngs_->set_enable_gpu(fmm::GpuKernels::get_num_devices()!=0 ? use_gpu_ : false);
		stngs_->set_gpu_devices(gpu_devices_);
		#endif
		stngs_->set_num_exp(8);
		stngs_->set_m2l_sorting(0);
		// #ifdef WIN32
		// stngs_->set_m2l_sorting(0);
		// #else
		// stngs_->set_m2l_sorting(2);
		// #endif
		stngs_->set_num_refine(16*16);
		stngs_->set_refine_stop_criterion(fmm::RefineStopCriterion::TIMES);
		// stngs_->set_num_refine(100);
		// stngs_->set_refine_stop_criterion(fmm::RefineStopCriterion::BOTH);
		// stngs_->set_memory_efficient_s2m(false);
		// stngs_->set_memory_efficient_l2t(false);
		// stngs_->set_split_m2l(true);
		// stngs_->set_single_threaded();
		// stngs_->set_large_ilist();
		stngs_->set_allow_subdivision(false); // this is to prevent subdivision of the elements as this would break updating the charge

		// longest edge refinement limit
		// not sure if we need this grid refinement size limit
		// looks like maybe for 1/r potentials we don't need it?
		if(limit_refinement_){
			const fltp longest_edge_length = arma::max(cmn::Extra::vec_norm(Rn_.cols(e_.row(1)) - Rn_.cols(e_.row(0))));
			stngs_->set_size_limit(longest_edge_length); // use edge length for element size
		}

		// create sources for all faces
		src_ = MgnCharges::create(Rf,arma::Row<fltp>(Rf.n_cols,arma::fill::zeros));

		// create targets at surface of mesh
		tar_ = MgnPotentials::create(Rf.cols(fs_));

		// create mlfmm
		mlfmm_ = fmm::Mlfmm::create(src_,tar_,stngs_);

		// setup mlfmm
		mlfmm_->setup(lg_);

		// get time
		time_.mlfmm_setup_ += timer.toc();
	}

	// average from gauss points to elements
	arma::Mat<fltp> NonLinSolver::element_volume_integral(const arma::Mat<fltp>&v)const{
		// check
		assert(v.n_cols==Rqh_.n_cols*n_.n_cols);

		// derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Hexahedron::shape_function_derivative(Rqh_) : cmn::Tetrahedron::shape_function_derivative(Rqh_);

		// allocate
		arma::Mat<fltp> vi(v.n_rows,n_.n_cols);

		// walk over elements
		for(arma::uword i=0;i<n_.n_cols;i++){
			// calculate determinant
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Hexahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN) : cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Hexahedron::jacobian2determinant(J) : cmn::Tetrahedron::jacobian2determinant(J);
			assert(arma::all(Jdet>0));

			// this check fails if mesh is not perfectly planar
			// assert(std::abs(arma::accu(Jdet%wqh_) - elem_volume_(i))/std::abs(elem_volume_(i))<1e-6);
			
			// calculate volume integral
			vi.col(i) = arma::sum(v.cols(i*Rqh_.n_cols,(i+1)*Rqh_.n_cols-1).eval().each_row()%(wqh_%Jdet),1);
		}

		// return integrated values
		return vi;
	}

	arma::Mat<fltp> NonLinSolver::element_average(const arma::Mat<fltp>&v)const{
		return (element_volume_integral(v).each_row()/elem_volume_);
	}

	// calculate delta reluctance over faces
	arma::Row<fltp> NonLinSolver::calc_delta_reluctance(
		const arma::Row<fltp> &nu, 
		const bool is_jacobian)const{

		// reluctivity of vacuum/air
		const fltp nu0 = RAT_CONST(1.0)/arma::Datum<fltp>::mu_0;

		// calculate average reluctance in elements
		const arma::Row<fltp> avnu = element_average(nu);

		// calculate delta flux term over faces
		arma::Row<fltp> dnu(f_.n_cols,arma::fill::zeros);
		for(arma::uword i=0;i<n_.n_cols;i++){
			// calculate element averaged reluctance
			const fltp element_reluctance = avnu(i);
			for(arma::uword j=0;j<n2f_.n_rows;j++){
				const arma::uword fid = n2f_(j,i);
				const fltp cw = clockwise2sign(fcw_(j,i));
				dnu(fid) -= cw*element_reluctance;
			}
		}

		// subtract reluctance of vacuum/air from surface faces
		// when using derivative of nu in the jacobian this 
		// addition is not necessary
		if(!is_jacobian)dnu.cols(fs_) += nu0;

		// return delta reluctance
		return dnu;
	}

	// calculate face integral of 1/r for surface fraces
	
	arma::Row<fltp> NonLinSolver::calc_surface_integrals()const{
		// allocate scalar potential for each surface face
		arma::Row<fltp> phi(fs_.n_elem,arma::fill::zeros);

		// derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Quadrilateral::shape_function_derivative(Rqs_) : cmn::Triangle::shape_function_derivative(Rqs_);

		// walk over surface faces
		for(arma::uword i=0;i<fs_.n_elem;i++){
			// target id
			const arma::uword tfid = fs_(i);

			// gauss points
			const arma::Mat<fltp> Rt = n_.n_rows==8 ? cmn::Quadrilateral::quad2cart(Rn_.cols(f_.col(tfid)), Rqs_) : cmn::Triangle::quad2cart(Rn_.cols(f_.col(tfid)), Rqs_);

			// determinant of jacobian
			const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Quadrilateral::shape_function_jacobian(Rn_.cols(f_.col(tfid)),dN) : cmn::Triangle::shape_function_jacobian(Rn_.cols(f_.col(tfid)),dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Quadrilateral::jacobian2determinant(J) : cmn::Triangle::jacobian2determinant(J);
			assert(arma::all(Jdet>0));

			// this check fails if mesh is not perfectly planar
			// assert(std::abs(arma::accu(Jdet%wqs_) - face_area_(tfid))/std::abs(face_area_(tfid))<1e-6);

			// get area
			const fltp target_area = face_area_(tfid);

			// // gauss point approach
			// for(arma::uword m=0;m<Rt.n_cols;m++){
			// 	const arma::Row<fltp> r = cmn::Extra::vec_norm(Rt.col(m) - Rt.each_col());
			// 	const arma::Col<arma::uword> id = arma::find(r>0);
			// 	phi(i) += arma::accu(wqf_(m)*Jdet(m)*(wqf_(id)*Jdet(id)/r(id)))/target_area;
			// }

			// use charged triangle script instead
			phi(i) = arma::accu(wqs_%Jdet%rat::fmm::ChargedPolyhedron::calc_scalar_potential(Rn_.cols(f_.col(tfid)), Rt))/target_area;

			// Following equations are adapted from
			// A.G.A.M. Armstrong et. al., "New developments in the magnet design computer program gfun", 
			// Presented at the 5th International Conference on Magnet Technology, Frascati, Rome, 1975.
			
			// // calculate face normal unit vector
			// const arma::Col<fltp>::fixed<3> zeta = Nf_.col(tfid); // unit vector normal to face

			// // walk over edges
			// for(arma::uword j=0;j<f2e_.n_rows;j++){
			// 	// get edge index
			// 	const arma::uword eid = f2e_(j,tfid);

			// 	// get face nodes
			// 	arma::Mat<fltp>::fixed<3,2> Re = Rn_.cols(e_.col(eid));

			// 	// make edge clockwise with respect to face
			// 	if(ecw_(j,tfid)==0)Re = arma::fliplr(Re);

			// 	// edge unit vector
			// 	const arma::Col<fltp>::fixed<3> Vg = arma::diff(Re,1,1);
			// 	const arma::Col<fltp>::fixed<3> xi = Vg.each_row()/cmn::Extra::vec_norm(Vg); // unit vector along edge

			// 	// plane unit vector
			// 	const arma::Col<fltp>::fixed<3> nu = cmn::Extra::cross(xi,zeta); // unit vector across the edge
				
			// 	// relative position of start and end point
			// 	const arma::Mat<fltp> R0 = Rt.each_col() - Re.col(0);
			// 	const arma::Mat<fltp> R1 = Rt.each_col() - Re.col(1);

			// 	// distance between points
			// 	const arma::Row<fltp> r0 = cmn::Extra::vec_norm(R0);
			// 	const arma::Row<fltp> r1 = cmn::Extra::vec_norm(R1);

			// 	// in local coordinates
			// 	const arma::Row<fltp> rxi0 = arma::sum(xi%R0.each_col());
			// 	const arma::Row<fltp> rxi1 = arma::sum(xi%R1.each_col());
			// 	const arma::Row<fltp> rnu = arma::sum(nu%R0.each_col());
			// 	const arma::Row<fltp> rzeta = arma::sum(zeta%R0.each_col());

			// 	// add edge contribution to phi
			// 	phi(i) += arma::accu(wqs_%Jdet%rnu%(
			// 		rxi1/(r1 + arma::abs(rzeta)) - 
			// 		rxi0/(r0 + arma::abs(rzeta))))/target_area;
			// }
		}

		// need to check sign
		assert(arma::all(phi>=0));

		// return face integral over 1/r
		return phi;
	}

	// setup self field integrals
	void NonLinSolver::setup_surface_integrals(){
		self_integrals_ = calc_surface_integrals();
	}

	// calculate flux leakage
	arma::Col<fltp> NonLinSolver::calc_flux_leakage(
		const arma::Row<fltp> &nu,
		const arma::Row<fltp> &Ae,
		const bool is_jacobian){

		// create timer
		arma::wall_clock timer; timer.tic();

		// calculate delta reluctance for each face
		const arma::Row<fltp> dnu = calc_delta_reluctance(nu,is_jacobian); // is the sign correct here?

		// calculate magnetic flux density normal to each face
		const arma::Row<fltp> Bnf = calc_facet_field(Ae);

		// calculate magnetic charge on the surface (delta magnetization in face normal direction)
		const arma::Row<fltp> dM = dnu%Bnf; // the sign does not matter for potential

		// set charges
		src_->set_magnetic_charge(dM%face_area_);

		// add to timer
		time_.flux_leakage_ += timer.toc();

		// set timer
		timer.tic();

		// run multipole method
		mlfmm_->calculate();

		// get time
		time_.mlfmm_ += timer.toc();

		// check for cancelled
		if(lg_->is_cancelled())return arma::Col<fltp>(e_.n_elem,arma::fill::zeros);

		// restart timer
		timer.tic();
		
		// dim_t nt = bli_thread_get_num_threads();
		// bli_thread_set_num_threads(62);
		const int prev_nt = omp_get_num_threads();
		if(!use_parallel_s2t_matrix_)omp_set_num_threads(std::thread::hardware_concurrency());

		// get potential
		const arma::Row<fltp> phi = tar_->get_field('S') + ((use_parallel_s2t_matrix_ ? Ms2t_par_*dM.t() : Ms2t_*dM.t())).t() + self_integrals_%dM.cols(fs_);
			
		// revert thread setting
		if(!use_parallel_s2t_matrix_)omp_set_num_threads(prev_nt);
		// bli_thread_set_num_threads(nt);

		// get time
		time_.source2target_ += timer.toc();

		// reset timer
		timer.tic();

		// allocate flux leakage though each edge
		arma::Col<fltp> flux_leakage(get_num_edges(),arma::fill::zeros);

		// walk over surface faces
		for(arma::uword i=0;i<fs_.n_elem;i++){
			// face index
			const arma::uword fid = fs_(i);

			// area of the face
			const fltp target_area = face_area_(fid);

			// get potential
			const fltp scalar_potential = phi(i);

			// walk over (surface) edges
			for(arma::uword j=0;j<f2e_.n_rows;j++){
				// edge index
				const arma::uword eid = f2e_(j,fid);

				// clockwise
				const fltp cw = clockwise2sign(ecw_(j,fid));

				// get length of edge
				const fltp target_length = edge_length_(eid);

				// calculate curl with
				flux_leakage(eid) += target_area*(cw*target_length/target_area)*scalar_potential;
			}
		}

		// divide by 4PI
		flux_leakage/=(4*arma::Datum<fltp>::pi);

		// add to timer
		time_.flux_leakage_ += timer.toc();

		// return column vector
		return flux_leakage;
	}


	// SOLVER FUNCTIONS FOR NEWTON
	// ================================
	// analyse kinsol output flag and send to log
	void NonLinSolver::flag2msg(const int flag)const{
		// check for cancelled
		if(lg_->is_cancelled())
			lg_->msg("%sKINSol was cancelled by user.%s\n",KYEL,KNRM);
		else if(flag==KIN_SUCCESS)
			lg_->msg("%sKINSol() succeeded; the scaled norm of F(u) is less than fnormtol.%s\n",KGRN,KNRM);
		else if(flag==KIN_INITIAL_GUESS_OK){
			lg_->msg("%sThe guess u=u0 satisfied the system F(u)=0 within the tolerances%s\n",KGRN,KNRM);
			lg_->msg("%sspecified (the scaled norm of F(u0) is less than 0.01*fnormtol).%s\n",KGRN,KNRM);
		}
		else if(flag==KIN_STEP_LT_STPTOL){
			lg_->msg("%sKINSOL stopped based on scaled step length. This means that%s\n",KYEL,KNRM);
			lg_->msg("%sthe current iterate may be an approximate solution of the given%s\n",KYEL,KNRM);
			lg_->msg("%snonlinear system, but it is also quite possible that the algorithm%s\n",KYEL,KNRM);
			lg_->msg("%sis \"stalled\" (making insufficient progress) near an invalid solution,%s\n",KYEL,KNRM);
			lg_->msg("%sor that the scalar scsteptol is too large (see KINSetScaledStepTol()%s\n",KYEL,KNRM);
			lg_->msg("%sin paragraph 8.4.5.4 to change scsteptol from its default value).%s\n",KYEL,KNRM);
		}
		else if(flag==KIN_MEM_NULL)
			lg_->msg("%sThe KINSOL memory block pointer was NULL.%s\n",KRED,KNRM);
		else if(flag==KIN_ILL_INPUT)
			lg_->msg("%sAn input parameter was invalid.%s\n",KRED,KNRM);
		else if(flag==KIN_NO_MALLOC)
			lg_->msg("%sThe KINSOL memory was not allocated by a call to KINCreate().%s\n",KRED,KNRM);
		else if(flag==KIN_MEM_FAIL)
			lg_->msg("%sA memory allocation failed.%s\n",KRED,KNRM);
		else if(flag==KIN_LINESEARCH_NONCONV){
			lg_->msg("%sThe line search algorithm was unable to find an iterate sufficiently%s\n",KRED,KNRM);
			lg_->msg("%sdistinct from the current iterate, or could not find an iterate%s\n",KRED,KNRM);
			lg_->msg("%ssatisfying the sufficient decrease condition. Failure to satisfy%s\n",KRED,KNRM);
			lg_->msg("%sthe sufficient decrease condition could mean the current iterate%s\n",KRED,KNRM);
			lg_->msg("%sis \"close\" to an approximate solution of the given nonlinear system,%s\n",KRED,KNRM);
			lg_->msg("%sthe difference approximation of the matrix-vector product is inaccurate,%s\n",KRED,KNRM);
			lg_->msg("%sor the real scalar scsteptol is too large.%s\n",KRED,KNRM);
		}
		else if(flag==KIN_MAXITER_REACHED)
			lg_->msg("%sThe maximum number of nonlinear iterations has been reached.%s\n",KRED,KNRM);
		else if(flag==KIN_MXNEWT_5X_EXCEEDED){
			lg_->msg("%sFive consecutive steps have been taken that satisfy the inequality,%s\n",KRED,KNRM);
			lg_->msg("%swhere denotes the current step and mxnewtstep is a scalar upper bound%s\n",KRED,KNRM);
			lg_->msg("%son the scaled step length. Such a failure may mean that asymptotes%s\n",KRED,KNRM);
			lg_->msg("%sfrom above to a positive value, or the real scalar mxnewtstep is too small.%s\n",KRED,KNRM);
		}
		else if(flag==KIN_LINESEARCH_BCFAIL){
			lg_->msg("%sThe line search algorithm was unable to satisfy the \"beta-condition\"%s\n",KRED,KNRM);
			lg_->msg("%sfor MXNBCF+1 nonlinear iterations (not necessarily consecutive), which%s\n",KRED,KNRM);
			lg_->msg("%smay indicate the algorithm is making poor progress.%s\n",KRED,KNRM);
		}
		else if(flag==KIN_LINSOLV_NO_RECOVERY){
			lg_->msg("%sThe user-supplied routine psolve encountered a recoverable%s\n",KRED,KNRM);
			lg_->msg("%serror, but the preconditioner is already current.%s\n",KRED,KNRM);
		}
		else if(flag==KIN_LINIT_FAIL)
			lg_->msg("%sThe KINLS initialization routine (linit) encountered an error.%s\n",KRED,KNRM);
		else if(flag==KIN_LSETUP_FAIL){
			lg_->msg("%sThe KINLS setup routine (lsetup) encountered an error; e.g., the%s\n",KRED,KNRM);
			lg_->msg("%suser-supplied routine pset (used to set up the preconditioner data)%s\n",KRED,KNRM);
			lg_->msg("%sencountered an unrecoverable error.%s\n",KRED,KNRM);
		}
		else if(flag==KIN_LSOLVE_FAIL){
			lg_->msg("%sThe KINLS solve routine (lsolve) encountered an error; e.g.,%s\n",KRED,KNRM);
			lg_->msg("%sthe user-supplied routine psolve (used to to solve the%s\n",KRED,KNRM);
			lg_->msg("%spreconditioned linear system) encountered an unrecoverable error.%s\n",KRED,KNRM);
		}
		else if(flag==KIN_SYSFUNC_FAIL)
			lg_->msg("%sThe system function failed in an unrecoverable manner.%s",KRED,KNRM);
		else if(flag==KIN_FIRST_SYSFUNC_ERR)
			lg_->msg("%sThe system function failed recoverably at the first call.%s",KRED,KNRM);
		else if(flag==KIN_REPTD_SYSFUNC_ERR){
			lg_->msg("%sThe system function had repeated recoverable errors.%s\n",KRED,KNRM);
			lg_->msg("%sNo recovery is possible.%s\n",KRED,KNRM);
		}
	}

	// reset timings
	void NonLinSolver::reset_timings()const{
		// just create a new struct
		time_ = Timings();
	}

	// display timing
	void NonLinSolver::display_timings()const{
		lg_->msg(2,"%sSetup Timings:%s\n",KBLU,KNRM);
		lg_->msg("MLFMM setup: %s%06.2e%s [s]\n",KYEL,time_.mlfmm_setup_,KNRM);
		lg_->msg("Ms2t setup: %s%06.2e%s [s]\n",KYEL,time_.ms2t_setup_,KNRM);
		lg_->msg("Mf2n setup: %s%06.2e%s [s]\n",KYEL,time_.mf2n_setup_,KNRM);
		lg_->msg("Me2f setup: %s%06.2e%s [s]\n",KYEL,time_.me2f_setup_,KNRM);
		lg_->msg("Mprec setup: %s%06.2e%s [s]\n",KYEL,time_.mpre_setup_,KNRM);
		lg_->msg(-2,"\n");

		lg_->msg(2,"%sSpecific Timings:%s\n",KBLU,KNRM);
		lg_->msg("multipole method: %s%06.2e%s [s]\n",KYEL,time_.mlfmm_,KNRM);
		lg_->msg("stiffness mvp: %s%06.2e%s [s]\n",KYEL,time_.stiffness_mvp_,KNRM);
		lg_->msg("source to target: %s%06.2e%s [s]\n",KYEL,time_.source2target_,KNRM);
		lg_->msg("flux leakage: %s%06.2e%s [s]\n",KYEL,time_.flux_leakage_,KNRM);
		lg_->msg("factorization: %s%06.2e%s [s]\n",KYEL,time_.factorization_,KNRM);
		lg_->msg("flux density: %s%06.2e%s [s]\n",KYEL,time_.flux_density_,KNRM);
		lg_->msg("reluctance: %s%06.2e%s [s]\n",KYEL,time_.reluctance_,KNRM);
		lg_->msg("dreluctance: %s%06.2e%s [s]\n",KYEL,time_.dreluctance_,KNRM);
		lg_->msg(-2,"\n");

		lg_->msg(2,"%sSolver Time Breakdown:%s\n",KBLU,KNRM);
		lg_->msg("system function: %s%06.2e%s [s]\n",KYEL,time_.system_fn_,KNRM);
		lg_->msg("jtv function: %s%06.2e%s [s]\n",KYEL,time_.jtv_fn_,KNRM);
		lg_->msg("prec setup function: %s%06.2e%s [s]\n",KYEL,time_.prec_setup_fn_,KNRM);
		lg_->msg("prec solve function: %s%06.2e%s [s]\n",KYEL,time_.prec_solve_fn_,KNRM);
		lg_->msg("kinsol: %s%06.2e%s [s]\n",KYEL,time_.solve_ - time_.system_fn_ - time_.jtv_fn_ - time_.prec_setup_fn_ - time_.prec_solve_fn_,KNRM);
		lg_->msg("total: %s%06.2e%s [s]\n",KYEL,time_.solve_,KNRM);
		lg_->msg(-2,"\n");
	}

	// display mesh quality
	void NonLinSolver::display_mesh_analysis()const{
		// header
		lg_->msg(2,"%sMesh Analysis%s\n",KBLU,KNRM);

		// number of merged nodes
		const arma::uword num_merged = original2merged_.n_elem - static_cast<arma::sword>(original2merged_.max()) - 1;

		// element counts
		lg_->msg("number of nodes: %s%llu%s (merged %s%llu%s)\n",KYEL,Rn_.n_cols,KNRM,KYEL,num_merged,KNRM);
		lg_->msg("number of elements: %s%llu%s\n",KYEL,n_.n_cols,KNRM);
		lg_->msg("number of facets: %s%llu%s\n",KYEL,f_.n_cols,KNRM);
		lg_->msg("number of edges: %s%llu%s\n",KYEL,e_.n_cols,KNRM);
		lg_->msg("number of surface facets: %s%llu%s\n",KYEL,fs_.n_elem,KNRM);

		// element deformation
		const arma::Row<fltp> skewedness = n_.n_rows==8 ? cmn::Hexahedron::calc_skewedness(Rn_,n_) : cmn::Tetrahedron::calc_skewedness(Rn_,n_);
		const arma::Row<fltp> aspect_ratio = n_.n_rows==8 ? cmn::Hexahedron::calc_aspect_ratio(Rn_,n_) : cmn::Tetrahedron::calc_aspect_ratio(Rn_,n_);
		lg_->msg("minimum skewedness: %s%2.2f%s (%s%2.2f [deg]%s)\n",KYEL,arma::min(skewedness),KNRM,KYEL,360.0*std::asin(arma::min(skewedness))/arma::Datum<fltp>::tau,KNRM);
		lg_->msg("maximum skewedness: %s%2.2f%s (%s%2.2f [deg]%s)\n",KYEL,arma::max(skewedness),KNRM,KYEL,360.0*std::asin(arma::max(skewedness))/arma::Datum<fltp>::tau,KNRM);
		lg_->msg("minimum aspect ratio: %s%2.2f%s\n",KYEL,arma::min(aspect_ratio),KNRM);
		lg_->msg("maximum aspect ratio: %s%2.2f%s\n",KYEL,arma::max(aspect_ratio),KNRM);
		
		// check if mesh is planar
		// currently for example hex sphere is not planar and we 
		// want the user to continue at his own risk
		if(n_.n_rows==8){
			if(!mesh_is_planar()){
				lg_->msg("%s%squadrilaterals are not perfectly planar!%s\n",KBLD,KRED,KNRM);
				lg_->msg("%s%sattempting to solve anyways ... %s\n",KBLD,KRED,KNRM);
			}
		}

		// mesh analysis done
		lg_->msg(-2,"\n");
	}
	

	// get starting point from persistent memory
	arma::Col<fltp> NonLinSolver::get_Ae0(const ShSolverCachePr& cache, const arma::Col<fltp>& fscale){
		// Create linear solver object with right preconditioning 
		lg_->msg(2,"%sFind Starting Point%s\n",KBLU,KNRM); 

		// // create background field
		// fmm::ShBackgroundPr bg = fmm::Background::create();
		// bg->set_magnetic({RAT_CONST(1.0)/arma::Datum<fltp>::mu_0,0.0,0.0});
		// const arma::Mat<fltp> Re = Rn_.cols(e_.row(0)) + Rn_.cols(e_.row(1))/2;
		// arma::Mat<fltp> Ve = Rn_.cols(e_.row(1)) - Rn_.cols(e_.row(0)); 
		// Ve.each_row()/=cmn::Extra::vec_norm(Ve);
		
		// // setup targets and calculate field
		// fmm::ShMgnTargetsPr tar = fmm::MgnTargets::create(Re);
		// tar->set_field_type('A',3); tar->allocate();
		// bg->calc_field(tar);

		// // get vector potential and calculate component along edges
		// arma::Col<fltp> Ae0 = cmn::Extra::dot(Ve,tar->get_field('A')).t();

		// zero field solution
		arma::Col<fltp> Ae0(get_num_edges(), arma::fill::zeros); 

		// check if there is a cache available
		if(cache!=NULL){
			// report
			lg_->msg("searching in cache\n");

			// check if there are solutions with this number of edges
			if(cache->get_num_solutions(e_.n_cols)>0){
				// get a list of candidates
				const std::list<arma::Col<fltp> >& Ae_cache = cache->get_solutions(e_.n_cols);

				// function values
				arma::Col<fltp> Aec(e_.n_cols,arma::fill::zeros);
				arma::Col<fltp> fval(e_.n_cols,arma::fill::zeros);

				// map to nvector
				N_Vector nvec_Aec = conv2nvec(Aec,sunctx_);
				N_Vector nvec_fval = conv2nvec(fval,sunctx_);

				// set norm
				lg_->msg(2,"trying available solutions with %s%llu%s edges\n",KYEL,e_.n_cols,KNRM);

				// table header
				lg_->msg("%s%5s %4s %8s%s\n",KCYN,"id","flag","fnorm",KNRM);
				lg_->msg("%s===================%s\n",KCYN,KNRM);

				// solution counter	
				arma::uword cnt = 0;

				// allocate for all cache solutions and an 
				// additional solution for constant background field
				arma::Col<fltp> fnorm(Ae_cache.size()+1,arma::fill::value(arma::Datum<fltp>::inf));
				arma::Col<int> flag(Ae_cache.size()+1,arma::fill::zeros);

				// default solution
				{
					// background field 
					Aec = Ae0;

					// try system function
					flag(cnt) = nt_systemfn(nvec_Aec, nvec_fval, static_cast<void*>(this));
					fnorm(cnt) = arma::norm(fscale%fval);
				
					// output table
					lg_->msg("%s%05llu %4i %8.2e%s\n",KCYN,cnt,flag(cnt),fnorm(cnt),KNRM);

					// done
					cnt++;
				}

				// walk over candidates
				for(auto it=Ae_cache.begin();it!=Ae_cache.end();it++){
					// set to Ae this will automatically also 
					// set it to the Nvector as they share memory
					Aec = (*it);

					// try system function
					flag(cnt) = nt_systemfn(nvec_Aec, nvec_fval, static_cast<void*>(this));
					fnorm(cnt) = arma::norm(fscale%fval);

					// output table
					lg_->msg("%s%05llu %4i %8.2e%s\n",KCYN,cnt,flag(cnt),fnorm(cnt),KNRM);

					// done
					cnt++;
				}

				// show best solution
				const arma::uword idx_best = fnorm.index_min();
				if(idx_best==0){
					lg_->msg("%susing constant flux density as starting point%s\n",KGRN,KNRM);
				}else{
					lg_->msg("%susing cache %s%llu%s as starting point%s\n",KGRN,KYEL,idx_best,KGRN,KNRM);
					auto it=Ae_cache.begin();
					std::advance(it,idx_best-1);
					Ae0 = (*it);
				}

				// table done
				lg_->msg(-2);

				// destroy the nvector
				destroy_nvector(nvec_Aec);
				destroy_nvector(nvec_fval);
			}else{
				lg_->msg("%sthere are no solutions with required%s\n",KGRN,KNRM);
				lg_->msg("%snumber of edges using constant flux density%s\n",KGRN,KNRM);
			}	
		}else{
			lg_->msg("%sno cache provided using constant flux density%s\n",KGRN,KNRM);
		}

		// done
		lg_->msg(-2,"\n");

		// return starting point
		return Ae0;
	}

	// create linear solver at LS
	void NonLinSolver::create_linear_solver(N_Vector& nvec_Ae, const bool dsolve){
		// create flag
		int flag = 0;

		// settings
		const int maxl = dsolve ? dmaxl_ : maxl_;
		const int maxlrst = dsolve ? dmaxlrst_ : maxlrst_;

		// Fast-Generalized Minimal Residual Method
		if(linear_solver_type_==LinsolType::FGMRES){
			// report settings for linear solver
			lg_->msg("using %sFGMRES%s algorithm\n",KYEL,KNRM);

			// create FGMRES linear solver object
			LS_ = SUNLinSol_SPFGMR(nvec_Ae, preconditioner_side_, maxl, sunctx_); // FGMRES
			// SUNLinearSolver LS = SUNLinSol_SPGMR(nvec_Ae, PREC_RIGHT, maxl, sunctx_); // GMRES

			// Set the maximum number of restarts
			flag = SUNLinSol_SPFGMRSetMaxRestarts(LS_, maxlrst);
			if(flag==KINLS_MEM_NULL)rat_throw_line("the SUNLinearSolver memory was NULL");
			if(flag==KINLS_ILL_INPUT)rat_throw_line("SUNDIALS was not built with monitoring enabled");
			if(flag!=KINLS_SUCCESS)rat_throw_line("could not set FGMRES number of restarts");

			// subspace size 
			lg_->msg("maximum krylov subspace size: %s%llu%s\n",KYEL,maxl,KNRM); 
			lg_->msg("maximum basis vector memory requirement: %s%.2f MB%s (flexible)\n",
				KYEL,maxl_*e_.n_cols*fltp(sizeof(sunrealtype))/1024/1024,KNRM);
			lg_->msg("maximum number of restarts: %s%llu%s\n",KYEL,maxlrst,KNRM); 
		}

		// Generalized Minimal Residual Method
		else if(linear_solver_type_==LinsolType::GMRES){
			// report settings for linear solver
			lg_->msg("using %sGMRES%s algorithm\n",KYEL,KNRM);

			// create FGMRES linear solver object
			LS_ = SUNLinSol_SPGMR(nvec_Ae, preconditioner_side_, maxl, sunctx_); // FGMRES

			// Set the maximum number of restarts
			flag = SUNLinSol_SPGMRSetMaxRestarts(LS_, maxlrst);
			if(flag==KINLS_MEM_NULL)rat_throw_line("the SUNLinearSolver memory was NULL");
			if(flag==KINLS_ILL_INPUT)rat_throw_line("SUNDIALS was not built with monitoring enabled");
			if(flag!=KINLS_SUCCESS)rat_throw_line("could not set FGMRES number of restarts");

			// subspace size 
			lg_->msg("maximum krylov subspace size: %s%llu%s\n",KYEL,maxl,KNRM); 
			lg_->msg("maximum basis vector memory requirement: %s%.2f MB%s\n",
				KYEL,maxl_*e_.n_cols*fltp(sizeof(sunrealtype))/1024/1024,KNRM);
			lg_->msg("maximum number of restarts: %s%llu%s\n",KYEL,maxlrst,KNRM); 
		}

		// biconjugent gradient method
		else if(linear_solver_type_==LinsolType::BCGS){
			// create BCGS linear solver object
			LS_ = SUNLinSol_SPBCGS(nvec_Ae, preconditioner_side_, maxl, sunctx_); // BICGSTAB
		}

		// matrix free
		else if(linear_solver_type_==LinsolType::TFQMR){
			// create TFQMR linear solver object
			LS_ = SUNLinSol_SPTFQMR(nvec_Ae, preconditioner_side_, maxl, sunctx_); // TFQMR
		}

		// unknown solver type
		else{
			rat_throw_line("could not recognize linear solver type");
		}
	}
	
	// setup the preconditioner based on the linear_solver_preconditioner setting
	void NonLinSolver::create_preconditioner(){
		switch(preconditioner_type_){
			case PreconditionerType::JACOBI:{
				preconditioner_ = PrecJacobi::create();
				break;
			}
			#ifdef ENABLE_SUPERLU
			case PreconditionerType::SUPERLU:{
				preconditioner_ = PrecSuperLU::create();
				break;
			}
			#endif
			#ifdef ENABLE_CHOLMOD
			case PreconditionerType::CHOLMOD:{
				preconditioner_ = PrecCholMod::create();
				break;
			}
			#endif
			#ifdef ENABLE_UMFPACK
			case PreconditionerType::UMFPACK:{
				preconditioner_ = PrecUMFPack::create();
				break;
			}
			#endif
			#ifdef ENABLE_QDLDL
			case PreconditionerType::QDLDL:{
				preconditioner_ = PrecQDLDL::create();
				break;
			}
			#endif
			#ifdef ENABLE_PARU
			case PreconditionerType::PARU:{
				preconditioner_ = PrecParU::create();
				break;
			}
			#endif
			default: rat_throw_line("preconditioner not enabled during compilation or not recognized");
		}
	}

	// determine scaling using zero solution
	rat::fltp NonLinSolver::get_scaling(){
		// number of equations
		const arma::uword neq = get_num_edges();

		// create vectors with zeros
		arma::Col<fltp> Ae00(neq,arma::fill::zeros);
		arma::Col<fltp> fval00(neq,arma::fill::zeros);

		// convert to n-vectors
		N_Vector nvec_Ae00 = conv2nvec(Ae00,sunctx_);
		N_Vector nvec_fval00 = conv2nvec(fval00,sunctx_);

		// run system function
		const int flag = nt_systemfn(nvec_Ae00, nvec_fval00, static_cast<void*>(this));

		// find the function norm
		const fltp fnorm00 = arma::norm(fval00); // unscaled

		// destroy the n-vectors
		destroy_nvector(nvec_Ae00); 
		destroy_nvector(nvec_fval00);

		// calculate scaling based on the function norm or 
		// the number of equations if the norm is too small
		const rat::fltp scaling = fnorm00>RAT_CONST(1e-8) && flag==KIN_SUCCESS ? RAT_CONST(1.0)/fnorm00 : RAT_CONST(1.0)/neq;

		// return the scaling
		return scaling;
	}

	// solve system using newton method
	arma::Col<fltp> NonLinSolver::solve(const ShSolverCachePr& cache){

		// reset timings
		reset_timings();

		// setup mlfmm
		setup_mlfmm();

		// calculate number of equations
		const arma::uword neq = get_num_edges();

		// only run the next section if the cancel flag is not set
		if(!lg_->is_cancelled()){
			// Create linear solver object with right preconditioning 
			lg_->msg(2,"%s%sSetup System of Equations%s\n",KBLD,KGRN,KNRM); 

			// show mesh analysis table
			display_mesh_analysis();
	
			// right hand side
			rhs_ = setup_rhs(Hext_);

			// setup source to target matrix
			create_source_to_target_matrix();
		}

		// only run the next section if the cancel flag is not set
		if(!lg_->is_cancelled()){
			// check mesh setup
			assert(!Ms2t_.empty());

			// setup matrix for converting flux through faces to field inside elements
			create_face_to_element_matrix();

			// check mesh setup
			assert(!Mf2n_.empty());

			// setup face to edge matrix
			create_edge_to_face_matrix();

			// check mesh setup
			assert(!Me2f_.empty());

			// setup the self field integrals
			setup_surface_integrals();

			// check mesh setup
			assert(!self_integrals_.empty());

			// setup volume for gauss points
			setup_gauss_volume();

			// setup finished
			lg_->msg(-2,"\n");
		}

		// allocate output vector
		arma::Col<fltp> Ae;

		// only run the next section if the cancel flag is not set
		if(!lg_->is_cancelled()){
			// output flag for sundials and kinsol commands
			int flag = 0;

			// get version
			int major,minor,patch; char version_label[10]; 
			flag = SUNDIALSGetVersionNumber(&major, &minor, &patch, version_label, 10);
			if(flag!=0)rat_throw_line("can not retreive version number");
			lg_->msg(2,"%s%sStarting SUNDIALS version: %s%i.%i.%i %s%s\n",KBLD,KGRN,KYEL,major,minor,patch,version_label,KNRM); 

			// create context
			flag = SUNContext_Create(SUN_COMM_NULL, &sunctx_);
			const SUNErrCode code = SUNContext_PushErrHandler(sunctx_, NonLinSolver::err_fn, this);
			if(code!=SUN_SUCCESS)rat_throw_line("could not set error handler function");

			// try system function
			const rat::fltp scaling = get_scaling();
			arma::Col<fltp> uscale = arma::Col<fltp>(neq,arma::fill::value(scaling));
			arma::Col<fltp> fscale = arma::Col<fltp>(neq,arma::fill::value(scaling));

			// get starting point
			Ae = get_Ae0(cache, fscale);

			// Create serial vectors of length NEQ
			// here we take pointers to armadillo objects
			// initial guess on input and solution vector
			N_Vector nvec_Ae = conv2nvec(Ae,sunctx_);

			// scaling vector, for the variable cc
			N_Vector nvec_uscale = conv2nvec(uscale,sunctx_);

			// scaling vector for function values fval
			N_Vector nvec_fscale = conv2nvec(fscale,sunctx_);

			// start 
			lg_->msg(2,"%sCreate KINSOL%s\n",KBLU,KNRM);
			
			// create KINSol memory block object 
			kmem_ = KINCreate(sunctx_);

			// User data assignment (pointer to this class)
			flag = KINSetUserData(kmem_, this);
			if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set user data");

			// Vector cc passed as template vector
			flag = KINInit(kmem_, NonLinSolver::nt_systemfn, nvec_Ae);
			if(flag==KIN_MEM_NULL)rat_throw_line("the KINSOL memory block was not initialized through a previous call to KINCreate");
			if(flag==KIN_MEM_FAIL)rat_throw_line("a memory allocation request has failed");
			if(flag==KIN_ILL_INPUT)rat_throw_line("an input argument to KINInit() has an illegal value");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not initialize KINSOL");

			// // set print level
			// flag = KINSetPrintLevel(kmem_, printfl_);
			// if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			// if(flag==KIN_ILL_INPUT)rat_throw_line("the argument printfl had an illegal value");
			// if(flag!=KIN_SUCCESS)rat_throw_line("could not set print level");

			// preconditioner setups
			flag = KINSetMaxSetupCalls(kmem_, msbset_);
			if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KIN_ILL_INPUT)rat_throw_line("the argument msbset was negative");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set maximum number of setup calls");
			lg_->msg("Max Setup Calls: %s%i%s\n",KYEL,msbset_,KNRM);	

			// set maximum number of newton iterations
			flag = KINSetNumMaxIters(kmem_, max_iter_);
			if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KIN_ILL_INPUT)rat_throw_line("the maximum number of iterations was non-positive");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set max number of iterations");
			lg_->msg("Max Iter: %s%i%s\n",KYEL,max_iter_,KNRM);

			// get tolerances relevant for computer architecture
			#if (defined(RAT_CUDA_DOUBLE_PRECISION) || !defined(ENABLE_CUDA_KERNELS)) && defined(RAT_DOUBLE_PRECISION)
			// use double-precision tolerances when CUDA double-precision is enabled 
			// or CUDA is disabled, and double-precision is explicitly required.
			const fltp fnormtol = fnormtol_double_;
			const fltp scsteptol = scsteptol_double_;
			#else 
			// use single-precision tolerances if GPU is enabled and applicable, 
			// otherwise fall back to double-precision tolerances.
			const fltp fnormtol = use_gpu_ ? fnormtol_single_ : fnormtol_double_;
			const fltp scsteptol = use_gpu_ ? scsteptol_single_ : scsteptol_double_;
			#endif

			// set function norm tolerance
			// const fltp fnormtol = 1e-7; // fnormtol_!=0 ? fnormtol_ : (use_gpu_ ? std::pow(arma::Datum<cufltp>::eps,1.0/3) : std::pow(arma::Datum<fltp>::eps,1.0/3));
			flag = KINSetFuncNormTol(kmem_, fnormtol);
			if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KIN_ILL_INPUT)rat_throw_line("the tolerance was negative");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set norm tolerance");
			lg_->msg("Norm Tolerance: %s%.2e%s\n",KYEL,fnormtol,KNRM);

			// set scaled step tolerance
			// const fltp scsteptol = 1e-9; // (scsteptol_!=0 ? scsteptol_ : (use_gpu_ ? std::pow(arma::Datum<cufltp>::eps,2.0/3) : std::pow(arma::Datum<fltp>::eps,2.0/3)));
			flag = KINSetScaledStepTol(kmem_, scsteptol);
			if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KIN_ILL_INPUT)rat_throw_line("The tolerance was non-positive");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set scaled step tolerance");
			lg_->msg("Sc. Step Tolerance: %s%.2e%s\n",KYEL,scsteptol,KNRM);

			// set linear stopping criterion
			// eisenstat and walker choice 2 seems to be much 
			// more efficient than 1 (with this choice one can 
			// also set alpha and gamma)
			flag = KINSetEtaForm(kmem_, eta_choice_); 
			if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KIN_ILL_INPUT)rat_throw_line("the argument etachoice had an illegal value");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set eta choice");

			// display eta form
			lg_->msg("ETA Form (eisenstat and walker): ");
			if(eta_choice_==KIN_ETACHOICE1)lg_->msg(0,"%sETACHOICE1%s\n",KYEL,KNRM);
			else if(eta_choice_==KIN_ETACHOICE2)lg_->msg(0,"%sETACHOICE2%s\n",KYEL,KNRM);
			else if(eta_choice_==KIN_ETACONSTANT)lg_->msg(0,"%sETACONSTANT%s\n",KYEL,KNRM);
				
			// set eta form constant value
			flag = KINSetEtaConstValue(kmem_, eta_const_);
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set ETA const value");
			
			// display gloabl strategy
			lg_->msg("global strategy: ");
			if(globalstrategy_==KIN_NONE)lg_->msg(0,"%sKIN_NONE%s\n",KYEL,KNRM);
			else if(globalstrategy_==KIN_LINESEARCH)lg_->msg(0,"%sKIN_LINESEARCH%s\n",KYEL,KNRM);
			else if(globalstrategy_==KIN_PICARD)lg_->msg("%sKIN_PICARD%s\n",KRED,KNRM);
			else if(globalstrategy_==KIN_FP)lg_->msg("%sKIN_FP%s\n",KRED,KNRM);

			// // output function
			// flag = KINSetInfoHandlerFn(kmem_, NonLinSolver::nt_infofn, this);
			// if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			// if(flag!=KIN_SUCCESS)rat_throw_line("could not set info function");

			// // set error handle function
			// flag = KINSetErrHandlerFn(kmem_, NonLinSolver::nt_errfn, this);
			// if(flag==KIN_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			// if(flag!=KIN_SUCCESS)rat_throw_line("could not set error function");

			// end linear solver
			lg_->msg(-2,"\n");

			// setup preconditioner
			lg_->msg(2,"%sSetup Preconditioner%s\n",KBLU,KNRM);
			create_preconditioner(); assert(preconditioner_);
			preconditioner_->set_logger(lg_);
			preconditioner_->display();
			lg_->msg(-2,"\n");

			// logger
			lg_->msg(2,"%sCreate Linear Solver%s\n",KBLU,KNRM);

			// create a linear solver object
			create_linear_solver(nvec_Ae);

			// display preconditioner type
			lg_->msg("using ");
			if(preconditioner_side_==SUN_PREC_NONE)lg_->msg(0,"%sNONE%s",KYEL,KNRM);
			else if(preconditioner_side_==SUN_PREC_RIGHT)lg_->msg(0,"%sRIGHT%s",KYEL,KNRM);
			else if(preconditioner_side_==SUN_PREC_LEFT)lg_->msg(0,"%sLEFT%s",KYEL,KNRM);
			else if(preconditioner_side_==SUN_PREC_BOTH)lg_->msg(0,"%sLEFT/RIGHT%s",KYEL,KNRM);
			else rat_throw_line("preconditioner side not recognized");
			lg_->msg(0," preconditioner\n");

			// Attach the linear solver to KINSOL
			flag = KINSetLinearSolver(kmem_, LS_, NULL);
			if(flag==KINLS_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KINLS_ILL_INPUT)rat_throw_line("the KINLS interface is not compatible with the LS or J input objects or is incompatible with the current NVECTOR module");
			if(flag==KINLS_SUNLS_FAIL)rat_throw_line("a call to the LS object failed");
			if(flag==KINLS_MEM_FAIL)rat_throw_line("a memory allocation request failed");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set linear solver");
			
			// Jacobian times vector function
			flag = KINSetJacTimesVecFn(kmem_, NonLinSolver::nt_jtvfn);
			if(flag==KINLS_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KINLS_LMEM_NULL)rat_throw_line("the KINLS linear solver has not been initialized");
			if(flag==KINLS_SUNLS_FAIL)rat_throw_line("an error occurred when setting up the system matrix-times-vector routines in the SUNLINSOL object used by the KINLS interface");
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set jacobian times vector function");

			// Set preconditioner functions
			flag = KINSetPreconditioner(kmem_, NonLinSolver::nt_precsetupfn, NonLinSolver::nt_precsolvefn);
			if(flag==KINLS_MEM_NULL)rat_throw_line("the kin_mem pointer is NULL");
			if(flag==KINLS_LMEM_NULL)rat_throw_line("the KINLS linear solver has not been initialized");
			if(flag==KINLS_SUNLS_FAIL)rat_throw_line("an error occurred when setting up preconditioning in the SUNLinearSolver object used by the KINLS interface");	
			if(flag!=KIN_SUCCESS)rat_throw_line("could not set preconditioner setup and solve functions");

			// end linear solver
			lg_->msg(-2,"\n");

			// report
			lg_->msg(2,"%sNon-Linear Iterations%s\n",KBLU,KNRM);
			lg_->msg("Solving for %s%llu%s non-linear equations\n",KYEL,neq,KNRM);
			lg_->msg("Logging %sdisabled%s on MLFMM to avoid spam\n",KRED,KNRM);
			
			// increment indent
			lg_->msg(2);

			// set display iteration counter
			iter_displayed_ = -1;

			// create timer
			arma::wall_clock timer; timer.tic();

			// if right hand side is zeros then the solution is zero
			if(arma::all(rhs_==0)){flag = KIN_SUCCESS; Ae.zeros(); update_reluctivity(Ae.t());}

			// call KINSol to solve the system
			else flag = KINSol(kmem_, nvec_Ae, globalstrategy_, nvec_uscale, nvec_fscale);

			// get solve time
			time_.solve_ += timer.toc();

			// check flag
			lg_->newl(); flag2msg(flag);

			// done
			lg_->msg(-2,"\n");
			lg_->msg("Logging %sRe-Enabled%s\n",KRED,KNRM);

			// storing solution in cache
			if(cache!=NULL){
				if(flag==KIN_SUCCESS || flag==KIN_INITIAL_GUESS_OK || flag==KIN_STEP_LT_STPTOL){
					cache->add_solution(Ae);
					lg_->msg("solution cached (current size %s%6.2f MB%s)\n",
						KYEL,fltp(cache->get_storage_size())/1024/1024,KNRM);
				}
			}else{
				lg_->msg("no cache storage provided\n");
			}

			// done with iterations
			lg_->msg(-2,"\n");

			// show timing table
			display_timings();

			// show mlfmm timings
			// mlfmm_->display(lg_);

			// free linear solver object
			if(linear_solver_type_==LinsolType::FGMRES)SUNLinSolFree_SPFGMR(LS_);
			else if(linear_solver_type_==LinsolType::GMRES)SUNLinSolFree_SPGMR(LS_);
			else if(linear_solver_type_==LinsolType::BCGS)SUNLinSolFree_SPBCGS(LS_);
			else if(linear_solver_type_==LinsolType::TFQMR)SUNLinSolFree_SPTFQMR(LS_);

			// free up memory
			destroy_nvector(nvec_Ae);
			destroy_nvector(nvec_uscale);
			destroy_nvector(nvec_fscale);

			// free kinsol memory
			KINFree(&kmem_);

			// free context
			SUNContext_PopErrHandler(sunctx_);
			SUNContext_Free(&sunctx_);

			// done with solver
			lg_->msg(-2,"\n");
		}

		// clear large sparse matrices
		// Ms2t_.clear(); Mf2n_.clear(); Me2f_.clear();

		// return flag
		return Ae;
	}


	// display status
	void NonLinSolver::display_status()const{
		// check if this was called with kinsol enabled
		if(kmem_==NULL)return;

		// get statistics
		int flag = 0;
		long int num_fn_evals; flag = KINGetNumFuncEvals(kmem_,&num_fn_evals);
		if(flag==KIN_MEM_NULL)rat_throw_line("kinsol memory points to NULL");
		if(flag!=KIN_SUCCESS)rat_throw_line("could not retreive number of function evaluations");

		long int num_nl_iter; flag = KINGetNumNonlinSolvIters(kmem_,&num_nl_iter);
		if(flag==KIN_MEM_NULL)rat_throw_line("kinsol memory points to NULL");
		if(flag!=KIN_SUCCESS)rat_throw_line("could not retreive number of nonlinear iterations");

		long int num_jac; flag = KINGetNumJtimesEvals(kmem_,&num_jac);
		if(flag==KIN_MEM_NULL)rat_throw_line("kinsol memory points to NULL");
		if(flag!=KIN_SUCCESS)rat_throw_line("could not retreive number of jacobian evaluations");

		fltp stepsize; flag = KINGetStepLength(kmem_,&stepsize);
		if(flag==KIN_MEM_NULL)rat_throw_line("kinsol memory points to NULL");
		if(flag!=KIN_SUCCESS)rat_throw_line("could not retreive stepsize");

		fltp fnorm; flag = KINGetFuncNorm(kmem_,&fnorm);
		if(flag==KIN_MEM_NULL)rat_throw_line("kinsol memory points to NULL");
		if(flag!=KIN_SUCCESS)rat_throw_line("could not retreive function norm");

		// already shown
		if(iter_displayed_==num_nl_iter)return;

		// on the first call create small header
		if(num_nl_iter%20==0){
			if(num_nl_iter!=0)lg_->newl();
			lg_->msg("%s%5s  %6s  %6s  %8s  %8s%s\n",KCYN,"niter","nilin","njac","fnorm","step",KNRM);
			lg_->msg("%s=========================================%s\n",KCYN,KNRM);
		}

		// create custom message
		if(num_fn_evals>0){
			lg_->msg("%s%05li  %06li  %06li  %08.2e  %08.2e%s\n",KCYN,num_nl_iter,num_fn_evals,num_jac,fnorm,stepsize,KNRM);
			iter_displayed_ = num_nl_iter;
		}
	}

	// system function
	// u - is the current value of the variable vector, u.
	// fval - is the output vector F (u).
	// user data - is a pointer to user data, the pointer user data passed to KINSetUserData.
	int NonLinSolver::nt_systemfn(
		N_Vector nvec_u, 
		N_Vector nvec_fval, 
		void *user_data){

		// std::cout<<"sys"<<std::endl;

		// create a timer
		arma::wall_clock timer; timer.tic();

		// convert input/output to armadillo
		arma::Col<fltp> u = conv2arma(nvec_u);
		arma::Col<fltp> fval = conv2arma(nvec_fval);
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);

		// calculate non-linear reluctivity
		slvrpr->update_reluctivity(u.t());

		// calculate residuals (need to add MLFMM later)
		fval = -slvrpr->rhs_ + 
			slvrpr->calc_stiffness_matrix_mvp(slvrpr->nu_, u.t()) +
			slvrpr->calc_flux_leakage(slvrpr->nu_, u.t()); 

		// status update
		slvrpr->display_status();

		// check 
		if(compare_vectors(nvec_fval, fval)!=0)
			rat_throw_line("N-vector conversion failed");

		// count towards function time
		slvrpr->time_.system_fn_ += timer.toc();

		// check for cancelled
		if(slvrpr->lg_->is_cancelled())return -1;
		
		// return flag
		return 0;
	}

	// jacobian times vector setup function
	// v - is the vector by which the Jacobian must be multiplied to the right.
	// Jv - is the computed output vector.
	// u - is the current value of the dependent variable vector.
	// new u - is a flag, input from kinsol and possibly reset by the user’s jtimes function,
	// indicating whether the iterate vector u has been updated since the last call to
	// jtimes. 
	int NonLinSolver::nt_jtvfn(
		N_Vector nvec_v, 
		N_Vector nvec_Jv, 
		N_Vector nvec_u, 
		int* new_u, 
		void *user_data){

		// std::cout<<"jtv"<<std::endl;

		// create a timer
		arma::wall_clock timer; timer.tic();

		// convert input/output to armadillo
		arma::Col<fltp> v = conv2arma(nvec_v);
		arma::Col<fltp> Jv = conv2arma(nvec_Jv);
		arma::Col<fltp> u = conv2arma(nvec_u);
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);

		// get field
		const arma::Mat<fltp> B = slvrpr->calc_elem_flux_density(u.t());

		// calculate sensitivity of Bmag with respect to each of its components
		const arma::Mat<fltp> dBmagdB = B.each_row()/cmn::Extra::vec_norm(B);

		// get flux change
		const arma::Mat<fltp> dB = slvrpr->calc_elem_flux_density(v.t());

		// calculate flux change
		const arma::Row<fltp> dBmag = cmn::Extra::dot(dBmagdB, dB);

		// change of nu
		arma::Row<fltp> dnu = slvrpr->dnudB_%dBmag;
		dnu.cols(arma::find(cmn::Extra::vec_norm(B)==0)).fill(0.0);

		// calculate residuals
		Jv = slvrpr->calc_stiffness_matrix_mvp(slvrpr->nu_, v.t()) + 
			slvrpr->calc_flux_leakage(slvrpr->nu_, v.t()) +
			slvrpr->calc_stiffness_matrix_mvp(dnu, u.t());
			// slvrpr->calc_flux_leakage(dnu, u.t(), true); // <- this term is likely not worth the computational cost

		// check 
		if(compare_vectors(nvec_Jv, Jv)!=0)
			rat_throw_line("N-vector conversion failed");

		// set new 
		new_u[0] = SUNFALSE;

		// count towards function time
		slvrpr->time_.jtv_fn_ += timer.toc();

		// check for cancelled
		if(slvrpr->lg_->is_cancelled())return -1;

		// return flag
		return 0;
	}

	// Preconditioner setup routine
	// u is the current (unscaled) value of the iterate.
	// uscale is a vector containing diagonal elements of the scaling matrix for u.
	// fval is the vector F (u) evaluated at u.
	// fscale is a vector containing diagonal elements of the scaling matrix for fval.
	// user data is a pointer to user data, the same as the user data parameter passed to
	// the function KINSetUserData.
	int NonLinSolver::nt_precsetupfn(
		N_Vector nvec_u, 
		N_Vector nvec_uscale, 
		N_Vector nvec_fval, 
		N_Vector nvec_fscale, 
		void *user_data){
		
		// std::cout<<"precsetup"<<std::endl;

		// create a timer
		arma::wall_clock timer; timer.tic();

		// convert input/output to armadillo
		arma::Col<fltp> u = conv2arma(nvec_u);
		arma::Col<fltp> uscale = conv2arma(nvec_uscale);
		arma::Col<fltp> fval = conv2arma(nvec_fval);
		arma::Col<fltp> fscale = conv2arma(nvec_fscale);
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);
		// Preconditioner* prec = slvrpr->preconditioner_.get();

		// calculate non-linear reluctivity
		slvrpr->update_reluctivity(u.t());

		// setup the stiffness matrix
		slvrpr->setup_preconditioner_matrix(slvrpr->nu_,slvrpr->dnudB_,u.t());

		// apply factorisation
		slvrpr->setup_preconditioner();

		// count towards function time
		slvrpr->time_.prec_setup_fn_ += timer.toc();

		// check for cancelled
		if(slvrpr->lg_->is_cancelled())return -1;

		// return flag
		return 0;
	}

	// Preconditioner solve function
	// u - is the current (unscaled) value of the iterate.
	// uscale - is a vector containing diagonal elements of the scaling matrix for u.
	// fval - is the vector F (u) evaluated at u.
	// fscale - is a vector containing diagonal elements of the scaling matrix for fval.
	// v on input, v is set to the right-hand side vector of the linear system, r. On
	// output, v must contain the solution z of the linear system P z = r.
	// user data is a pointer to user data, the same as the user data parameter passed to
	// the function KINSetUserData.
	int NonLinSolver::nt_precsolvefn(
		N_Vector nvec_u, 
		N_Vector nvec_uscale, 
		N_Vector nvec_fval, 
		N_Vector nvec_fscale, 
		N_Vector nvec_v, 
		void *user_data){

		// std::cout<<"precsolve"<<std::endl;
		
		// create a timer
		arma::wall_clock timer; timer.tic();

		// convert input/output to armadillo
		arma::Col<fltp> u = conv2arma(nvec_u);
		arma::Col<fltp> v = conv2arma(nvec_v);
		arma::Col<fltp> uscale = conv2arma(nvec_uscale);
		arma::Col<fltp> fscale = conv2arma(nvec_fscale);
		arma::Col<fltp> fval = conv2arma(nvec_fval);
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);
		Preconditioner* prec = slvrpr->preconditioner_.get();

		// delta
		const fltp delta = 0.0;

		// run preconditioner
		const arma::Col<double> x = prec->solve(v, delta);
		
		// assign x to v
		v = x;

		// check 
		if(compare_vectors(nvec_v, v)!=0)
			rat_throw_line("N-vector conversion failed");

		// count towards function time
		slvrpr->time_.prec_solve_fn_ += timer.toc();

		// check for cancelled
		if(slvrpr->lg_->is_cancelled())return -1;

		// return flag
		return 0;
	} 

	// // info function for fixed point
	// void NonLinSolver::nt_infofn(
	// 	const char* module, const char* fn, char* msg, void *user_data){
	// 	// get solver
	// 	NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);

	// 	// output status
	// 	slvrpr->lg_->msg("%s %s %s",module,fn,msg);
	// }

	// // error function
	// void NonLinSolver::nt_errfn(
	// 	int error_code, const char *module, 
	// 	const char *function, char *msg, void *user_data){
	// 	NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);
	// 		slvrpr->lg_->msg("%sKINSol %s%s %i - %s: %s: %s\n",
	// 			error_code>0 ? KYEL : KRED,
	// 			error_code>0 ? "warning" : "error",
	// 			KNRM,error_code,module,function,msg);
	// }

	// error handler
	void NonLinSolver::err_fn(
		int line,
		const char *function,
		const char *file,
		const char *msg,
		SUNErrCode err_code,
		void *err_user_data,
		SUNContext /*sunctx*/){

		// get solver object
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(err_user_data);

		// send error message to logger
		slvrpr->lg_->msg("%sKINSol %s%s %i - %s: %s: %i: %s\n",
			err_code>0 ? KYEL : KRED,
			err_code>0 ? "warning" : "error",
			KNRM,err_code,file,function,line,msg);
	}


	// CONVERSION BETWEEN ARMADILLO AND SUNDIALS NVECTOR
	// ======================
	// compare vectors to be sure output to N_Vector is done correctly
	int NonLinSolver::compare_vectors(
		const N_Vector &nvec, 
		const arma::Col<fltp> &v){
		int flag = 0;
		for(arma::uword i=0;i<v.n_elem;i++){
			#ifdef NVECTOR_PTHREADS
			if(NV_CONTENT_PT(nvec)->data[i] != v(i))flag = 1;
			#else
			if(NV_CONTENT_S(nvec)->data[i] != v(i))flag = 1;
			#endif
		}
		return flag;
	}

	// conversion from armadillo to nvector
	// note that this is just swapping pointers around
	// this is not a full copy of the stored data
	N_Vector NonLinSolver::conv2nvec(arma::Col<fltp> &v, SUNContext &sunctx){
		// create new vector threaded
		#ifdef NVECTOR_PTHREADS
			int num_cpus = std::thread::hardware_concurrency();
			N_Vector nvec_v = N_VNewEmpty_Pthreads(v.n_elem, num_cpus, sunctx);
			NV_CONTENT_PT(nvec_v)->own_data = false;
			NV_CONTENT_PT(nvec_v)->data = v.memptr();
			NV_CONTENT_PT(nvec_v)->length = v.n_elem;

		// create new vector serial
		#else
			N_Vector nvec_v = N_VNewEmpty_Serial(v.n_elem, sunctx);
			NV_CONTENT_S(nvec_v)->own_data = false;
			NV_CONTENT_S(nvec_v)->data = v.memptr();
			NV_CONTENT_S(nvec_v)->length = v.n_elem;
		#endif

		// return new vector
		return nvec_v;
	}

	// convert from nvector to armadillo
	// note that this is just swapping pointers around
	// this is not a full copy of the stored data
	arma::Col<fltp> NonLinSolver::conv2arma(N_Vector &nvec_v){
		#ifdef NVECTOR_PTHREADS
		return arma::Col<fltp>(NV_CONTENT_PT(nvec_v)->data,
			NV_CONTENT_PT(nvec_v)->length, false, true);
		#else
		return arma::Col<fltp>(NV_CONTENT_S(nvec_v)->data,
			NV_CONTENT_S(nvec_v)->length, false, true);
		#endif
	}

	// delete n-vector
	void NonLinSolver::destroy_nvector(N_Vector &nvec_v){
		#ifdef NVECTOR_PTHREADS
		N_VDestroy_Pthreads(nvec_v);
		#else
		N_VDestroy_Serial(nvec_v);
		#endif
	}



	// DELTA SOLVE
	// ==========================
	// the assumption is that Hext changes so little 
	// that the system is linear
	arma::Col<fltp> NonLinSolver::dsolve(const arma::Col<fltp>&Ae){
		// output flag for sundials and kinsol commands
		int flag = 0;

		// check nu and dnu
		if(nu_.empty())rat_throw_line("nu is not set");
		if(dnudB_.empty())rat_throw_line("dnudB is not set");
		if(dHext_.empty())rat_throw_line("dHext is not set");

		// set to self
		Ae_ = Ae.t();

		// allocate change of vector potential along edges
		arma::Col<fltp> dAe(get_num_edges());

		// check if preconditioner is setup
		if(!preconditioner_)rat_throw_line("preconditioner is not setup");

		// check if nl-solver already setup using regular solve
		if(!preconditioner_->get_factorised() && !lg_->is_cancelled()){
			// setup preconditioner this only needs to be done once
			setup_preconditioner_matrix(nu_,dnudB_,Ae.t());

			// setup the preconditioner here
			setup_preconditioner();
		}

		// only run the next section if the cancel flag is not set
		if(!lg_->is_cancelled()){
			// create context
			SUNContext_Create(SUN_COMM_NULL, &sunctx_);
			const SUNErrCode code = SUNContext_PushErrHandler(sunctx_, NonLinSolver::err_fn, this);
			if(code!=SUN_SUCCESS)rat_throw_line("could not set error handler function");

			// change of right hand side
			arma::Col<fltp> drhs = setup_rhs(dHext_);
			N_Vector nvec_drhs = conv2nvec(drhs,sunctx_);

			// wrap inside Nvector
			N_Vector nvec_dAe = conv2nvec(dAe,sunctx_);

			// logger
			lg_->msg(2,"%sCreate Linear Solver%s\n",KBLU,KNRM);

			// create a linear solver object
			const bool is_dsolve = true;
			create_linear_solver(nvec_dAe, is_dsolve);

			// set system function
			flag = SUNLinSolSetATimes(LS_, static_cast<void*>(this), NonLinSolver::datimes_fn);
			if(flag!=KINLS_SUCCESS)rat_throw_line("could not set matrix vector multiplication function");

			// set preconditioner function
			flag = SUNLinSolSetPreconditioner(LS_, static_cast<void*>(this), NULL, NonLinSolver::dprec_solve_fn);
			if(flag!=KINLS_SUCCESS)rat_throw_line("could not set preconditioner");

			// initialize solver
			flag = SUNLinSolInitialize(LS_);
			if(flag!=KINLS_SUCCESS)rat_throw_line("could not initialize solver");

			// end linear solver
			lg_->msg(-2,"\n");

			// reset times
			reset_timings();

			// report
			lg_->msg(2,"%sLinear Iterations%s\n",KBLU,KNRM);
			lg_->msg("Solving for %s%llu%s linear equations\n",KYEL,get_num_edges(),KNRM);
			lg_->msg("Logging %sdisabled%s on MLFMM to avoid spam\n",KRED,KNRM);

			// increment indent
			lg_->msg(2);

			// solve
			flag = SUNLinSolSolve(LS_,NULL,nvec_dAe,nvec_drhs,dlinsoltol_);
			if(flag!=KINLS_SUCCESS)rat_throw_line("could not solve linear system");
			
			// done
			lg_->msg(-2,"\n");
			lg_->msg("Logging %sRe-Enabled%s\n",KRED,KNRM);

			// done
			lg_->msg(-2,"\n");

			// display times
			display_timings();

			// free vectors
			destroy_nvector(nvec_dAe);
			destroy_nvector(nvec_drhs);

			// free linear solver
			SUNLinSolFree_SPFGMR(LS_);

			// free context
			SUNContext_PopErrHandler(sunctx_);
			SUNContext_Free(&sunctx_);
		}

		// return change
		return dAe;
	}

	// int NonLinSolver::dprec_setup_fn(void *user_data){

	// }

	// display status
	void NonLinSolver::ddisplay_status()const{
		// check if this was called with kinsol enabled
		if(LS_==NULL)return;

		// get statistics
		const int num_iter = SUNLinSolNumIters(LS_);
		const sunrealtype res_norm = SUNLinSolResNorm(LS_);

		// on the first call create small header
		if(num_iter%20==0){
			if(num_iter!=0)lg_->newl();
			lg_->msg("%s%5s  %8s%s\n",KCYN,"niter","resnorm",KNRM);
			lg_->msg("%s================%s\n",KCYN,KNRM);
		}

		// create custom message
		if(num_iter>0){
			lg_->msg("%s%05li  %08.2e%s\n",KCYN,num_iter,res_norm,KNRM);
		}
	}

	// solve function for delta solver
	int NonLinSolver::dprec_solve_fn(void *user_data, N_Vector nvec_r, N_Vector nvec_z, sunrealtype tol, int /*lr*/){
		// convert input/output to armadillo
		arma::Col<fltp> r = conv2arma(nvec_r);
		arma::Col<fltp> z = conv2arma(nvec_z);
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);
		Preconditioner* prec = slvrpr->preconditioner_.get();

		// set timer
		arma::wall_clock timer; timer.tic();

		// run preconditioner
		z = prec->solve(r, tol);

		// check 
		if(compare_vectors(nvec_z, z)!=0)
			rat_throw_line("N-vector conversion failed");

		// count time
		slvrpr->time_.prec_solve_fn_+= timer.toc();

		// check for cancelled
		if(slvrpr->lg_->is_cancelled())return -1;

		// return flag
		return 0;
	}

	// matrix vector fun for delta solver
	int NonLinSolver::datimes_fn(void *user_data, N_Vector nvec_v, N_Vector nvec_z){
		// convert input/output to armadillo
		arma::Col<fltp> v = conv2arma(nvec_v);
		arma::Col<fltp> z = conv2arma(nvec_z);
		NonLinSolver* slvrpr = reinterpret_cast<NonLinSolver*>(user_data);

		// set timer
		arma::wall_clock timer; timer.tic();


		// get field
		const arma::Mat<fltp> B = slvrpr->calc_elem_flux_density(slvrpr->Ae_);

		// calculate sensitivity of Bmag with respect to each of its components
		const arma::Mat<fltp> dBmagdB = B.each_row()/cmn::Extra::vec_norm(B);

		// get flux change
		const arma::Mat<fltp> dB = slvrpr->calc_elem_flux_density(v.t());

		// calculate flux change
		const arma::Row<fltp> dBmag = cmn::Extra::dot(dBmagdB, dB);

		// change of nu
		arma::Row<fltp> dnu = slvrpr->dnudB_%dBmag;
		dnu.cols(arma::find(cmn::Extra::vec_norm(B)==0)).fill(0.0);

		// calculate residuals
		z = slvrpr->calc_stiffness_matrix_mvp(slvrpr->nu_, v.t()) + 
			slvrpr->calc_flux_leakage(slvrpr->nu_, v.t()) + 
			slvrpr->calc_stiffness_matrix_mvp(dnu, slvrpr->Ae_) + 
			slvrpr->calc_flux_leakage(dnu, slvrpr->Ae_, true);

		// status update
		slvrpr->ddisplay_status();

		// check 
		if(compare_vectors(nvec_z, z)!=0)
			rat_throw_line("N-vector conversion failed");

		// count time
		slvrpr->time_.system_fn_ += timer.toc();

		// check for cancelled
		if(slvrpr->lg_->is_cancelled())return -1;

		// return flag
		return 0;
	}


	// EXTERNAL FIELD CALCULATION
	// ================================
	// magnetic moment sources
	fmm::ShMomentSourcesPr NonLinSolver::create_magnetic_moments(const arma::Row<fltp> &Ae)const{
		// check prerequisites
		assert(!Mf2n_.is_empty()); assert(!Me2f_.is_empty());

		// magnetization
		const arma::Mat<fltp> Mg = calc_elem_magnetization(Ae);

		// calculate magnetic moment for each gauss node
		const arma::Mat<fltp> mg = Mg.each_row()%gauss_volume_;

		// get coordinates of gauss points
		const arma::Mat<fltp> Rg = get_gauss_coords();

		// create moment sources
		const fmm::ShMomentSourcesPr src = fmm::MomentSources::create(Rg,mg);

		// return moment sources
		return src;
	}

	// calculate magnetic flux density in element nodes
	// interpolates from facets using Hdiv shape functions
	arma::Mat<fltp> NonLinSolver::calc_node_flux_density(const arma::Row<fltp> &Ae)const{
		// calculate element flux density
		const arma::Mat<fltp> Bg = calc_elem_flux_density(Ae);

		// calculate reluctance
		const arma::Row<fltp> nu = calc_reluctivity(cmn::Extra::vec_norm(Bg));

		// calculate H field inside
		// const arma::Mat<fltp> Hg = Bg.each_row()%nu;

		// calculate delta reluctance for each face
		const arma::Row<fltp> dnu = calc_delta_reluctance(nu);

		// calculate magnetic flux density normal to each face
		const arma::Row<fltp> Bnf = calc_facet_field(Ae);

		// calculate magnetic charge on the surface (delta magnetization in face normal direction)
		const arma::Row<fltp> dM = dnu%Bnf; // the sign does not matter for potential

		// face centers
		arma::Mat<fltp> Rf(3,f_.n_cols);
		for(arma::uword i=0;i<f_.n_cols;i++)
			Rf.col(i) = arma::mean(Rn_.cols(f_.col(i)),1);

		// create magnetic charges
		const ShMgnChargesPr src = MgnCharges::create(Rf,dM%face_area_);

		// create targets at surface of mesh
		const ShMgnPotentialsPr tar = MgnPotentials::create(Rn_);
		tar->set_field_type('H',3);

		// create mlfmm
		const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src,tar);

		// setup mlfmm
		mlfmm->setup(); 

		// check for cancel
		if(lg_->is_cancelled())return arma::Mat<fltp>(3,Rn_.n_cols,arma::fill::zeros);

		// run mlfmm
		mlfmm->calculate();

		// check for cancel
		if(lg_->is_cancelled())return arma::Mat<fltp>(3,Rn_.n_cols,arma::fill::zeros);

		// return field at elements
		return tar->get_field('H');
	}

	// // get the magnetic field extrapolated and averaged at the nodes
	// arma::Mat<fltp> NonLinSolver::calc_nodal_magnetic_field(const arma::Row<fltp>&Ae)const{
	// 	// calculate element flux density
	// 	const arma::Mat<fltp> Bg = calc_elem_flux_density(Ae);

	// 	// calculate reluctance
	// 	const arma::Row<fltp> nug = calc_reluctivity(cmn::Extra::vec_norm(Bg));

	// 	// calculate H field inside
	// 	const arma::Mat<fltp> Hg = Bg.each_row()%nug;

	// 	// extrapolate to nodes
	// 	const arma::Mat<fltp> Hn = n_.n_rows==8 ? 
	// 		mn::Hexahedron::gauss2nodes(Rn_,n_,Hg,Rqh_,wqh_,Rn_.n_cols) : 
	// 		cmn::Tetrahedron::gauss2nodes(Rn_,n_,Hg,Rqh_,wqh_,Rn_.n_cols);
	// 	assert(Hn.n_cols==Rn_.n_cols);

	// 	// return the field at the nodes
	// 	return Hn;
	// }

	// calculate magnetization at element gauss points
	arma::Mat<fltp> NonLinSolver::calc_elem_magnetization(const arma::Row<fltp> &Ae)const{
		// calculate element flux density
		const arma::Mat<fltp> Bg = calc_elem_flux_density(Ae);

		// calculate reluctance
		const arma::Row<fltp> nug = calc_reluctivity(cmn::Extra::vec_norm(Bg));

		// calculate magnetic field 
		const arma::Mat<fltp> Hg = Bg.each_row()%nug;

		// calculate magnetization
		const arma::Mat<fltp> Mg = Bg/arma::Datum<fltp>::mu_0 - Hg;

		// magnetization at elements
		return Mg;
	}

	// get the magnetic field extrapolated and averaged at the nodes
	arma::Mat<fltp> NonLinSolver::calc_nodal_magnetization(const arma::Row<fltp>&Ae)const{
		// calculate element flux density
		const arma::Mat<fltp> Mg = calc_elem_magnetization(Ae);

		// extrapolate to nodes
		const arma::Mat<fltp> Mn = n_.n_rows==8 ? 
			cmn::Hexahedron::gauss2nodes(Rn_,n_,Mg,Rqh_,wqh_,Rn_.n_cols) : 
			cmn::Tetrahedron::gauss2nodes(Rn_,n_,Mg,Rqh_,wqh_,Rn_.n_cols);
		assert(Mn.n_cols==Rn_.n_cols);
		assert(Mn.is_finite());

		// return the field at the nodes
		return Mn;
	}


	// // gradient of the magnetization at the gauss nodes
	// arma::Mat<fltp> NonLinSolver::calc_elem_magnetization_grad(const arma::Row<fltp>&Ae, const arma::sword num_gauss_volume)const{
	// 	// derivative of shape function
	// 	const arma::Mat<fltp> dN = n_.n_rows==8 ? cmn::Hexahedron::shape_function_derivative(Rqh_) : cmn::Tetrahedron::shape_function_derivative(Rqh_);
	// 	const arma::Mat<fltp> Vq = n_.n_rows==8 ? cmn::Hexahedron::nedelec_hdiv_shape_function(Rqh_) : cmn::Tetrahedron::nedelec_hdiv_shape_function(Rqh_);

	// 	// calculate number of nonzeros on the matrix
	// 	const arma::uword nnz = n_.n_cols*3*Rqh_.n_cols*n2f_.n_rows;

	// 	// allocate sparse matrix
	// 	arma::Mat<arma::uword> locations(2,nnz);
	// 	arma::Col<fltp> val(nnz);

	// 	// walk over elements
	// 	for(arma::uword i=0;i<n_.n_cols;i++){
	// 		// calculate shape function
	// 		const arma::Mat<fltp> J = n_.n_rows==8 ? cmn::Hexahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN) : cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
	// 		const arma::Row<fltp> Jdet = n_.n_rows==8 ? cmn::Hexahedron::jacobian2determinant(J) : cmn::Tetrahedron::jacobian2determinant(J);
	// 		const arma::Mat<fltp> Jinv = n_.n_rows==8 ? cmn::Hexahedron::invert_jacobian(J,Jdet) : cmn::Tetrahedron::invert_jacobian(J,Jdet);
	// 		const arma::Mat<fltp> Vcgrad = n_.n_rows==8 ? cmn::Hexahedron::quad2cart_covariant_piola(Vq,Jinv) : cmn::Tetrahedron::quad2cart_covariant_piola(Vq,Jinv);

	// 		// walk over gauss points
	// 		for(arma::uword j=0;j<Rqh_.n_cols;j++){
	// 			for(arma::uword k=0;k<n2f_.n_rows;k++){
	// 				for(arma::uword m=0;m<3;m++){
	// 					const arma::uword idx = i*Rqh_.n_cols*n2f_.n_rows*3 + j*n2f_.n_rows*3 + k*3 + m;
	// 					locations(0,idx) = 3*(i*Rqh_.n_cols + j) + m;
	// 					locations(1,idx) = n2f_(k,i);
	// 					val(idx) = clockwise2sign(fcw_(k,i))*Vcgrad(3*k+m,j);
	// 				}
	// 			}
	// 		}
			
	// 	}

	// 	// create matrix
	// 	const arma::SpMat<fltp> Mf2n_grad(true,locations,val,3*n_.n_cols*Rqh_.n_cols,f_.n_cols,true,true);

	// 	// calculate B using matrix
	// 	const arma::Mat<fltp> Bggrad = arma::reshape(Mf2n_grad*calc_flux(Ae).t(),3,n_.n_cols*Rqh_.n_cols);

	// 	// calculate reluctance
	// 	const arma::Row<fltp> nug = calc_reluctivity(cmn::Extra::vec_norm(calc_elem_flux_density(Ae)));

	// 	// calculate magnetic field 
	// 	const arma::Mat<fltp> Hggrad = Bggrad.each_row()%nug;

	// 	// calculate magnetization
	// 	const arma::Mat<fltp> Mggrad = Bggrad/arma::Datum<fltp>::mu_0 - Hggrad;

	// 	// return the gradient of the magnetization
	// 	return Mggrad;
	// }

	// // get vector potential in nodes
	// arma::Mat<fltp> NonLinSolver::calc_nodal_vector_potential(const arma::Row<fltp>&Ae)const{
	// 	// calculate element flux density
	// 	const arma::Mat<fltp> Ag = calc_elem_vector_potential(Ae);

	// 	// extrapolate to nodes
	// 	const arma::Mat<fltp> An = cmn::Tetrahedron::gauss2nodes(Rn_,n_,Ag,Rqh_,wqh_,Rn_.n_cols);
	// 	assert(An.n_cols==Rn_.n_cols);

	// 	// return the field at the nodes
	// 	return An;
	// }

	// // calculate magnetic vector potential in element gauss points
	// // interpolates from facets using Hcurl shape functions
	// arma::Mat<fltp> NonLinSolver::calc_elem_vector_potential(const arma::Row<fltp> &Ae)const{
		
	// 	// derivative of shape function
	// 	const arma::Mat<fltp> dN = cmn::Tetrahedron::shape_function_derivative(Rqh_);
	// 	const arma::Mat<fltp> Vq = cmn::Tetrahedron::nedelec_hcurl_shape_function(Rqh_);

	// 	// get edges
	// 	const cmn::Tetrahedron::EdgeMatrix E = cmn::Tetrahedron::get_edges();

	// 	// create edges for all hexahedra
	// 	arma::Mat<arma::uword> e(2,n_.n_cols*E.n_rows);

	// 	// walk over elements
	// 	for(arma::uword i=0;i<n_.n_cols;i++){
	// 		// get this hexahedron
	// 		const cmn::Tetrahedron::ElementVector h = n_.col(i);

	// 		// walk over edges
	// 		for(arma::uword j=0;j<E.n_rows;j++){
	// 			// get faces from this hexahedron
	// 			e.col(i*E.n_rows + j) = h.rows(arma::vectorise(E.row(j)));
	// 		}
	// 	}

	// 	// create map of edges
	// 	std::map<std::pair<arma::uword,arma::uword>,arma::uword> edge_map;
	// 	for(arma::uword i=0;i<e_.n_cols;i++){
	// 		edge_map[{e_(0,i),e_(1,i)}] = i;
	// 		edge_map[{e_(1,i),e_(0,i)}] = i;
	// 	}

	// 	// link edges of element to edges in map
	// 	arma::Mat<arma::uword> n2e(E.n_rows,n_.n_cols);
	// 	for(arma::uword i=0;i<e.n_cols;i++)
	// 		n2e(i) = edge_map.at({e(0,i),e(1,i)});

	// 	// determine if the edge is clockwise or not
	// 	arma::Mat<fltp> eecw(E.n_rows,n_.n_cols);
	// 	for(arma::uword i=0;i<n2e.n_elem;i++){
	// 		eecw(i)=clockwise2sign(arma::all(e_.col(n2e(i))==e.col(i)));
	// 	}

	// 	// allocate field at gauss points
	// 	arma::Mat<fltp> A(3,n_.n_cols*Rqh_.n_cols);

	// 	// walk over elements
	// 	for(arma::uword i=0;i<n_.n_cols;i++){
	// 		// calculate shape function
	// 		const arma::Mat<fltp> J = cmn::Tetrahedron::shape_function_jacobian(Rn_.cols(n_.col(i)),dN);
	// 		const arma::Mat<fltp> Jdet = cmn::Tetrahedron::jacobian2determinant(J);
	// 		const arma::Mat<fltp> Jinv = cmn::Tetrahedron::invert_jacobian(J,Jdet);
	// 		const arma::Mat<fltp> Vc = cmn::Tetrahedron::quad2cart_covariant_piola(Vq,Jinv);
	// 		// const arma::Mat<fltp> Vc = cmn::Tetrahedron::quad2cart_contravariant_piola(Vq,J,Jdet);
	// 		assert(std::abs(arma::accu(Jdet%wqh_) - elem_volume_(i))/std::abs(elem_volume_(i))<1e-6);

	// 		// vector potential contribution of eadch edge
	// 		const arma::Row<fltp> Aedge = Ae.cols(n2e.col(i))%edge_length_.cols(n2e.col(i))%eecw.col(i).t()/2;

	// 		// magnetic flux density at gauss points
	// 		for(arma::uword j=0;j<Rqh_.n_cols;j++)
	// 			A.col(i*Rqh_.n_cols+j) = arma::sum(arma::reshape(Vc.col(j),3,Vc.n_rows/3).eval().each_row()%Aedge,1);
	// 	}

	// 	// return field at elements
	// 	return A;
	// }

	// calculate energy
	// method described in:
	// integrate over volume (ignoring coil energy here)
	// Quentin Debray, Gerard Meunier, Olivier Chadebec, 
	// Jean-Louis Coulomb and Anthony Carpentier, "An expression 
	// of the Magnetic Co-Energy adapted to MagnetoStatic Volume
	// Integral Formulations - Application to the Magnetic Force
	// computation", International Journal of Applied Electromagnetics
	// and Mechanics 50 (2017) 1–7
	arma::Row<fltp> NonLinSolver::calc_coenergy(const arma::Row<fltp>&Ae)const{
		// calculate element flux density
		const arma::Mat<fltp> Bg = calc_elem_flux_density(Ae);
		const arma::Mat<fltp> Mg = calc_elem_magnetization(Ae);
		const arma::Mat<fltp>& H0 = Hext_;

		// calculate reluctance
		const arma::Row<fltp> nug = calc_reluctivity(cmn::Extra::vec_norm(Bg));

		// calculate magnetic field 
		const arma::Mat<fltp> Hg = Bg.each_row()%nug;

		// derivative of shape function
		const arma::Mat<fltp> dN = cmn::Hexahedron::shape_function_derivative(Rqh_);

		// allocate energy
		arma::Row<fltp> co_energy(get_num_elements());

		// walk over HB-Curves
		for(arma::uword i=0;i<hb_curve_.n_elem;i++){
			// integrate hb-curve
			const arma::Col<fltp>&H = hb_curve_(i)->get_magnetic_field_table();
			const arma::Col<fltp>&B = hb_curve_(i)->get_flux_density_table();
			const arma::Row<fltp> hb_cumtrapz = cmn::Extra::cumtrapz(H.t(),B.t(),1);

			// find elements to match this HB-curve
			const arma::Col<arma::uword> idx = arma::find(n2part_==i);

			// walk over elements
			for(arma::uword j=0;j<idx.n_elem;j++){
				// my index
				const arma::uword myidx = idx(j);
				const arma::uword idx1 = myidx*Rqh_.n_cols;
				const arma::uword idx2 = (myidx+1)*Rqh_.n_cols-1;

				// element nodes
				const arma::Mat<fltp> myRn = Rn_.cols(n_.col(myidx));

				// calculate jacobian determinant and weights
				const arma::Mat<fltp> J = cmn::Hexahedron::shape_function_jacobian(myRn,dN);
				const arma::Row<fltp> Jdet = cmn::Hexahedron::jacobian2determinant(J);

				// calculate integral
				arma::Col<fltp> hb_int;
				cmn::Extra::interp1(H,hb_cumtrapz.t(),cmn::Extra::vec_norm(Hg.cols(idx1,idx2)).t(),hb_int,"linear",true);

				// add energy contribution
				const arma::Row<fltp> ce1 = arma::Datum<fltp>::mu_0*cmn::Extra::dot(Mg.cols(idx1,idx2),H0.cols(idx1,idx2))/2;
				const arma::Row<fltp> ce2 = -cmn::Extra::dot(Bg.cols(idx1,idx2),Hg.cols(idx1,idx2))/2;
				const arma::Row<fltp> ce3 = hb_int.t();

				// add parts
				// the non-linear term is actually ce2 + ce3
				co_energy(myidx) = arma::accu((ce1+ce2+ce3)%wqh_%Jdet);
			}
		}

		// return energy
		return co_energy;
	}

}}