// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "hbdata.hh"

// common includes
#include "rat/common/extra.hh"

// boost headers
#include <boost/math/interpolators/barycentric_rational.hpp>

// code specific to Raccoon
namespace rat{namespace nl{

	// empty constructor
	HBData::HBData(){
		
	}

	// constructor
	HBData::HBData(
		const arma::Col<fltp> &Hinterp,
		const arma::Col<fltp> &Binterp, 
		const fltp ff){
		// input
		Hinterp_ = Hinterp; Binterp_ = Binterp; ff_ = ff;

		// check if H or B is monospaced to allow for faster interpolation
		mono_spaced_h_ = arma::all(arma::abs(arma::diff(Hinterp_,1) - (Hinterp_(1) - Hinterp_(0)))<RAT_CONST(1e-9));
		mono_spaced_b_ = arma::all(arma::abs(arma::diff(Binterp_,1) - (Binterp_(1) - Binterp_(0)))<RAT_CONST(1e-9));
	}

	// factory
	ShHBDataPr HBData::create(){
		return std::make_shared<HBData>();
	}

	// factory
	ShHBDataPr HBData::create(
		const arma::Col<fltp> &Hinterp, 
		const arma::Col<fltp> &Binterp, 
		const fltp ff){
		return std::make_shared<HBData>(Hinterp,Binterp,ff);
	}

	// access data
	const arma::Col<fltp>& HBData::get_magnetic_field_table()const{
		return Hinterp_;
	}

	const arma::Col<fltp>& HBData::get_flux_density_table()const{
		return Binterp_;
	}

	// set the filling fraction
	void HBData::set_filling_fraction(const fltp ff){
		ff_ = ff;
	}

	// set BH curve for the team 13 problem
	// https://www.compumag.org/wp/wp-content/uploads/2018/06/problem13.pdf
	void HBData::set_team13(){
		Hinterp_ = {
			0,16.0,30.0,54.0,93.0,143.0,191.0,
			210.0,222.0,233.0,247.0,258.0,272.0,
			289.0,313.0,342.0,377.0,433.0,509.0,
			648.0,933.0,1228.0,1934.0,2913.0,
			4993.0,7189.0,9423.0,9500,10000,15000,
			20000,25000,30000,35000,40000,45000,
			55704.09982,87535.01401,111408.1996,
			190985.4851,270562.7706,270562.7706+0.1/arma::Datum<fltp>::mu_0
		};

		Binterp_ = {
			0.0,0.0025,0.0050,0.0125,0.025,0.05,0.10,
			0.20,0.30,0.40,0.50,0.60,0.70,0.80,0.90,
			1.00,1.1,1.2,1.3,1.4,1.5,1.55,1.6,1.65,1.7,1.75,1.80,
			1.81772453,1.8282464,1.9257046,2.0090528,2.078291,2.1334192,
			2.1744374,2.2013456,2.2141438,2.23,2.27,2.3,2.4,2.5,2.6
		};

		// check lengths
		assert(Hinterp_.n_elem==Binterp_.n_elem);

		// filling fraction
		ff_ = RAT_CONST(1.0);
	}

	// ARMCO Pure Iron Sheet Hot Rolled
	// data reproduced with written permission from aksteel 
	// https://www.aksteel.eu/
	void HBData::set_armco_hot_rolled(){
		// magnetic field table
		Hinterp_ = {0.0,4.3041e+01,
			8.3849e+01,1.1484e+02,1.1484e+02,1.5147e+02,
			1.8293e+02,2.1275e+02,2.5056e+02,2.9508e+02,
			3.5638e+02,4.3586e+02,5.8214e+02,8.3849e+02,
			1.5339e+03,3.3465e+03,5.2639e+03,8.1765e+03,
			1.0000e+04,1.0000e+04+1.0/arma::datum::mu_0};

		// flux density table
		Binterp_ = {0.0,5.5249e-03,
			2.2099e-02,4.4199e-02,4.4199e-02,9.3923e-02,
			1.9337e-01,3.3702e-01,5.2486e-01,7.0718e-01,
			8.8950e-01,1.0663e+00,1.2762e+00,1.4530e+00,
			1.5856e+00,1.6906e+00,1.7514e+00,1.8232e+00,
			1.8564e+00,1.8564e+00+1.0};

		// filling fraction
		ff_ = RAT_CONST(1.0);
	}

	// ARMCO Pure Iron Sheet Col Rolled
	// data reproduced with written permission from aksteel 
	// https://www.aksteel.eu/
	void HBData::set_armco_cold_rolled(){
		// magnetic field
		Hinterp_ = {0.0,2.0934e+01,3.6885e+01,
			7.1716e+01,9.6373e+01,1.1592e+02,1.4117e+02,
			1.7838e+02,2.1722e+02,2.7112e+02,3.1818e+02,
			3.9227e+02,4.7184e+02,6.5793e+02,8.8415e+02,
			1.1736e+03,1.5966e+03,2.4268e+03,3.5987e+03,
			5.0180e+03,6.8269e+03,9.8776e+03,9.8776e+03+1.0/arma::datum::mu_0};

		// flux density
		Binterp_ = {0.0,1.0899e-02,1.6349e-02,
			4.9046e-02,9.8093e-02,1.6894e-01,2.9428e-01,
			4.9046e-01,6.7030e-01,8.5014e-01,9.7003e-01,
			1.1117e+00,1.2153e+00,1.3624e+00,1.4768e+00,
			1.5586e+00,1.6240e+00,1.6785e+00,1.7384e+00,
			1.7766e+00,1.8256e+00,1.8910e+00,1.8910e+00+1.0};

		// filling fraction
		ff_ = RAT_CONST(1.0);
	}


	// adjust B for filling fraction
	arma::Col<fltp> HBData::calc_Bff()const{
		// calc M
		const arma::Mat<fltp> M = Binterp_/arma::Datum<fltp>::mu_0 - Hinterp_;

		// scale M and convert back to B
		const arma::Mat<fltp> B = arma::Datum<fltp>::mu_0*(ff_*M + Hinterp_);

		// return scaled B
		return B;
	}

	// calculate magnetic flux density from HB curve
	arma::Row<fltp> HBData::calc_magnetic_flux_density(
		const arma::Row<fltp> &Hmag) const{

		// ensure that Hmag has no nans
		assert(!Hmag.has_nan()); assert(arma::all(Hmag>=0));
		assert(!Hinterp_.is_empty()); assert(!Binterp_.is_empty());

		// allocate output
		arma::Col<fltp> Bmag;
		
		// scale B for filling fraction
		const arma::Col<fltp> Bff = calc_Bff();

		// interpolate HB curve to acquire field magnitude
		if(mono_spaced_h_)cmn::Extra::lininterp1f(Hinterp_,Bff,Hmag.t(),Bmag,true);
		else arma::interp1(Hinterp_,Bff,Hmag.t(),Bmag,"linear",RAT_CONST(0.0));
		
		// extrapolation: beyond the last interpolation point
		// it is assumed that the material no longer magnetises
		const fltp Hend = Hinterp_.back();
		const fltp Bend = Bff.back();
		const arma::Col<arma::uword> idx = arma::find(Hmag>Hend);
		Bmag.rows(idx) = Bend + arma::datum::mu_0*(Hmag.cols(idx).t()-Hend);
		
		// return magnetic field
		return Bmag.t();
	}

	// calculate magnetic flux density from HB curve
	arma::Row<fltp> HBData::calc_magnetic_field(
		const arma::Row<fltp> &Bmag) const{

		// ensure that Hmag has no nans
		assert(!Bmag.has_nan()); assert(arma::all(Bmag>=0));
		assert(!Hinterp_.is_empty()); assert(!Binterp_.is_empty());

		// allocate output
		arma::Col<fltp> Hmag;

		// scale B for filling fraction
		const arma::Col<fltp> Bff = calc_Bff();

		// interpolate HB curve to acquire field magnitude
		if(mono_spaced_b_)cmn::Extra::lininterp1f(Bff,Hinterp_,Bmag.t(),Hmag,true);
		else arma::interp1(Bff,Hinterp_,Bmag.t(),Hmag,"linear",RAT_CONST(0.0));

		// extrapolation: beyond the last interpolation point
		// it is assumed that the material no longer magnetises
		const fltp Hend = Hinterp_.back(); 
		const fltp Bend = Bff.back();
		const arma::Col<arma::uword> idx = arma::find(Bmag>Bend);
		Hmag.rows(idx) = Hend + (Bmag.cols(idx).t()-Bend)/arma::datum::mu_0;
		
		// return magnetic field
		return Hmag.t();
	}


	// calculate magnetistion from HB curve
	arma::Row<fltp> HBData::calc_magnetisation(
		const arma::Row<fltp> &Hmag) const{
		// get magnetic flux density
		const arma::Row<fltp> Bmag = calc_magnetic_flux_density(Hmag);
			
		// calculate magnetisation
		const arma::Row<fltp> M = Bmag/arma::datum::mu_0 - Hmag;

		// return
		return M; 
	}

	// calculate magnetic reluctivity from HB curve
	arma::Row<fltp> HBData::calc_reluctivity(
		const arma::Row<fltp> &Bmag) const{

		// limit Bmag
		const arma::Row<fltp> Bmagc = arma::clamp(Bmag,RAT_CONST(0.001),RAT_CONST(1e12));

		// calc magnetic field
		const arma::Row<fltp> Hmag = calc_magnetic_field(Bmagc);

		// calculate susceptibility
		arma::Row<fltp> nu = Hmag/Bmagc;

		// return susceptibility
		return nu;
	}

	// calculate magnetic susceptibility from HB curve
	arma::Row<fltp> HBData::calc_reluctivity_diff(
		const arma::Row<fltp> &Bmag) const{

		// get delta
		const arma::Row<fltp> B1 = arma::clamp(Bmag-dB_/2,RAT_CONST(1e-14),arma::Datum<fltp>::inf);
		const arma::Row<fltp> B2 = Bmag+dB_/2;

		// calculate susceptibility
		const arma::Row<fltp> nu1 = calc_reluctivity(B1);
		const arma::Row<fltp> nu2 = calc_reluctivity(B2);

		// difference
		const arma::Row<fltp> dnudB = (nu2-nu1)/(B2-B1);

		// return derivative of susceptibility
		return dnudB;
	}

	// calculate magnetic susceptibility from HB curve
	arma::Row<fltp> HBData::calc_susceptibility(
		const arma::Row<fltp> &Hmag) const{

		// get magnetic flux density
		const arma::Row<fltp> Bmag = calc_magnetic_flux_density(Hmag);

		// calculate susceptibility
		const arma::Row<fltp> Chi = Bmag/(arma::datum::mu_0*Hmag)-RAT_CONST(1.0);

		// return susceptibility
		return Chi;
	}

	// calculate magnetic susceptibility from HB curve
	arma::Row<fltp> HBData::calc_susceptibility_diff(
		const arma::Row<fltp> &Hmag) const{
		
		// get magnetic flux density
		const arma::Row<fltp> Bmag1 = calc_magnetic_flux_density(Hmag);
		const arma::Row<fltp> Bmag2 = calc_magnetic_flux_density(Hmag+dH_);
		
		// calculate susceptibility
		const arma::Row<fltp> Chi1 = Bmag1/(arma::datum::mu_0*Hmag)-RAT_CONST(1.0);
		const arma::Row<fltp> Chi2 = Bmag2/(arma::datum::mu_0*(Hmag+dH_))-RAT_CONST(1.0);

		// difference
		const arma::Row<fltp> dChidH = (Chi2-Chi1)/dH_;

		// return derivative of susceptibility
		return dChidH;
	}

	// calculate permeability
	arma::Row<fltp> HBData::calc_permeability(const arma::Row<fltp> &Bmag) const{

		// get magnetic flux density
		const arma::Row<fltp> Hmag1 = calc_magnetic_field(Bmag);
		const arma::Row<fltp> Hmag2 = calc_magnetic_field(Bmag+dB_);

		// calculate permeability
		const arma::Row<fltp> mu = dB_/(Hmag2 - Hmag1);

		// return it
		return mu;
	}

	// calculate permeability
	arma::Row<fltp> HBData::calc_relative_permeability(const arma::Row<fltp> &Bmag)const{
		return calc_permeability(Bmag)/arma::Datum<fltp>::mu_0;
	}
}}