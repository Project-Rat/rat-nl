// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "precqdldl.hh"

// common headers
#include "rat/common/extra.hh"
#include "amd.h"

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecQDLDL::PrecQDLDL(){

	}

	// factory
	ShPrecQDLDLPr PrecQDLDL::create(){
		return std::make_shared<PrecQDLDL>();
	}


	// set matrix
	void PrecQDLDL::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		// check matrix
		assert(idx_col.is_sorted());
		assert(idx_col.n_elem==val.n_elem);
		assert(idx_row.n_elem==val.n_elem);
		assert(idx_col.max()<num_size);
		assert(idx_row.max()<num_size);

		// set counters
		num_size_ = num_size;

		// convert
		const arma::Col<int64_t> idx_col_li = arma::conv_to<arma::Col<int64_t> >::from(idx_col);
		const arma::Col<int64_t> idx_row_li = arma::conv_to<arma::Col<int64_t> >::from(idx_row);


		// order the matrix (control and info are set to NULL here)
		P_.set_size(num_size_);
		int result = amd_l_order(num_size_, 
			idx_col_li.memptr(), 
			idx_row_li.memptr(), 
			P_.memptr(), NULL, NULL) ;
		if(result!=AMD_OK)rat_throw_line("could not reorder matrix");

		// inversion matrix
		Pinv_.set_size(num_size_);
		for(arma::uword i=0;i<num_size_;i++)Pinv_(P_(i)) = i;

		// unpack columns and reorder both columns and rows
		arma::Col<arma::uword> idx_col_p = Pinv_(idx_col);
		arma::Col<arma::uword> idx_row_p = Pinv_(idx_row);
		arma::Col<fltp> val_p = val;
		
		// sort matrix entries by row
		{
			arma::Col<arma::uword> id1 = arma::sort_index(idx_row_p);
			idx_row_p = idx_row_p(id1); idx_col_p = idx_col_p(id1); val_p = val_p(id1);
		}

		// sort matrix entries by col without disturbing rows
		{
			arma::Col<arma::uword> id2 = arma::stable_sort_index(idx_col_p);
			idx_row_p = idx_row_p(id2); idx_col_p = idx_col_p(id2); val_p = val_p(id2);
		}

		
		// take upper triangle only
		const arma::Col<arma::uword> idx_upper_triangle = arma::find(idx_col_p>=idx_row_p);

		// compress column indices
		// count number of entries in each column
		arma::Col<arma::uword> ncol(num_size, arma::fill::zeros);
		for(arma::uword i=0;i<idx_upper_triangle.n_elem;i++)ncol(idx_col_p(idx_upper_triangle(i)))++;

		// create compression array
		const arma::Col<arma::uword> idx_col_c = arma::join_vert(
			arma::Col<arma::uword>{0},arma::cumsum(ncol));

		// set matrix
		idx_col_c_ = arma::conv_to<arma::Col<QDLDL_int> >::from(idx_col_c);
		idx_row_ = arma::conv_to<arma::Col<QDLDL_int> >::from(idx_row_p.rows(idx_upper_triangle));
		val_ = arma::conv_to<arma::Col<QDLDL_float> >::from(val_p.rows(idx_upper_triangle)); 

		// find number of entries
		num_nnz_ = val.n_elem;

		// check duplicates
		assert(!arma::any(idx_row.head_rows(num_nnz_-1)==idx_row.tail_rows(num_nnz_-1) && 
			idx_col.head_rows(num_nnz_-1)==idx_col.tail_rows(num_nnz_-1)));

		// find number of entries
		num_nnz_ = val.n_elem;
	}

	// analysis
	void PrecQDLDL::analyse(){

		// set size of L matrix
		An_ = num_size_;
		Ln_ = An_;

		// For the elimination tree
		etree_.set_size(An_);
		Lnz_.set_size(An_);

		// For the L factors.   Li and Lx are sparsity dependent
		// so must be done after the etree is constructed
		Lp_.set_size(An_+1);
		D_.set_size(An_);
		Dinv_.set_size(An_);

		// Working memory.  Note that both the etree and factor
		// calls requires a working vector of QDLDL_int, with
		// the factor function requiring 3*An elements and the
		// etree only An elements.   Just allocate the larger
		// amount here and use it in both places
		iwork_.set_size(3*An_);
		bwork_.set_size(An_); 
		fwork_.set_size(An_);

		// run factorisation
		sumLnz_ = QDLDL_etree(An_,idx_col_c_.memptr(),idx_row_.memptr(),iwork_.memptr(),Lnz_.memptr(),etree_.memptr());
		if(sumLnz_<0)rat_throw_line("analysis failed");

		// report
		lg_->msg("%s  <<< qdldl analyse >>>%s\n",KMAG,KNRM);

		// analysis done
		analysed_ = true;
	}

	// factorisation
	void PrecQDLDL::factorise(){
		//First allocate memory for Li and Lx
		Li_.set_size(sumLnz_);
		Lx_.set_size(sumLnz_);

		//now factor
		QDLDL_factor(num_size_,idx_col_c_.memptr(),idx_row_.memptr(),val_.memptr(),Lp_.memptr(),Li_.memptr(),Lx_.memptr(),D_.memptr(),Dinv_.memptr(),Lnz_.memptr(),etree_.memptr(),bwork_.memptr(),iwork_.memptr(),fwork_.memptr());

		// report
		lg_->msg("%s  <<< qdldl factorise >>>%s\n",KMAG,KNRM);
		// lg_->msg("%s  <<< qdldl solving (c.n. %.2e) >>>%s\n",KMAG,cholmod_l_rcond(L_, &c_),KNRM);

		// factorization done
		factorised_ = true;
	}

	// solving
	arma::Col<double> PrecQDLDL::solve(
		const arma::Col<fltp> &b, 
		const rat::fltp /*delta*/){

		// permute b
		arma::Col<fltp> b_p(b.n_elem);
		b_p.rows(Pinv_) = b;

		//when solving A\b, start with x = b
		arma::Col<QDLDL_float> x = arma::conv_to<arma::Col<QDLDL_float> >::from(b_p);

		// run solve
		QDLDL_solve(Ln_,Lp_.memptr(),Li_.memptr(),Lx_.memptr(),Dinv_.memptr(),x.memptr());

		// return result
		return arma::conv_to<arma::Col<fltp> >::from(x.rows(Pinv_));
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecQDLDL::mvp(
		const arma::Col<rat::fltp> &x) const{

		// permute x
		arma::Col<fltp> x_p(x.n_elem);
		x_p.rows(Pinv_) = x;

		// make sure matrix is still compressed
		assert(!idx_col_c_.is_empty());
		assert(x.n_elem==num_size_);

		// allocate output
		arma::Col<rat::fltp> b(num_size_,arma::fill::zeros);

		// walk over columns
		for(arma::uword j=0;j<num_size_;j++){
			// walk over row entries
			for(arma::uword idx=arma::uword(idx_col_c_(j));idx<arma::uword(idx_col_c_(j+1));idx++){
				// get row index
				const arma::uword i = idx_row_(idx);
				
				// add value
				b(i) += val_(idx)*x_p(j);
				
				// off-diagonal
				if(i!=j){
					b(j) += val_(idx)*x_p(i);
				}
			}
		}

		// output
		return b.rows(Pinv_);
	}

	// display preconditioner info
	void PrecQDLDL::display()const{
		lg_->msg("preconditioner: %sQDLDL%s (SuiteSparse)\n",KYEL,KNRM);
		lg_->msg("factorisation type: %sCholesky%s\n",KYEL,KNRM);
	}


}}


