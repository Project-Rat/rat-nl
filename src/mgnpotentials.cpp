// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "mgnpotentials.hh"

// code specific to Rat
namespace rat{namespace nl{

	// constructor
	MgnPotentials::MgnPotentials(){
		// set field type
		add_field_type('S',1);
	}

	// constructor with input
	MgnPotentials::MgnPotentials(
		const arma::Mat<fltp> &Rt){

		// also set target coords
		set_target_coords(Rt);

		// set field type
		add_field_type('S',1);
	}

	// factory
	ShMgnPotentialsPr MgnPotentials::create(){
		//return ShIListPr(new IList);
		return std::make_shared<MgnPotentials>();
	}

	// factory with input
	ShMgnPotentialsPr MgnPotentials::create(
		const arma::Mat<fltp> &Rt){
		return std::make_shared<MgnPotentials>(Rt);
	}


	// localpole to target setup function
	void MgnPotentials::setup_localpole_to_target(
		const arma::Mat<fltp> &dR, 
		const arma::uword /*num_dim*/,
		const rat::fmm::ShSettingsPr &stngs){
		
		const int num_exp = stngs->get_num_exp();

		// check input
		assert(dR.is_finite());

		// memory efficient implementation (default)
		if(stngs->get_memory_efficient_l2t()){
			dRl2p_ = dR;
		}

		// maximize computation speed
		else{
			// scalar potential
			if(has('S')){
				// calculate matrix for all target points
				M_A_.set_num_exp(num_exp);
				M_A_.calc_matrix(-dR);
			}

			// magnetic field
			if(has('H')){
				// calculate matrix for all target points
				M_H_.set_num_exp(num_exp);
				M_H_.calc_matrix(-dR);
			}
		}
	}

	// localpole to target function
	void MgnPotentials::localpole_to_target(
		const arma::Mat<std::complex<fltp> > &Lp, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target, 
		const arma::uword num_dim, const rat::fmm::ShSettingsPr &stngs){

		// set number of expansions
		const int num_exp = stngs->get_num_exp();

		// memory efficient implementation (default)
		// note that this method is called many times in parallel
		// do not re-use the class property of M_A and M_H
		if(stngs->get_memory_efficient_l2t()){
			// check if dR was set
			assert(!dRl2p_.is_empty());

			// magnetic scalar potential
			if(has('S')){
				// create temporary storage for vector potential
				arma::Row<fltp> S(num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,stngs->get_parallel_l2t(),[&](arma::uword i, int){
					// calculate matrix for these target points
					fmm::StMat_Lp2Ta M_A;
					M_A.set_num_exp(num_exp);
					M_A.calc_matrix(-dRl2p_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					S.cols(first_target(i),last_target(i)) += M_A.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1));
				});

				// add to self
				add_field('S',S,true);
			}

			// magnetic scalar potential
			if(has('H')){
				// create temporary storage for vector potential
				arma::Mat<fltp> H(3,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,stngs->get_parallel_l2t(),[&](arma::uword i, int){
					// calculate matrix for these target points
					fmm::StMat_Lp2Ta_Grad M_H;
					M_H.set_num_exp(num_exp);
					M_H.calc_matrix(-dRl2p_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					H.cols(first_target(i),last_target(i)) += M_H.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1));
				});

				// add to self
				add_field('H',H,true);
			}
		}

		// maximize computation speed using pre-calculated matrix
		else{
			// magnetic scalar potential
			if(has('S')){
				// check if localpole to target matrix was set
				assert(!M_A_.is_empty());

				// create temporary storage for vector potential
				arma::Row<fltp> S(num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,stngs->get_parallel_l2t(),[&](arma::uword i, int){
					S.cols(first_target(i),last_target(i)) += M_A_.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1), first_target(i), last_target(i));
				});

				// add to self
				add_field('S',S,true);
			}

			// magnetic field
			if(has('H')){
				// check if localpole to target matrix was set
				assert(!M_H_.is_empty());

				// create temporary storage for vector potential
				arma::Mat<fltp> H(3,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,stngs->get_parallel_l2t(),[&](arma::uword i, int){
					H.cols(first_target(i),last_target(i)) += M_H_.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1), first_target(i), last_target(i));
				});

				// add to self
				add_field('H',H,true);
			}
		}

	}

}}