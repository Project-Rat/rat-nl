// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "precparu.hh"

// open mp headers
#include "omp.h"

// common headers
#include "rat/common/extra.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecParU::PrecParU(){
		// set solver data object
		cholmod_l_start(&c_);

		// settings
		control_.umfpack_ordering = UMFPACK_ORDERING_METIS_GUARD;
		control_.umfpack_strategy = UMFPACK_STRATEGY_AUTO;
		control_.paru_max_threads = std::thread::hardware_concurrency();

		// allocate matrix
		A_ = new cholmod_sparse;
	}

	// destructor
	PrecParU::~PrecParU(){
		cholmod_l_finish(&c_); 
		delete A_;
		if(factorized_)ParU_Freenum(&num_, &control_);
		if(analyzed_)ParU_Freesym(&sym_, &control_);
	}

	// factory
	ShPrecParUPr PrecParU::create(){
		return std::make_shared<PrecParU>();
	}

	// set matrix
	void PrecParU::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		// set counters
		num_size_ = num_size;
		num_nnz_ = val.n_elem;

		// check matrix
		assert(idx_col.is_sorted());
		assert(idx_col.n_elem==val.n_elem);
		assert(idx_row.n_elem==val.n_elem);
		assert(idx_col.max()<num_size);
		assert(idx_row.max()<num_size);

		// compress column indices
		// count number of entries in each column
		arma::Col<arma::uword> ncol(num_size, arma::fill::zeros);
		for(arma::uword i=0;i<idx_col.n_elem;i++)ncol(idx_col(i))++;

		// create compression array
		const arma::Col<arma::uword> idx_col_c = arma::join_vert(
			arma::Col<arma::uword>{0},arma::cumsum(ncol));

		// set matrix
		idx_col_c_ = arma::conv_to<arma::Col<SuiteSparse_long> >::from(idx_col_c);
		idx_row_ = arma::conv_to<arma::Col<SuiteSparse_long> >::from(idx_row);
		val_ = val; 

		// allocate sparse matrix
		//A_ = cholmod_allocate_sparse(num_size_,num_size_,num_nnz_,true,true,0,CHOLMOD_REAL,&c_);
		
		// transfer matrix pointers
		A_->nrow = num_size_;
		A_->ncol = num_size_;
		A_->nzmax = num_nnz_;
		A_->nz = NULL;
		A_->p = idx_col_c_.memptr();
		A_->i = idx_row_.memptr();
		A_->x = val_.memptr();
		A_->z = NULL;
		A_->stype = 0; // symmetric (only upper triangle used)
		A_->itype = CHOLMOD_LONG; // problem with LONG
		A_->xtype = CHOLMOD_REAL;
		A_->dtype = CHOLMOD_DOUBLE;
		A_->sorted = true;
		A_->packed = true;

		// check matrix 
		cholmod_l_check_sparse(A_, &c_);

		// check duplicates
		assert(!arma::any(idx_row.head_rows(num_nnz_-1)==idx_row.tail_rows(num_nnz_-1) && 
			idx_col.head_rows(num_nnz_-1)==idx_col.tail_rows(num_nnz_-1)));
	}

	// analysis
	void PrecParU::analyse(){
		// report
		lg_->msg("%s  <<< paru analyse >>>%s\n",KMAG,KNRM);

		// store number of threads and set maximum
		const int previous_num_threads = omp_get_num_threads();
		omp_set_num_threads(std::thread::hardware_concurrency());

		// free previous analysis
		if(analyzed_)ParU_Freesym(&sym_, &control_);

		// run analysis
		ParU_Ret info = ParU_Analyze(A_, &sym_, &control_);

		// revert to old number of threads
		omp_set_num_threads(previous_num_threads);

		// check output
		if(info!=PARU_SUCCESS)rat_throw_line("paru analysis failed");

		// set analysed to true
		analyzed_ = true;
	}

	// factorisation
	void PrecParU::factorise(){
		// report
		lg_->msg("%s  <<< paru factorise (ntask %llu) >>>%s\n",KMAG,sym_->ntasks,KNRM);

		// store number of threads and set maximum
		const int previous_num_threads = omp_get_num_threads();
		omp_set_num_threads(std::thread::hardware_concurrency());

		// free previous analysis
		if(factorized_)ParU_Freenum(&num_, &control_);

		// run factorisation
		ParU_Ret info = ParU_Factorize(A_, sym_, &num_, &control_);

		// revert to old number of threads
		omp_set_num_threads(previous_num_threads);

		// check output
		if(info!=PARU_SUCCESS)rat_throw_line("paru factorisation failed");

		// now solving
		lg_->msg("%s  <<< paru solving (c.n. %.2e) >>>%s\n",KMAG,num_->rcond,KNRM);

		// set analysed to true
		factorized_ = true;
	}

	// solving
	arma::Col<double> PrecParU::solve(
		const arma::Col<rat::fltp> &b, 
		const rat::fltp /*delta*/){

		// copy right hand side
		arma::Col<double> rhs = b;

		// allocate result
		arma::Col<double> xx(num_size_,arma::fill::zeros);

		// store number of threads and set maximum
		const int previous_num_threads = omp_get_num_threads();
		omp_set_num_threads(std::thread::hardware_concurrency());

		// solve
		ParU_Ret info = ParU_Solve(sym_, num_, rhs.memptr(), xx.memptr(), &control_);
		
		// revert to old number of threads
		omp_set_num_threads(previous_num_threads);

		// check output
		if(info!=PARU_SUCCESS)rat_throw_line("paru solve failed");

		// return result
		return xx;
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecParU::mvp(
		const arma::Col<rat::fltp> &x) const{

		// make sure matrix is still compressed
		assert(!idx_col_c_.is_empty());
		assert(x.n_elem==num_size_);

		// allocate output
		arma::Col<rat::fltp> b(num_size_,arma::fill::zeros);

		// walk over columns
		for(arma::uword i=0;i<num_size_;i++){
			// get the indices of the rows present in this column
			arma::Col<arma::uword> idx_row = 
				arma::conv_to<arma::Col<arma::uword>>::from(
				idx_row_.rows(idx_col_c_(i),idx_col_c_(i+1)-1));

			// add this column's contribution to b
			b.rows(idx_row) += (val_.rows(idx_col_c_(i),idx_col_c_(i+1)-1)*x(i));
		}

		// output
		return b;
	}

	// display preconditioner info
	void PrecParU::display()const{
		lg_->msg("preconditioner: %sPARU%s (SuiteSparse)\n",KYEL,KNRM);
	}

}}


