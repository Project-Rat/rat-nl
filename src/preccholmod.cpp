// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "preccholmod.hh"

// common headers
#include "rat/common/extra.hh"

// Cholmod General Public License (GPL) safety check
#if !defined ENABLE_CHOLMOD_GPL && defined CHOLMOD_HAS_GPL
#error "Cholmod was compiled with GPL license, please set ENABLE_CHOLMOD_GPL=ON in cmake if you can accept that the compiled library becomes GPL licensed, or recompile suitesparse with the NGPL flag to only use the LGPL licensed features of CHOLMOD."
#endif

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecCholMod::PrecCholMod(){
		
		// set solver data object
		cholmod_l_start(&c_);

		// enable supernodal if compiled
		#ifdef CHOLMOD_HAS_SUPERNODAL
		c_.supernodal = CHOLMOD_AUTO; // supernodal falls under GNU General License
		#else
		c_.supernodal = CHOLMOD_SIMPLICIAL;
		#endif

		// enable GPU features (it seems slower and restricted by GPU memory)
		c_.useGPU = false;

		// #ifdef CHOLMOD_HAS_CUDA
		// c_.useGPU = true;
		// #else
		// c_.useGPU = false;
		// #endif

		// allocate matrix
		A_ = new cholmod_sparse;
	}

	// destructor
	PrecCholMod::~PrecCholMod(){
		// cholmod_l_gpu_stats(&c_);
		delete A_;
		if(L_!=NULL)cholmod_l_free_factor(&L_, &c_);
		cholmod_l_finish(&c_); 
	}

	// factory
	ShPrecCholModPr PrecCholMod::create(){
		return std::make_shared<PrecCholMod>();
	}

	// set matrix
	void PrecCholMod::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		// set counters
		num_size_ = num_size;
		num_nnz_ = val.n_elem;

		// check matrix
		assert(idx_col.is_sorted());
		assert(idx_col.n_elem==val.n_elem);
		assert(idx_row.n_elem==val.n_elem);
		assert(idx_col.max()<num_size);
		assert(idx_row.max()<num_size);

		// compress column indices
		// count number of entries in each column
		arma::Col<arma::uword> ncol(num_size, arma::fill::zeros);
		for(arma::uword i=0;i<idx_col.n_elem;i++)ncol(idx_col(i))++;

		// create compression array
		const arma::Col<arma::uword> idx_col_c = arma::join_vert(
			arma::Col<arma::uword>{0},arma::cumsum(ncol));

		// set matrix
		idx_col_c_ = arma::conv_to<arma::Col<SuiteSparse_long> >::from(idx_col_c);
		idx_row_ = arma::conv_to<arma::Col<SuiteSparse_long> >::from(idx_row);
		val_ = val; 

		// allocate sparse matrix
		//A_ = cholmod_allocate_sparse(num_size_,num_size_,num_nnz_,true,true,0,CHOLMOD_REAL,&c_);
		
		// transfer matrix pointers
		A_->nrow = num_size_;
		A_->ncol = num_size_;
		A_->nzmax = num_nnz_;
		A_->nz = NULL;
		A_->p = idx_col_c_.memptr();
		A_->i = idx_row_.memptr();
		A_->x = val_.memptr();
		A_->z = NULL;
		A_->stype = 1; // symmetric (only upper triangle used)
		A_->itype = CHOLMOD_LONG; // problem with LONG
		A_->xtype = CHOLMOD_REAL;
		A_->dtype = CHOLMOD_DOUBLE;
		A_->sorted = true;
		A_->packed = true;

		// check matrix 
		cholmod_l_check_sparse(A_, &c_);

		// check duplicates
		assert(!arma::any(idx_row.head_rows(num_nnz_-1)==idx_row.tail_rows(num_nnz_-1) && 
			idx_col.head_rows(num_nnz_-1)==idx_col.tail_rows(num_nnz_-1)));
	}

	// analysis
	void PrecCholMod::analyse(){
		// free matrix if it was already used
		if(L_!=NULL)cholmod_l_free_factor(&L_, &c_);

		// report
		lg_->msg("%s  <<< cholmod analyse >>>%s\n",KMAG,KNRM);

		// run analysis
		L_ = cholmod_l_analyze(A_, &c_);

		// analysis done
		analysed_ = true;
	}

	// factorisation
	void PrecCholMod::factorise(){
		// report
		lg_->msg("%s  <<< cholmod factorise >>>%s\n",KMAG,KNRM);

		// run factorisation
		cholmod_l_factorize(A_, L_, &c_);

		// report
		lg_->msg("%s  <<< cholmod solving (c.n. %.2e) >>>%s\n",KMAG,cholmod_l_rcond(L_, &c_),KNRM);

		// factorization done
		factorised_ = true;
	}

	// solving
	arma::Col<double> PrecCholMod::solve(
		const arma::Col<rat::fltp> &b, 
		const rat::fltp /*delta*/){

		// copy B
		arma::Col<double> rhs = b;

		// convert right hand side
		cholmod_dense *chol_rhs = new cholmod_dense; 
		chol_rhs->nrow = num_size_;
		chol_rhs->ncol = 1;
		chol_rhs->nzmax = num_size_;
		chol_rhs->x = rhs.memptr();
		chol_rhs->z = NULL;
		chol_rhs->xtype = CHOLMOD_REAL;
		chol_rhs->dtype = CHOLMOD_DOUBLE;
		chol_rhs->d = num_size_;

		// solve
		cholmod_dense *chol_x = cholmod_l_solve(CHOLMOD_A, L_, chol_rhs, &c_); 

		// get result by copying memory
		arma::Col<double> x(static_cast<double*>(chol_x->x), num_size_);

		// remove right hand side
		delete chol_rhs;
		cholmod_l_free_dense(&chol_x, &c_);

		// return result
		return x;
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecCholMod::mvp(
		const arma::Col<rat::fltp> &x) const{

		// make sure matrix is still compressed
		assert(!idx_col_c_.is_empty());
		assert(x.n_elem==num_size_);

		// allocate output
		arma::Col<rat::fltp> b(num_size_,arma::fill::zeros);

		// walk over columns
		for(arma::uword i=0;i<num_size_;i++){
			// get the indices of the rows present in this column
			arma::Col<arma::uword> idx_row = 
				arma::conv_to<arma::Col<arma::uword>>::from(
				idx_row_.rows(idx_col_c_(i),idx_col_c_(i+1)-1));

			// add this column's contribution to b
			b.rows(idx_row) += (val_.rows(idx_col_c_(i),idx_col_c_(i+1)-1)*x(i));
		}

		// output
		return b;
	}

	// display preconditioner info
	void PrecCholMod::display()const{
		lg_->msg("preconditioner: %sCHOLMOD%s (SuiteSparse)\n",KYEL,KNRM);
		if(c_.supernodal==CHOLMOD_SIMPLICIAL)lg_->msg("factorisation type: %sSimplical Cholesky%s\n",KYEL,KNRM);
		else if(c_.supernodal==CHOLMOD_SUPERNODAL)lg_->msg("factorisation type: %sSupernodal Cholesky%s\n",KYEL,KNRM);
		else if(c_.supernodal==CHOLMOD_AUTO)lg_->msg("factorisation type: %sAuto%s\n",KYEL,KNRM);
	}


}}


