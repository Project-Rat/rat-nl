// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "precjacobi.hh"

#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecJacobi::PrecJacobi(){
		
	}

	// destructor
	PrecJacobi::~PrecJacobi(){
		
	}

	// factory
	ShPrecJacobiPr PrecJacobi::create(){
		return std::make_shared<PrecJacobi>();
	}

	// set matrix
	void PrecJacobi::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		const arma::Col<arma::uword> idx_diag = arma::find(idx_col==idx_row);
		diag_.set_size(num_size); diag_.fill(RAT_CONST(1.0));
		diag_(idx_col(idx_diag)) = val(idx_diag);
	}

	// analysis
	void PrecJacobi::analyse(){
		
	}

	// factorisation
	void PrecJacobi::factorise(){
		
	}

	// solving
	arma::Col<rat::fltp> PrecJacobi::solve(
		const arma::Col<rat::fltp> &b, const rat::fltp){
		
		arma::Col<rat::fltp> x = b/diag_;

		// return output
		return x;
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecJacobi::mvp(
		const arma::Col<rat::fltp> &x) const{
		return diag_%x;
	}

	// display preconditioner info
	void PrecJacobi::display()const{
		lg_->msg("preconditioner: %sJACOBI (warning: may result in non-convergence!)%s\n",KRED,KNRM);
		lg_->msg("factorisation type: %sJACOBI%s\n",KYEL,KNRM);
	}

}}

