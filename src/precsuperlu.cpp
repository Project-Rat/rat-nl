// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only when armadillo available with SUPERLU
#ifndef DARMA_DONT_USE_SUPERLU

// include header file
#include "precsuperlu.hh"

#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecSuperLU::PrecSuperLU(){
		opts_.allow_ugly = false; // default: false
		opts_.equilibrate = true; // default: false
		opts_.symmetric = true; // default: false
		opts_.pivot_thresh = 1.0; // default: 1.0
		opts_.permutation = arma::superlu_opts::COLAMD; // default: superlu_opts::COLAMD (superlu_opts::NATURAL, superlu_opts::MMD_ATA, superlu_opts::MMD_AT_PLUS_A)
		opts_.refine = arma::superlu_opts::REF_NONE; // default: superlu_opts::REF_NONE (superlu_opts::REF_SINGLE, superlu_opts::REF_DOUBLE, superlu_opts::REF_EXTRA)
	}

	// factory
	ShPrecSuperLUPr PrecSuperLU::create(){
		return std::make_shared<PrecSuperLU>();
	}

	// set matrix
	void PrecSuperLU::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		// create spmat
		const bool check_for_zeros = false;
		const bool add_values = false;
		const bool sort_locations = false;
		A_ = arma::SpMat<fltp>(add_values, arma::join_horiz(idx_row,idx_col).t(),val, num_size,num_size,sort_locations,check_for_zeros);
	}

	// analysis
	void PrecSuperLU::analyse(){
		// report
		lg_->msg("%s  <<< superlu analyse >>>%s\n",KMAG,KNRM);

		// analysis is done as part of factorisation

		// set analyzed to true
		analysed_ = true;
	}

	// factorisation
	void PrecSuperLU::factorise(){
		// factorise t
		lg_->msg("%s  <<< superlu factorise >>>%s\n",KMAG,KNRM);
		const bool status = SF_.factorise(A_, opts_);
		if(status==false)rat_throw_line("superlu factorisation failed");

		// get condition number
		const double rcond_value = SF_.rcond();

		// report
		lg_->msg("%s  <<< superlu solving (c.n. %.2e) >>>%s\n",KMAG,rcond_value,KNRM);

		// set factorised to true
		factorised_ = true;
	}

	// solving
	arma::Col<rat::fltp> PrecSuperLU::solve(
		const arma::Col<rat::fltp> &b, 
		const rat::fltp /*delta*/){
				
		// solve for x
		arma::Col<fltp> x;
		const bool solution_ok = SF_.solve(x,b);

		// check output
		if(solution_ok==false)rat_throw_line("superlu solution not ok");

		// return output
		return x;
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecSuperLU::mvp(
		const arma::Col<rat::fltp> &x) const{

		// output
		return A_*x;
	}

	// display preconditioner info
	void PrecSuperLU::display()const{
		lg_->msg("preconditioner: %sSUPERLU%s\n",KYEL,KNRM);
		lg_->msg("factorisation type: %sLU%s\n",KYEL,KNRM);
	}

}}

#endif