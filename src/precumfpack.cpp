// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header file
#include "precumfpack.hh"

#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Raccoon
namespace rat{namespace nl{

	// constructor
	PrecUMFPack::PrecUMFPack(){
		// set default settings
		umfpack_dl_defaults(control_);
		control_[UMFPACK_ORDERING] = UMFPACK_ORDERING_BEST;
		// control_[UMFPACK_ORDERING] = UMFPACK_ORDERING_COLAMD;
		control_[UMFPACK_STRATEGY] = UMFPACK_STRATEGY_AUTO;
		// control_[UMFPACK_PIVOT_TOLERANCE] = 1e-6;
	}

	// destructor
	PrecUMFPack::~PrecUMFPack(){
		if(analysed_)umfpack_dl_free_symbolic(&Symbolic_);
		if(factorised_)umfpack_dl_free_numeric(&Numeric_);
	}

	// factory
	ShPrecUMFPackPr PrecUMFPack::create(){
		return std::make_shared<PrecUMFPack>();
	}

	// set droptol
	void PrecUMFPack::set_droptol(const rat::fltp droptol){
		control_[UMFPACK_DROPTOL] = droptol; // incomplete LU factorisation unless 0
	}

	// set matrix
	void PrecUMFPack::set_matrix(
		const arma::Col<arma::uword> &idx_col, 
		const arma::Col<arma::uword> &idx_row, 
		const arma::Col<rat::fltp> &val, 
		const arma::uword num_size){

		// set counters
		num_size_ = num_size;
		num_nnz_ = val.n_elem;

		// check matrix
		assert(idx_col.is_sorted());
		assert(idx_col.n_elem==val.n_elem);
		assert(idx_row.n_elem==val.n_elem);
		assert(idx_col.max()<num_size);
		assert(idx_row.max()<num_size);

		// compress column indices
		// count number of entries in each column
		arma::Col<arma::uword> ncol(num_size, arma::fill::zeros);
		for(arma::uword i=0;i<idx_col.n_elem;i++)ncol(idx_col(i))++;

		// create compression array
		const arma::Col<arma::uword> idx_col_c = arma::join_vert(
			arma::Col<arma::uword>{0},arma::cumsum(ncol));

		// set matrix
		idx_col_c_ = arma::conv_to<arma::Col<SuiteSparse_long> >::from(idx_col_c);
		idx_row_ = arma::conv_to<arma::Col<SuiteSparse_long> >::from(idx_row);
		val_ = val; 

		// check duplicates
		assert(!arma::any(idx_row.head_rows(num_nnz_-1)==idx_row.tail_rows(num_nnz_-1) && 
			idx_col.head_rows(num_nnz_-1)==idx_col.tail_rows(num_nnz_-1)));
	}

	// analysis
	void PrecUMFPack::analyse(){
		// make sure matrix is compressed
		assert(!idx_col_c_.is_empty());
		assert(arma::as_scalar(idx_col_c_.tail(1))==(arma::sword)num_nnz_);

		// get and cast memory pointers
		const SuiteSparse_long* col_ptr = static_cast<SuiteSparse_long*>(idx_col_c_.memptr());
		const SuiteSparse_long* row_ptr = static_cast<SuiteSparse_long*>(idx_row_.memptr());
		const rat::fltp* val_ptr = static_cast<rat::fltp*>(val_.memptr());
		int n = static_cast<int>(num_size_);

		// free memory
		if(analysed_)umfpack_dl_free_symbolic(&Symbolic_);

		//  Carry out the symbolic factorization
		lg_->msg("%s  <<< umfpack analysing >>>%s\n",KMAG,KNRM);
		status_ = umfpack_dl_symbolic(SuiteSparse_long(n), SuiteSparse_long(n), col_ptr, row_ptr, 
			val_ptr, &Symbolic_, control_, info_ );

		// report and check
		if(status_!=UMFPACK_OK){
			umfpack_di_report_status(control_, int(status_));
			rat_throw_line("PrecUMFPack failed the analysis step");
		}

		// set analyzed to true
		analysed_ = true;
	}

	// factorisation
	void PrecUMFPack::factorise(){
		// make sure values are set
		assert(!idx_row_.is_empty());
		assert(!idx_col_c_.is_empty());
		assert(!val_.is_empty());

		// check
		assert(analysed_==true);
		assert(!idx_col_c_.is_empty());

		// get and cast memory pointers
		const SuiteSparse_long* col_ptr = static_cast<SuiteSparse_long*>(idx_col_c_.memptr());
		const SuiteSparse_long* row_ptr = static_cast<SuiteSparse_long*>(idx_row_.memptr());
		const rat::fltp* val_ptr = static_cast<rat::fltp*>(val_.memptr());

		// free previous analysis
		if(factorised_)umfpack_dl_free_numeric(&Numeric_);

		//  Use the symbolic factorization to carry out the numeric factorization.
		lg_->msg("%s  <<< umfpack factorising >>>%s\n",KMAG,KNRM);
		status_ = umfpack_dl_numeric(col_ptr, row_ptr, 
			val_ptr, Symbolic_, &Numeric_, control_, info_ );

		// report and check
		if(status_!=UMFPACK_OK){
			umfpack_di_report_status(control_, int(status_));
			rat_throw_line("PrecUMFPack failed calculating the factorisation");
		}

		// now solving
		lg_->msg("%s  <<< umfpack solving (c.n. %.2e) >>>%s\n",KMAG,info_[UMFPACK_RCOND],KNRM);

		// check strategy
		// if(info_[UMFPACK_STRATEGY_USED]==UMFPACK_STRATEGY_SYMMETRIC)
		// 	std::printf("using symmetric strategy\n");

		// set factorised to true
		factorised_ = true;
	}

	// solving
	arma::Col<rat::fltp> PrecUMFPack::solve(
		const arma::Col<rat::fltp> &b, 
		const rat::fltp /*delta*/){
		
		// make sure matrix is still compressed
		assert(!idx_col_c_.is_empty());
		
		// make sure factorisation was performed
		assert(factorised_==true);

		// make sure R has the correct size
		assert(b.n_elem==num_size_);

		// check for nans in input vector
		assert(b.is_finite());

		// allocate output
		arma::Col<rat::fltp> x(num_size_,arma::fill::zeros);

		// get pointer to r
		const rat::fltp* B = const_cast<rat::fltp*>(b.memptr());
		rat::fltp* X = static_cast<rat::fltp*>(x.memptr());

		// get and cast memory pointers
		const SuiteSparse_long* col_ptr = static_cast<SuiteSparse_long*>(idx_col_c_.memptr());
		const SuiteSparse_long* row_ptr = static_cast<SuiteSparse_long*>(idx_row_.memptr());
		const rat::fltp* val_ptr = static_cast<rat::fltp*>(val_.memptr());

		//  Solve the linear system.
		status_ = umfpack_dl_solve(UMFPACK_A, col_ptr, row_ptr, 
			val_ptr, X, B, Numeric_, control_, info_);
		
		// report and check
		if(status_!=UMFPACK_OK){
			umfpack_di_report_status(control_, int(status_));
			rat_throw_line("PrecUMFPack failed finding a solution");
		}

		// check result
		if(arma::norm(mvp(x)-b)/arma::norm(b)>1e-3 && arma::any(b!=0))
			rat_throw_line("PrecUMFPack failed solution check");

		// return output
		return x;
	}

	// matrix vector multiplication
	arma::Col<rat::fltp> PrecUMFPack::mvp(
		const arma::Col<rat::fltp> &x) const{

		// make sure matrix is still compressed
		assert(!idx_col_c_.is_empty());
		assert(x.n_elem==num_size_);

		// allocate output
		arma::Col<rat::fltp> b(num_size_,arma::fill::zeros);

		// walk over columns
		for(arma::uword i=0;i<num_size_;i++){
			// get the indices of the rows present in this column
			arma::Col<arma::uword> idx_row = 
				arma::conv_to<arma::Col<arma::uword>>::from(
				idx_row_.rows(idx_col_c_(i),idx_col_c_(i+1)-1));

			// add this column's contribution to b
			b.rows(idx_row) += (val_.rows(idx_col_c_(i),idx_col_c_(i+1)-1)*x(i));
		}

		// output
		return b;
	}

	// display preconditioner info
	void PrecUMFPack::display()const{
		lg_->msg("preconditioner: %sUMFPACK%s (SuiteSparse)\n",KYEL,KNRM);
		lg_->msg("factorisation type: %sLU%s\n",KYEL,KNRM);
	}

}}

