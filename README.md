![Logo](./figs/RATLogo.png)
# NLSolver
<em>RAT thinks he is iron man yes yes ...</em>

## Methodology
RAT utilizes the Volume Integral Method (VIM) to calculate magnetic fields in non-linear magnetic materials, particularly Iron. The VIM method was originally developed by Vinh Le-Van at Grenoble Electrical Engineering Laboratory [1, 2, 3, 4]. Unlike the Finite Element Method (FEM), the VIM method eliminates the need for meshing the air surrounding the non-linear magnetic material. This results in a significant reduction in the number of elements required and simplifies the process of generating a suitable mesh. Additionally, it overcomes the inherent instability issues associated with magnetic moment methods when used with non-linear magnetic materials [5, 6] and does not require loop/tree finding algorithms [7]. Nor does it require any gauge fixing techniques [13]. Further sources worth mentioning are [14,15,16].

The research papers describe various types of elements that can be used. One option is to employ facet-elements, which calculate the flux through the internal and external faces of the mesh. Another option is to use edge-elements, which determine the vector potential along each edge of the mesh. RAT employs the latter approach, benefiting from its auto-gauging capability. The iron solver implemented here can deal with both hexahedral and tetrahedral meshes. The non-linear magnetic field as function of flux density within the mesh is described with a so-called HB-curve. 

To solve the resulting non-linear system of equations, RAT utilizes Sundials [KINSOL](https://computation.llnl.gov/projects/sundials/kinsol) solver [8, 9]. Sundials' KINSOL is an Inexact Newton Non-Linear Solver, enabling the use of an iterative solver like FGMRES for solving the linear system. The iterative solver is pre-conditioned using Cholmod [17] or Superlu [18]. This approach offers the advantage of not requiring the storage of a dense flux leakage matrix. Instead, an approximation is used for pre-conditioning with CHOLMOD or UMFPACK.

## Validation
The code is validated using three different models. It is expected that more models will be added in the future. These models are included as test models in the [RAT-Models](https://gitlab.com/Project-RAT/rat-models) library. 

The first model is an iron sphere. Magnetized spheres exhibit the property that the field inside is homogenous and related to the magnetization as H = -M/3 + Hext (see [this link](https://farside.ph.utexas.edu/teaching/jk1/Electromagnetism/node61.html)), where the magnetization M = chi(H)H. By using these relations in a fixed point approach the magnetization inside the sphere is calculated directly. The calculated value is then compared to the magnetization in the center of the sphere as calculated by the rat-nl solver. When using sufficiently small element size the results are identical within 0.01 percent. 

The second test uses an iron ring that is magnetized by a small solenoid sitting on one side. A mesh calculation showing the flux density on the mesh surface is shown below. The ring magnetizes all the way round. To have a reference flux density in the ring for comparison the model was also build in COMSOL. The resulting flux density in the non-coil side of the ring is compared to the COMSOL model. The results are within a percent.

![Logo](./figs/NLRing_Mesh.png)

To validate the code the Compumag [Team13](https://www.compumag.org/jsite/images/stories/TEAM/problem13.pdf) problem is implemented in RAT. The model consists of a rectangular coil and 3.2 mm thick steel plates described by an HB-curve that is also supplied with the model. A mesh calculation showing the flux density calculated on the surface of the mesh is shown in the figure below. In the tables the flux densities calculated by RAT are compared to the reference values, gathered from [3].

![Logo](./figs/Team13_Mesh_2.png)

```
Flux Density in Air (1000AT)                     Flux Density in Air (3000AT)
y = 0.020 [m], z = 0.055 [m]                     y = 0.020 [m], z = 0.055 [m]
  id    x [m]  |Brat| [T]  |Bref| [T] diff [%]     id    x [m]  |Brat| [T]  |Bref| [T] diff [%]
==============================================   ==============================================
0001    0.010  3.5156e-02  3.4392e-02   2.2215   0001    0.010  6.3813e-02  6.2500e-02   2.1014
0002    0.020  2.0315e-02  2.0676e-02   1.7460   0002    0.020  4.2249e-02  4.2905e-02   1.5307
0003    0.030  1.5994e-02  1.6351e-02   2.1841   0003    0.030  3.6000e-02  3.6554e-02   1.5170
0004    0.040  1.4014e-02  1.4392e-02   2.6248   0004    0.040  3.2988e-02  3.3446e-02   1.3691
0005    0.050  1.2843e-02  1.3108e-02   2.0256   0005    0.050  3.0904e-02  3.1284e-02   1.2138
0006    0.060  1.1904e-02  1.2095e-02   1.5775   0006    0.060  2.8812e-02  2.9257e-02   1.5213
0007    0.070  1.0786e-02  1.1014e-02   2.0667   0007    0.070  2.5863e-02  2.6216e-02   1.3476
0008    0.080  8.7011e-03  8.6486e-03   0.6066   0008    0.080  2.0279e-02  2.0473e-02   0.9472
0009    0.090  5.6938e-03  5.7432e-03   0.8610   0009    0.090  1.2837e-02  1.2905e-02   0.5285
0010    0.100  2.9340e-03  2.9054e-03   0.9830   0010    0.100  6.9219e-03  7.0946e-03   2.4340
0011    0.110  1.3888e-03  1.4189e-03   2.1201   0011    0.110  4.4714e-03  4.4595e-03   0.2683
==============================================   ==============================================
```

```
Flux Density in steel (1000AT)          Flux Density in steel (3000AT)
  id  |Brat| [T]  |Bref| [T] diff [%]     id  |Brat| [T]  |Bref| [T] diff [%]
=====================================   =====================================
0001  1.3488e+00  1.3819e+00   2.3951   0001  1.8851e+00  1.8701e+00   0.8042
0002  1.3371e+00  1.3701e+00   2.4047   0002  1.8768e+00  1.8661e+00   0.5707
0003  1.3031e+00  1.3346e+00   2.3637   0003  1.8536e+00  1.8425e+00   0.5992
0004  1.2451e+00  1.2795e+00   2.6930   0004  1.8096e+00  1.8031e+00   0.3564
0005  1.1561e+00  1.1890e+00   2.7657   0005  1.7261e+00  1.7244e+00   0.0983
0006  1.0165e+00  1.0551e+00   3.6607   0006  1.5482e+00  1.5591e+00   0.6975
0007  6.5661e-01  6.6535e-01   1.3143   0007  9.9180e-01  1.0276e+00   3.4796
0008  2.6969e-01  2.7165e-01   0.7232   0008  4.1263e-01  3.9764e-01   3.7711
0009  4.7187e-01  4.7244e-01   0.1211   0009  7.4878e-01  7.5984e-01   1.4560
0010  5.7234e-01  5.9055e-01   3.0844   0010  9.4209e-01  9.4882e-01   0.7092
0011  6.4566e-01  6.6535e-01   2.9598   0011  1.0885e+00  1.0945e+00   0.5505
0012  7.0787e-01  7.2835e-01   2.8114   0012  1.2156e+00  1.2205e+00   0.3990
0013  7.6312e-01  7.7953e-01   2.1044   0013  1.3288e+00  1.3346e+00   0.4366
0014  8.1355e-01  8.3071e-01   2.0660   0014  1.4303e+00  1.4370e+00   0.4657
0015  8.9596e-01  9.1339e-01   1.9083   0015  1.5810e+00  1.5906e+00   0.6013
0016  9.3952e-01  9.5669e-01   1.7949   0016  1.6283e+00  1.6378e+00   0.5820
0017  9.4745e-01  9.6063e-01   1.3722   0017  1.6236e+00  1.6339e+00   0.6284
0018  9.3865e-01  9.6457e-01   2.6869   0018  1.5883e+00  1.6181e+00   1.8431
0019  9.4432e-01  9.6850e-01   2.4975   0019  1.5856e+00  1.6063e+00   1.2862
0020  9.5572e-01  9.6850e-01   1.3200   0020  1.6019e+00  1.6142e+00   0.7634
0021  9.6034e-01  9.7638e-01   1.6427   0021  1.6070e+00  1.6142e+00   0.4451
0022  9.6515e-01  9.8031e-01   1.5470   0022  1.6132e+00  1.6181e+00   0.3063
0023  9.6943e-01  9.8425e-01   1.5056   0023  1.6189e+00  1.6260e+00   0.4358
0024  9.7166e-01  9.8819e-01   1.6729   0024  1.6216e+00  1.6299e+00   0.5096
0025  9.7288e-01  9.8819e-01   1.5488   0025  1.6234e+00  1.6339e+00   0.6415
=====================================   =====================================
```

## Performance and Scaling
The plot below shows how the computation time scales with the number of edges for a diverse number of applications. The scaling is more or less linear which implies O(N) complexity.

![clover](./figs/solverscaling.png)

## Preconditioner Choice
When it comes to the direct sparse matrix solver used by the preconditioner Suitesparse Supernodal Cholmod provides the best performance. However, the act of linking to this library will cause the combined code to be under the GNU license. If you wish to avoid this its better to use sequential SuperLU which is made available through the Armadilo library. It would be nice to have a permissively licensed Cholesky solver. If you have suggestions for this feel free to send me a message, create an issue or even better send me a merge request.

## Gallery
Magnetic flux density in a cloverleaf coil with iron yoke simulated in RAT and visualized using Paraview volume rendering:
![clover](./figs/clover.png)

Rat GUI model of a magnetic sprocket inside low field solenoid coil:
![sprocket](./figs/Sprocket.png)

Rat GUI model of an iron based Actuator:
![actuator](./figs/Actuator.png)

Rat GUI model showing a plane and mesh calculation on the CERN Large Hadron Collider (LHC) twin-aperture dipole:
![lhc_dipole](./figs/LHC_Dipole2.png)

Rat GUI model of the CERN Fresca2 Nb3Sn dipole magnet:
![fresca2](./figs/Fresca2.png)

## Authors
* Jeroen van Nugteren
* Nikkie Deelen

## License
This project is licensed under the [MIT](LICENSE) licensec.

## References
[1] A. Carpentier, O. Chadebec, N. Galopin, G. Meunier and B. Bannwarth, "Resolution of Nonlinear Magnetostatic Problems With a Volume Integral Method Using the Magnetic Scalar Potential," in IEEE Transactions on Magnetics, vol. 49, no. 5, pp. 1685-1688, May 2013, doi: 10.1109/TMAG.2013.2241750.

[2] V. Le-Van, G. Meunier, O. Chadebec and J. -M. Guichon, "A Volume Integral Formulation Based on Facet Elements for Nonlinear Magnetostatic Problems," in IEEE Transactions on Magnetics, vol. 51, no. 7, pp. 1-6, July 2015, Art no. 7002206, doi: 10.1109/TMAG.2015.2389197.

[3] V. Le-Van, G. Meunier, O. Chadebec and J. -M. Guichon, "A Magnetic Vector Potential Volume Integral Formulation for Nonlinear Magnetostatic Problems," in IEEE Transactions on Magnetics, vol. 52, no. 3, pp. 1-4, March 2016, Art no. 7002804, doi: 10.1109/TMAG.2015.2490627.

[4] Christophe Rubeck, Jean-Paul Yonnet, Hicham Allag, Benoît Delinchant, Olivier Chadebec. "Analytical Calculation of Magnet Systems: Magnetic Field Created by Charged Triangles and Polyhedra", IEEE Transactions on Magnetics, 2013, 49 (1), pp.144-147. 10.1109/TMAG.2012.2219511. hal-00982164

[5] Newman, Michael J. et al. “GFUN: an interactive program as an aid to magnet design.” (1972).

[6] Armstrong, A G.A.M., Collie, C J, Diserens, N J, Newman, M J, Simkin, J, and Trowbridge, C W. New developments in the magnet design computer program GFUN. United Kingdom: N. p., 1975. Web. 

[7] Kettunen, L., Forsman, K., Levine, D., & Gropp, W. (1995). Volume integral equations in non‐linear 3‐D magnetostatics. International Journal for Numerical Methods in Engineering, 38(16), 2655-2675. https://doi.org/10.1002/nme.1620381602

[8] A. C. Hindmarsh, P. N. Brown, K. E. Grant, S. L. Lee, R. Serban, D. E. Shumaker, and C. S. Woodward, "SUNDIALS: Suite of Nonlinear and Differential/Algebraic Equation Solvers," ACM Transactions on Mathematical Software, 31(3), pp. 363-396, 2005. Also available as LLNL technical report UCRL-JP-200037.

[9] Gardner, D. J., Reynolds, D.R., Woodward, C.S., & Balos, C. J. (2022). Enabling new flexibility in the {SUNDIALS} suite of nonlinear and differential/algebraic equation solvers. ACM Trans. on Math. Software. https://doi.org/10.1145/3539801. 

[10] Conrad Sanderson and Ryan Curtin. Armadillo: a template-based C++ library for linear algebra. Journal of Open Source Software, Vol. 1, No. 2, pp. 26, 2016.

[11] Conrad Sanderson and Ryan Curtin. A User-Friendly Hybrid Sparse Matrix Class in C++. Lecture Notes in Computer Science (LNCS), Vol. 10931, pp. 422-430, 2018. 

[12] https://defelement.com/

[13] Zhuoxiang Ren, "Influence of the R.H.S. on the Convergence Behaviour of the Curl-Curl Equation", IEEE Transactions On Magnetics, Vol 32, No 3, May 1996

[14] Anthony Carpentier, "Formulation intégrale de volume magnétostatique et calcul des densités de force magnétique : Application au couplage magnéto-mécanique", Application au couplage magnéto-mécanique. Sciences de l’ingénieur [physics].
Université de Grenoble, 2013. Français.

[15] Jean-Louis Coulomb. "Analyse tridimensionnelle des champs électriques et magnétiques par la méthode des éléments finis." Modélisation et simulation. Institut National Polytechnique de Grenoble - INPG; Université Joseph-Fourier - Grenoble I magnétiques par la méthode des éléments finis

[16] Olivier Barre. "Contribution à l’étude des formulations de calcul de la force magnétique en magnétostatique, approche numérique et validation expérimentale", Modélisation et simulation. Université des Sciences et Technologie de Lille - Lille I, 2003. Français.

[17] Algorithm 887: CHOLMOD, Supernodal Sparse Cholesky Factorization and Update/Downdate, Yanqing Chen, Timothy A. Davis, William W. Hager, Sivasankaran Rajamanickam, ACM Transactions on Mathematical Software, Vol 35, Issue 3, 2008, pp 22:1 - 22:14.

[18] James W. Demmel and Stanley C. Eisenstat and John R. Gilbert and Xiaoye S. Li and Joseph W. H. Liu, 
"A supernodal approach to sparse partial pivoting", SIAM J. Matrix Analysis and Applications, 1999, volume 20, number 3, pages 720-755

